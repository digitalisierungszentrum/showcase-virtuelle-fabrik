"""add Arbeitsschritt entity

Revision ID: 8741480c2ca3
Revises: 87a01f4da4cf
Create Date: 2023-03-29 04:41:33.682696

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8741480c2ca3'
down_revision = '87a01f4da4cf'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('arbeitsschritt',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column('produktionsschritt', sa.Column('arbeitsschritt_id', sa.String(), nullable=False))
    op.create_foreign_key(None, 'produktionsschritt', 'arbeitsschritt', ['arbeitsschritt_id'], ['id'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'produktionsschritt', type_='foreignkey')
    op.drop_column('produktionsschritt', 'arbeitsschritt_id')
    op.drop_table('arbeitsschritt')
    # ### end Alembic commands ###
