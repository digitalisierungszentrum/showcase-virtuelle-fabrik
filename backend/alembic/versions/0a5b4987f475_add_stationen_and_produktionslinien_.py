"""add stationen and produktionslinien tables

Revision ID: 0a5b4987f475
Revises: e14b4e7cbc95
Create Date: 2023-03-28 08:40:56.637492

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0a5b4987f475'
down_revision = 'e14b4e7cbc95'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('produktionslinien',
    sa.Column('id', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('stationen',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('produktionslinie_id', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['produktionslinie_id'], ['produktionslinien.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('station_charge_association_table',
    sa.Column('station_id', sa.String(), nullable=False),
    sa.Column('charge_id', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['charge_id'], ['charge.id'], ),
    sa.ForeignKeyConstraint(['station_id'], ['stationen.id'], ),
    sa.PrimaryKeyConstraint('station_id', 'charge_id')
    )
    op.create_table('station_maschine_association_table',
    sa.Column('station_id', sa.String(), nullable=False),
    sa.Column('maschine_id', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['maschine_id'], ['maschinen.id'], ),
    sa.ForeignKeyConstraint(['station_id'], ['stationen.id'], ),
    sa.PrimaryKeyConstraint('station_id', 'maschine_id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('station_maschine_association_table')
    op.drop_table('station_charge_association_table')
    op.drop_table('stationen')
    op.drop_table('produktionslinien')
    # ### end Alembic commands ###
