from typing import List
import uuid
from attrs import asdict


from fastapi.responses import JSONResponse
from fastapi import APIRouter, FastAPI, Request, status
from fastapi.middleware.cors import CORSMiddleware
import numpy as np
from pydantic import BaseConfig, BaseModel

from virtuelle_fabrik.domain.exception import DomainException
from virtuelle_fabrik.domain.models import (
    Arbeitsschritt,
    Charge,
    LeistungsErgebnis,
    Maschine,
    MaschinenBefaehigung,
    Maschinenauslastung,
    Material,
    Materialbedarf,
    Optimierung,
    OptimierungsErgebnis,
    Produkt,
    Produktbedarf,
    Produktionsschritt,
)
from virtuelle_fabrik.optimization.controller import calc_optimization
from virtuelle_fabrik.persistence.charge import (
    add_charge,
    get_all_chargen,
    get_charge,
    edit_charge,
    remove_charge,
)
from virtuelle_fabrik.persistence.database import async_session
from virtuelle_fabrik.persistence.maschinen import (
    get_maschine,
    get_maschinen,
    add_maschine,
    remove_maschine,
    edit_maschine,
)
from virtuelle_fabrik.persistence.produkte import (
    add_arbeitsschritt,
    add_material,
    add_produkt,
    get_all_material,
    get_all_produkte,
    get_arbeitsschritt,
    get_arbeitsschritte,
    get_material,
    get_produkt,
    edit_arbeitsschritt,
    edit_material,
    edit_produkt,
    remove_arbeitsschritt,
    remove_material,
    remove_produkt,
)
from virtuelle_fabrik.persistence.produktionslinien import (
    get_or_create_produktionslinie,
)
from .socket_handlers import setupWebsocket


app = FastAPI(title="REST API using FastAPI PostgreSQL Async EndPoints")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

setupWebsocket(app)

szenario_router = APIRouter(prefix="/api/szenarios/{szenario_id}")


def to_lower_camel(string: str) -> str:
    upper = "".join(word.capitalize() for word in string.split("_"))
    return upper[:1].lower() + upper[1:]


class APIModel(BaseModel):
    class Config(BaseConfig):
        allow_population_by_alias = True
        allow_population_by_field_name = True
        alias_generator = to_lower_camel


class MaschinenBefaehigungIn(APIModel):
    schritt_id: str
    taktrate: float


class MaschineIn(APIModel):
    name: str
    ruestzeit: float
    kosten_minute: float
    anschaffungskosten: float
    ausfall_wahrscheinlichkeit: float
    mitarbeiter_min: int
    mitarbeiter_max: int
    maschinenbefaehigungen: List[MaschinenBefaehigungIn]


class MaschinenBefaehigungTO(APIModel):
    id: str
    schritt_id: str
    taktrate: float


class MaschineTO(APIModel):
    id: str
    name: str
    ruestzeit: float
    kosten_minute: float
    anschaffungskosten: float
    ausfall_wahrscheinlichkeit: float
    mitarbeiter_min: int
    mitarbeiter_max: int
    maschinenbefaehigungen: List[MaschinenBefaehigungTO]


@app.exception_handler(DomainException)
async def domain_exception_handler(request: Request, exc: DomainException):
    return JSONResponse(
        status_code=500,
        content={"message": f"{exc.message}"},
    )


@szenario_router.get(
    "/maschinen/",
    response_model=List[MaschineTO],
    status_code=status.HTTP_200_OK,
)
async def read_maschinen(skip: int = 0, take: int = 20):
    async with async_session() as session:
        result = await get_maschinen(session, skip, take)
        return [MaschineTO(**asdict(x)) for x in result]


@szenario_router.get(
    "/maschinen/{maschine_id}",
    response_model=MaschineTO,
    status_code=status.HTTP_200_OK,
)
async def read_maschine(maschine_id: str):
    async with async_session() as session:
        result = await get_maschine(session, maschine_id)
        return MaschineTO(**asdict(result))


@szenario_router.post(
    "/maschinen/",
    response_model=MaschineTO,
    status_code=status.HTTP_201_CREATED,
)
async def create_maschine(maschine: MaschineIn):
    async with async_session() as session:
        maschine_in_dict = maschine.dict()
        maschine_in_dict.pop("maschinenbefaehigungen", None)
        result = await add_maschine(
            session,
            Maschine(
                id=uuid.uuid4().hex,
                **maschine_in_dict,
                maschinenbefaehigungen=[
                    MaschinenBefaehigung(id=uuid.uuid4().hex, **x.dict())
                    for x in maschine.maschinenbefaehigungen
                ],
            ),
        )
        return MaschineTO(**asdict(result))


@szenario_router.put(
    "/maschinen/{maschine_id}",
    response_model=MaschineTO,
    status_code=status.HTTP_200_OK,
)
async def update_maschine(maschine_id: str, maschine: MaschineIn):
    async with async_session() as session:
        maschine_in_dict = maschine.dict()
        maschine_in_dict.pop("maschinenbefaehigungen", None)
        result = await edit_maschine(
            session,
            maschine_id,
            Maschine(
                id=uuid.uuid4().hex,
                **maschine_in_dict,
                maschinenbefaehigungen=[
                    MaschinenBefaehigung(id=uuid.uuid4().hex, **x.dict())
                    for x in maschine.maschinenbefaehigungen
                ],
            ),
        )
        return MaschineTO(**asdict(result))


@szenario_router.delete("/maschinen/{maschine_id}", status_code=status.HTTP_200_OK)
async def delete_maschine(maschine_id: str):
    async with async_session() as session:
        await remove_maschine(session, maschine_id)
        return {
            "message": "Maschine with id: {} deleted successfully!".format(maschine_id)
        }


class MaterialIn(APIModel):
    name: str
    kosten_stueck: float
    bestand: float
    aufstocken_minute: float


class MaterialTO(APIModel):
    id: str
    name: str
    kosten_stueck: float
    bestand: float
    aufstocken_minute: float


@szenario_router.get(
    "/materialien/",
    response_model=List[MaterialTO],
    status_code=status.HTTP_200_OK,
)
async def read_all_material(skip: int = 0, take: int = 20):
    async with async_session() as session:
        result = await get_all_material(session, skip, take)
        return [MaterialTO(**asdict(x)) for x in result]


@szenario_router.get(
    "/materialien/{material_id}",
    response_model=MaterialTO,
    status_code=status.HTTP_200_OK,
)
async def read_material(material_id: str):
    async with async_session() as session:
        result = await get_material(session, material_id)
        return MaterialTO(**asdict(result))


@szenario_router.post(
    "/materialien/",
    response_model=MaterialTO,
    status_code=status.HTTP_201_CREATED,
)
async def create_material(material: MaterialIn):
    async with async_session() as session:
        result = await add_material(
            session,
            Material(
                id=uuid.uuid4().hex,
                **material.dict(),
            ),
        )
        return MaterialTO(**asdict(result))


@szenario_router.put(
    "/materialien/{material_id}",
    response_model=MaterialTO,
    status_code=status.HTTP_200_OK,
)
async def update_material(material_id: str, material: MaterialIn):
    async with async_session() as session:
        material_in_dict = material.dict()
        result = await edit_material(
            session,
            material_id,
            Material(
                id=uuid.uuid4().hex,
                **material_in_dict,
            ),
        )
        return MaterialTO(**asdict(result))


@szenario_router.delete("/materialien/{material_id}", status_code=status.HTTP_200_OK)
async def delete_material(material_id: str):
    async with async_session() as session:
        await remove_material(session, material_id)
        return {
            "message": "Material with id: {} deleted successfully!".format(material_id)
        }


class ArbeitsschrittIn(APIModel):
    name: str


class ArbeitsschrittTO(APIModel):
    id: str
    name: str


@szenario_router.get(
    "/arbeitsschritte/",
    response_model=List[ArbeitsschrittTO],
    status_code=status.HTTP_200_OK,
)
async def read_all_arbeitsschritte(skip: int = 0, take: int = 20):
    async with async_session() as session:
        result = await get_arbeitsschritte(session, skip, take)
        return [ArbeitsschrittTO(**asdict(x)) for x in result]


@szenario_router.get(
    "/arbeitsschritte/{arbeitsschritt_id}",
    response_model=ArbeitsschrittTO,
    status_code=status.HTTP_200_OK,
)
async def read_arbeitsschritt(arbeitsschritt_id: str):
    async with async_session() as session:
        result = await get_arbeitsschritt(session, arbeitsschritt_id)
        return ArbeitsschrittTO(**asdict(result))


@szenario_router.post(
    "/arbeitsschritte/",
    response_model=ArbeitsschrittTO,
    status_code=status.HTTP_201_CREATED,
)
async def create_arbeitsschritt(arbeitsschritt: ArbeitsschrittIn):
    async with async_session() as session:
        result = await add_arbeitsschritt(
            session,
            Arbeitsschritt(
                id=uuid.uuid4().hex,
                **arbeitsschritt.dict(),
            ),
        )
        return ArbeitsschrittTO(**asdict(result))


@szenario_router.put(
    "/arbeitsschritte/{arbeitsschritt_id}",
    response_model=ArbeitsschrittTO,
    status_code=status.HTTP_200_OK,
)
async def update_arbeitsschritt(
    arbeitsschritt_id: str, arbeitsschritt: ArbeitsschrittIn
):
    async with async_session() as session:
        arbeitsschritt_in_dict = arbeitsschritt.dict()
        result = await edit_arbeitsschritt(
            session,
            arbeitsschritt_id,
            Arbeitsschritt(
                id=uuid.uuid4().hex,
                **arbeitsschritt_in_dict,
            ),
        )
        return ArbeitsschrittTO(**asdict(result))


@szenario_router.delete(
    "/arbeitsschritte/{arbeitsschritt_id}", status_code=status.HTTP_200_OK
)
async def delete_arbeitsschritt(arbeitsschritt_id: str):
    async with async_session() as session:
        await remove_arbeitsschritt(session, arbeitsschritt_id)
        return {
            "message": "Arbeitsschritt with id: {} deleted successfully!".format(
                arbeitsschritt_id
            )
        }


class ProduktionsschrittIn(APIModel):
    arbeitsschritt_id: str
    schritt: int


class ProduktionsschrittTO(APIModel):
    id: str
    arbeitsschritt_id: str
    schritt: int


class MaterialbedarfIn(APIModel):
    material_id: str
    menge: float


class MaterialbedarfTO(APIModel):
    id: str
    material_id: str
    menge: float


class ProduktIn(APIModel):
    name: str
    verkaufspreis: float
    produktionsschritte: list[ProduktionsschrittIn]
    materialbedarf: list[MaterialbedarfIn]


class ProduktTO(APIModel):
    id: str
    name: str
    verkaufspreis: float
    produktionsschritte: list[ProduktionsschrittTO]
    materialbedarf: list[MaterialbedarfTO]


def convert_to_produktto(produkt: Produkt) -> ProduktTO:
    return ProduktTO(
        id=produkt.id,
        name=produkt.name,
        verkaufspreis=produkt.verkaufspreis,
        produktionsschritte=[
            ProduktionsschrittTO(
                id=x.id, schritt=x.schritt, arbeitsschritt_id=x.arbeitsschritt.id
            )
            for x in produkt.produktionsschritte
        ],
        materialbedarf=[
            MaterialbedarfTO(id=x.id, material_id=x.material.id, menge=x.menge)
            for x in produkt.materialbedarf
        ],
    )


@szenario_router.get(
    "/produkte/",
    response_model=List[ProduktTO],
    status_code=status.HTTP_200_OK,
)
async def read_all_produkte(szenario_id: str, skip: int = 0, take: int = 20):
    async with async_session() as session:
        result = await get_all_produkte(session, skip, take)
        return [convert_to_produktto(x) for x in result]


@szenario_router.get(
    "/produkte/{produkt_id}",
    response_model=ProduktTO,
    status_code=status.HTTP_200_OK,
)
async def read_produkt(szenario_id: str, produkt_id: str):
    async with async_session() as session:
        result = await get_produkt(session, produkt_id)
        return convert_to_produktto(result)


@szenario_router.post(
    "/produkte/",
    response_model=ProduktTO,
    status_code=status.HTTP_201_CREATED,
)
async def create_produkt(szenario_id: str, produkt: ProduktIn):
    async with async_session() as session:
        result = await add_produkt(
            session,
            Produkt(
                id=uuid.uuid4().hex,
                name=produkt.name,
                verkaufspreis=produkt.verkaufspreis,
                produktionsschritte=[
                    Produktionsschritt(
                        id=uuid.uuid4().hex,
                        arbeitsschritt=await get_arbeitsschritt(
                            session, x.arbeitsschritt_id
                        ),
                        schritt=x.schritt,
                    )
                    for x in produkt.produktionsschritte
                ],
                materialbedarf=[
                    Materialbedarf(
                        id=uuid.uuid4().hex,
                        material=await get_material(session, x.material_id),
                        menge=x.menge,
                    )
                    for x in produkt.materialbedarf
                ],
            ),
        )

        return convert_to_produktto(result)


@szenario_router.put(
    "/produkte/{produkt_id}",
    response_model=ProduktTO,
    status_code=status.HTTP_200_OK,
)
async def update_produkt(produkt_id: str, produkt: ProduktIn):
    async with async_session() as session:
        produkt_in_dict = produkt.dict()
        produkt_in_dict.pop("produktionsschritte", None)
        produkt_in_dict.pop("materialbedarf", None)
        result = await edit_produkt(
            session,
            produkt_id,
            Produkt(
                produkt_id,
                **produkt_in_dict,
                produktionsschritte=[
                    Produktionsschritt(
                        id=uuid.uuid4().hex,
                        arbeitsschritt=await get_arbeitsschritt(
                            session, x.arbeitsschritt_id
                        ),
                        schritt=x.schritt,
                    )
                    for x in produkt.produktionsschritte
                ],
                materialbedarf=[
                    Materialbedarf(
                        id=uuid.uuid4().hex,
                        material=await get_material(session, x.material_id),
                        menge=x.menge,
                    )
                    for x in produkt.materialbedarf
                ],
            ),
        )
        return convert_to_produktto(result)


@szenario_router.delete("/produkte/{produkt_id}", status_code=status.HTTP_200_OK)
async def delete_produkt(produkt_id: str):
    async with async_session() as session:
        await remove_produkt(session, produkt_id)
        return {
            "message": "Produkt with id: {} deleted successfully!".format(produkt_id)
        }


class ProduktbedarfIn(APIModel):
    produkt_id: str
    stueckzahl: int


class ChargeIn(APIModel):
    name: str
    prioritaet: int
    produktbedarf: List[ProduktbedarfIn]


class ProduktbedarfTO(APIModel):
    id: str
    produkt_id: str
    stueckzahl: int


class ChargeTO(APIModel):
    id: str
    name: str
    prioritaet: int
    produktbedarf: List[ProduktbedarfTO]


def convert_to_chargeto(charge: Charge) -> ChargeTO:
    return ChargeTO(
        id=charge.id,
        name=charge.name,
        prioritaet=charge.prioritaet,
        produktbedarf=[
            ProduktbedarfTO(id=x.id, produkt_id=x.produkt.id, stueckzahl=x.stueckzahl)
            for x in charge.produktbedarf
        ],
    )


@szenario_router.get(
    "/chargen/",
    response_model=List[ChargeTO],
    status_code=status.HTTP_200_OK,
)
async def read_all_chargen(szenario_id: str, skip: int = 0, take: int = 20):
    async with async_session() as session:
        result = await get_all_chargen(session, skip, take)
        return [convert_to_chargeto(x) for x in result]


@szenario_router.get(
    "/chargen/{charge_id}",
    response_model=ChargeTO,
    status_code=status.HTTP_200_OK,
)
async def read_charge(szenario_id: str, charge_id: str):
    async with async_session() as session:
        result = await get_charge(session, charge_id)
        return convert_to_chargeto(result)


@szenario_router.post(
    "/chargen/",
    response_model=ChargeTO,
    status_code=status.HTTP_201_CREATED,
)
async def create_charge(szenario_id: str, charge: ChargeIn):
    async with async_session() as session:
        result = await add_charge(
            session,
            Charge(
                id=uuid.uuid4().hex,
                name=charge.name,
                prioritaet=charge.prioritaet,
                produktbedarf=[
                    Produktbedarf(
                        id=uuid.uuid4().hex,
                        produkt=await get_produkt(session, x.produkt_id),
                        stueckzahl=x.stueckzahl,
                    )
                    for x in charge.produktbedarf
                ],
            ),
        )

        return convert_to_chargeto(result)


@szenario_router.put(
    "/chargen/{charge_id}",
    response_model=ChargeTO,
    status_code=status.HTTP_200_OK,
)
async def update_charge(charge_id: str, charge: ChargeIn):
    async with async_session() as session:
        charge_in_dict = charge.dict()
        charge_in_dict.pop("produktbedarf", None)
        result = await edit_charge(
            session,
            charge_id,
            Charge(
                charge_id,
                **charge_in_dict,
                produktbedarf=[
                    Produktbedarf(
                        id=uuid.uuid4().hex,
                        produkt=await get_produkt(session, x.produkt_id),
                        stueckzahl=x.stueckzahl,
                    )
                    for x in charge.produktbedarf
                ],
            ),
        )

        return convert_to_chargeto(result)


@szenario_router.delete("/chargen/{charge_id}", status_code=status.HTTP_200_OK)
async def delete_charge(charge_id: str):
    async with async_session() as session:
        await remove_charge(session, charge_id)
        return {"message": "Charge with id: {} deleted successfully!".format(charge_id)}


class MaschinenStueckzahlTO(APIModel):
    maschine: str
    stueckzahl: int


class MaschinenauslastungTO(APIModel):
    maschinen_stueckzahl: MaschinenStueckzahlTO
    arbeitsschritt: str
    auslastung: float


class LeistungsErgebnisTO(APIModel):
    kosten_produkt: float
    produktions_rate: float
    anschaffungskosten: float
    maschinenauslastung: list[MaschinenauslastungTO]


class OptimierungsErgebnisTO(APIModel):
    station: str
    name: str
    charge: str
    gegeben: LeistungsErgebnisTO
    optimiert: LeistungsErgebnisTO


class OptimierungTO(APIModel):
    id: str
    ausfuehrung: str
    produktionslinie: str
    stationen: list[OptimierungsErgebnisTO]


def convert_to_maschinenauslastungto(obj: Maschinenauslastung):
    return MaschinenauslastungTO(
        maschinen_stueckzahl=MaschinenStueckzahlTO(
            maschine=obj.maschinen_stueckzahl.maschine.id,
            stueckzahl=obj.maschinen_stueckzahl.stueckzahl,
        ),
        arbeitsschritt=obj.arbeitsschritt.id,
        auslastung=obj.auslastung,
    )


def convert_to_leistungsergebnisto(obj: LeistungsErgebnis):
    return LeistungsErgebnisTO(
        kosten_produkt=0 if np.isnan(obj.kosten_produkt) else obj.kosten_produkt,
        produktions_rate=obj.produktions_rate,
        anschaffungskosten=obj.anschaffungskosten,
        maschinenauslastung=[
            convert_to_maschinenauslastungto(a) for a in obj.maschinenauslastung
        ],
    )


def convert_to_optimierungsergebnisto(obj: OptimierungsErgebnis):
    return OptimierungsErgebnisTO(
        station=obj.station.id,
        name=obj.station.name,
        charge=obj.station.chargen[0].id,
        gegeben=convert_to_leistungsergebnisto(obj.gegeben),
        optimiert=convert_to_leistungsergebnisto(obj.optimiert),
    )


def convert_to_optimizationto(obj: Optimierung):
    return OptimierungTO(
        id=obj.id,
        ausfuehrung=obj.ausfuehrung,
        produktionslinie=obj.produktionslinie.id,
        stationen=[convert_to_optimierungsergebnisto(s) for s in obj.stationen],
    )


@szenario_router.get(
    "/optimierungen/",
    response_model=List[OptimierungTO],
    status_code=status.HTTP_200_OK,
)
async def read_all_optimierungen(szenario_id: str, skip: int = 0, take: int = 20):
    async with async_session() as session:
        produktionslinie = await get_or_create_produktionslinie(session, "1")
        maschinen = await get_maschinen(session)
        arbeitsschritte = await get_arbeitsschritte(session)

        res = [
            convert_to_optimizationto(
                calc_optimization(
                    produktionslinie=produktionslinie,
                    maschinen=maschinen,
                    arbeitsschritte=arbeitsschritte,
                )
            )
        ]

        return res


app.include_router(szenario_router)
