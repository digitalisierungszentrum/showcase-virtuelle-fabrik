from typing import List
import uuid
from attr import asdict, define
from fastapi import FastAPI
import socketio
import asyncio
from virtuelle_fabrik.domain.exception import DomainException, ProblemInfeasibleException

from virtuelle_fabrik.domain.models import (
    MaschinenStueckzahl,
    Produktionslinie,
    Station,
    Optimierung,
    OptimierungsErgebnis,
    LeistungsErgebnis,
    Maschinenauslastung,
    EditorNotification,
    NotificationType
)
from virtuelle_fabrik.optimization.controller import (
    calc_optimization,
    validate_station,
)

from virtuelle_fabrik.persistence.charge import get_charge
from virtuelle_fabrik.persistence.database import async_session
from virtuelle_fabrik.persistence.maschinen import get_maschine, get_maschinen
from virtuelle_fabrik.persistence.produkte import get_arbeitsschritte
from virtuelle_fabrik.persistence.produktionslinien import (
    add_produktionslinie,
    get_or_create_produktionslinie,
    update_produktionslinie,
)


@define
class MaschinenStueckzahlTO:
    maschine_id: str
    stueckzahl: int


@define
class StationTO:
    id: str
    name: str
    order: int
    maschinen_stueckzahl: List[MaschinenStueckzahlTO]
    chargen: List[str]
    budget_optimierung: float


@define
class ProduktionslinieTO:
    id: str
    stationen: List[StationTO]

@define
class MaschinenStueckzahlTO2:
    maschine: str
    stueckzahl: int

@define 
class MaschinenauslastungTO:
    maschinenStueckzahl: MaschinenStueckzahlTO2
    arbeitsschritt: str
    auslastung: float

@define
class LeistungsErgebnisTO:
    kostenProdukt: float
    maschinenauslastung: List[MaschinenauslastungTO]
    produktionsRate: float
    anschaffungskosten: float

@define 
class OptimierungsErgebnisTO:
    station: str
    name: str
    charge: str
    produkt: str
    stueckzahl: int
    gegeben: LeistungsErgebnisTO
    optimiert: LeistungsErgebnisTO

@define
class OptimierungTO:
    stationen: List[OptimierungsErgebnisTO]


def convert_to_produktionslinieto(
    produktionslinie: Produktionslinie,
) -> ProduktionslinieTO:
    return ProduktionslinieTO(
        id=produktionslinie.id,
        stationen=[
            StationTO(
                id=x.id,
                name=x.name,
                order=x.order,
                maschinen_stueckzahl=list(
                    [
                        MaschinenStueckzahlTO(maschine_id=m.maschine.id, stueckzahl=m.stueckzahl)
                        for m in x.maschinen_stueckzahl
                    ]
                ),
                chargen=list([c.id for c in x.chargen]),
                budget_optimierung=x.budget_optimierung
            )
            for x in produktionslinie.stationen
        ],
    )

def dict_to_produktionslinieto(
    produktionslinie: dict,
) -> ProduktionslinieTO:
    return ProduktionslinieTO(
        id=produktionslinie["id"],
        stationen=[
            StationTO(
                id=x["id"],
                name=x["name"],
                order=x["order"],
                maschinen_stueckzahl=list(
                    [
                        MaschinenStueckzahlTO(maschine_id=m["maschine_id"], stueckzahl=m["stueckzahl"])
                        for m in x["maschinen_stueckzahl"]
                    ]
                ),
                chargen=list(x["chargen"]),
                budget_optimierung=x["budget_optimierung"],
            )
            for x in produktionslinie["stationen"]
        ],
    )

def convert_to_maschinenauslastungto(
        maschinenAuslastung: Maschinenauslastung
) -> MaschinenauslastungTO:
    return MaschinenauslastungTO(
        maschinenStueckzahl=MaschinenStueckzahlTO2(
            maschinenAuslastung.maschinen_stueckzahl.maschine.name, 
            maschinenAuslastung.maschinen_stueckzahl.stueckzahl, 
        ),
        arbeitsschritt=maschinenAuslastung.arbeitsschritt.name,
        auslastung=maschinenAuslastung.auslastung,
    )

def convert_to_leistungsergebnisto(
        leistungsErgebnis: LeistungsErgebnis
) -> LeistungsErgebnisTO:
    return LeistungsErgebnisTO(
        kostenProdukt=leistungsErgebnis.kosten_produkt,
        maschinenauslastung=list([convert_to_maschinenauslastungto(ma) for ma in leistungsErgebnis.maschinenauslastung]),
        produktionsRate=leistungsErgebnis.produktions_rate,
        anschaffungskosten=leistungsErgebnis.anschaffungskosten,
    )

def convert_to_optimierungsergebnisto(
        optimierungsErgebnis: OptimierungsErgebnis,
        charge: str,
        produkt: str,
        stueckzahl: int
) -> OptimierungsErgebnisTO:
    return OptimierungsErgebnisTO(
        station=optimierungsErgebnis.station.name,
        name="Optimierungsergebnis",
        charge=charge,
        produkt=produkt,
        stueckzahl=stueckzahl,
        gegeben=convert_to_leistungsergebnisto(optimierungsErgebnis.gegeben),
        optimiert=convert_to_leistungsergebnisto(optimierungsErgebnis.optimiert),
    )

def convert_to_optimierungto(
        optimierung: Optimierung,
) -> OptimierungTO:
    charge = optimierung.produktionslinie.stationen[0].chargen[0].name
    produkt = optimierung.produktionslinie.stationen[0].chargen[0].produktbedarf[0].produkt.name
    stueckzahl = optimierung.produktionslinie.stationen[0].chargen[0].produktbedarf[0].stueckzahl
    return OptimierungTO(
        stationen=list([convert_to_optimierungsergebnisto(station, charge, produkt, stueckzahl) for station in optimierung.stationen]),
    )

class EditorNamespace(socketio.AsyncNamespace):
    _task: asyncio.Task | None = None

    _id: str = "1"

    async def run_optimization(self, produktionslinie: Produktionslinie):
        async with async_session() as session:
            simulation_id = "1"
            await self.emit("simulationRunning", simulation_id)

            maschinen = await get_maschinen(session)
            arbeitsschritte = await get_arbeitsschritte(session)

            try:
                optimization = calc_optimization(
                    produktionslinie=produktionslinie,
                    maschinen=maschinen,
                    arbeitsschritte=arbeitsschritte,
                )
            except ProblemInfeasibleException:
                await self.emit(
                    "simulationInfeasible",
                    data=(simulation_id),
                )
                return


            # TODO: Optimization result has to be persisted

            await self.emit(
                "simulationFinished",
                data=(
                    simulation_id,
                    asdict(
                        EditorNotification(
                            details="Die Simulation war erfolgreich!",
                            type=NotificationType.SUCCESS,
                        )
                    ),
                    asdict(convert_to_optimierungto(optimization)),
                ),
            )

    async def on_connect(self, sid, *args):
        async with async_session() as session:
            await self.emit(
                "produktionslinieChanged",
                asdict(
                    convert_to_produktionslinieto(
                        await get_or_create_produktionslinie(session, self._id)
                    ),
                ),
                to=sid,
            )

    def on_disconnect(self, sid):
        pass

    async def on_changeProduktionslinie(self, sid, produktionslinieto_dict: dict):
        async with async_session() as session:
            produktionslinieto = dict_to_produktionslinieto(produktionslinieto_dict)
            def synchronous_get_all(getter, list):
                results = [getter(session, x) for x in list]
                return asyncio.gather(*results)

            produktionslinie = Produktionslinie(
                id=produktionslinieto.id,
                stationen=[
                    Station(
                        id=s.id,
                        name=s.name,
                        order=s.order,
                        maschinen_stueckzahl=[
                            MaschinenStueckzahl(
                                maschine=await get_maschine(session, ms.maschine_id),
                                stueckzahl=ms.stueckzahl,
                            )
                            for ms in s.maschinen_stueckzahl
                        ],
                        chargen=await synchronous_get_all(get_charge, s.chargen),
                        budget_optimierung=s.budget_optimierung,
                    )
                    for s in produktionslinieto.stationen
                ],
            )

            await update_produktionslinie(session, produktionslinie)
            await self.emit("produktionslinieChanged", produktionslinieto, skip_sid=sid)

    async def on_validateProduktionslinie(
        self, sid, produktionslinieto_dict
    ) -> list[dict]:
        async with async_session() as session:
            produktionslinieto = dict_to_produktionslinieto(produktionslinieto_dict)

            def synchronous_get_all(getter, list):
                results = [getter(session, x) for x in list]
                return asyncio.gather(*results)

            produktionslinie = Produktionslinie(
                id=produktionslinieto.id,
                stationen=[
                    Station(
                        id=s.id,
                        name=s.name,
                        order=s.order,
                        maschinen_stueckzahl=[
                            MaschinenStueckzahl(
                                maschine=await get_maschine(session, ms.maschine_id),
                                stueckzahl=ms.stueckzahl,
                            )
                            for ms in s.maschinen_stueckzahl
                        ],
                        chargen=await synchronous_get_all(get_charge, s.chargen),
                        budget_optimierung=s.budget_optimierung,
                    )
                    for s in produktionslinieto.stationen
                ],
            )
            
            maschinen = await get_maschinen(session)
            arbeitsschritte = await get_arbeitsschritte(session)

            errors: List[EditorNotification] = []
            for s in produktionslinie.stationen:
                try:
                    errors = errors + validate_station(
                        station=s,
                        maschinen=maschinen,
                        arbeitsschritte=arbeitsschritte,
                    )
                except DomainException as ex:
                    errors.append(EditorNotification(
                        id=uuid.uuid4().hex,
                        title="Unvollständige Daten",
                        details=ex.message,
                        type=NotificationType.ERROR,
                        showClose=True,
                    ))

            return [asdict(error) for error in errors]

    async def on_startSimulation(self, sid, produktionslinieto_dict: dict):
        if self._task and not self._task.cancelled():
            self._task.cancel()

        async with async_session() as session:
            produktionslinieto = dict_to_produktionslinieto(produktionslinieto_dict)

            def synchronous_get_all(getter, list):
                results = [getter(session, x) for x in list]
                return asyncio.gather(*results)

            produktionslinie = Produktionslinie(
                id=produktionslinieto.id,
                stationen=[
                    Station(
                        id=s.id,
                        name=s.name,
                        order=s.order,
                        maschinen_stueckzahl=[
                            MaschinenStueckzahl(
                                maschine=await get_maschine(session, ms.maschine_id),
                                stueckzahl=ms.stueckzahl,
                            )
                            for ms in s.maschinen_stueckzahl
                        ],
                        chargen=await synchronous_get_all(get_charge, s.chargen),
                        budget_optimierung=s.budget_optimierung,
                    )
                    for s in produktionslinieto.stationen
                ],
            )

        asyncio.create_task(self.run_optimization(produktionslinie))

    async def on_cancelSimulation(self, sid):
        if not self._task:
            return

        cancelled = self._task.cancel()

        if cancelled:
            await self.emit("simulationCanceled")


def setupWebsocket(app: FastAPI):
    sio = socketio.AsyncServer(async_mode="asgi", cors_allowed_origins=[])
    _app = socketio.ASGIApp(socketio_server=sio, socketio_path="socket.io")
    app.mount("/api/ws", _app)
    app.sio = sio  # type: ignore[attr-defined]

    sio.register_namespace(EditorNamespace("/szenarios/1/editor/"))
