import asyncio
from typing import List, Sequence

from attr import asdict
from sqlalchemy import Column, Float, Integer, String, ForeignKey, Table, delete, select
from sqlalchemy.orm import relationship, mapped_column, Mapped
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.exc import NoResultFound
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.dialects.postgresql import insert

from .database import Base
from .maschinen import (
    MaschineEntity,
    _get_machine_entity,
    convert_to_maschine,
)
from .charge import ChargeEntity, convert_to_charge, get_charge

from virtuelle_fabrik.domain.models import (
    MaschinenStueckzahl,
    Produktionslinie,
    Station,
)
from virtuelle_fabrik.domain.exception import DomainException


station_charge_association_table = Table(
    "station_charge_association_table",
    Base.metadata,
    Column("station_id", String, ForeignKey("stationen.id"), primary_key=True),
    Column("charge_id", String, ForeignKey("charge.id"), primary_key=True),
)


class MaschinenStueckzahlEntity(Base):
    __tablename__ = "maschinenstueckzahl"

    maschine_id: Mapped[str] = mapped_column(
        ForeignKey("maschinen.id"), primary_key=True
    )
    maschine: Mapped["MaschineEntity"] = relationship(lazy="joined")
    stueckzahl: int = Column("stueckzahl", Integer)
    station_id: Mapped[str] = mapped_column(
        ForeignKey("stationen.id", ondelete="CASCADE"), primary_key=True
    )

    __table_args__ = (
        UniqueConstraint("station_id", "maschine_id", name="uq_foreign_keys"),
    )


class StationEntity(Base):
    __tablename__ = "stationen"

    id: str = Column(String, primary_key=True)
    name: str = Column(String)
    order: int = Column(Integer, nullable=False)
    maschinen_stueckzahl: Mapped[List["MaschinenStueckzahlEntity"]] = relationship(
        lazy="joined",
        cascade="all, delete-orphan",
    )
    chargen: Mapped[List["ChargeEntity"]] = relationship(
        secondary=station_charge_association_table, lazy="joined"
    )
    produktionslinie_id: Mapped[str] = mapped_column(ForeignKey("produktionslinien.id"))
    budget_optimierung: float = Column(Float, nullable=False)


class ProduktionslinieEntity(Base):
    __tablename__ = "produktionslinien"

    id: str = Column(String, primary_key=True)
    stationen: Mapped[List["StationEntity"]] = relationship(
        lazy="joined", cascade="all, delete-orphan"
    )


# define persistence interface + implementation here


def convert_to_maschinen_stueckzahl(
    entity: MaschinenStueckzahlEntity,
) -> MaschinenStueckzahl:
    return MaschinenStueckzahl(
        maschine=convert_to_maschine(entity.maschine),
        stueckzahl=entity.stueckzahl,
    )


def convert_to_station(entity: StationEntity) -> Station:
    return Station(
        id=entity.id,
        name=entity.name,
        order=entity.order,
        maschinen_stueckzahl=list(
            [convert_to_maschinen_stueckzahl(x) for x in entity.maschinen_stueckzahl]
        ),
        chargen=list([convert_to_charge(x) for x in entity.chargen]),
        budget_optimierung=entity.budget_optimierung,
    )


async def get_all_stationen(
    session: AsyncSession, skip: int = 0, take: int = 20
) -> Sequence[Station]:
    query = await session.execute(select(StationEntity).offset(skip).limit(take))

    return [convert_to_station(p) for p in query.scalars().unique().all()]


async def get_station(session: AsyncSession, station_id: str) -> Station:
    query = await session.execute(
        select(StationEntity).filter(StationEntity.id == station_id)
    )
    try:
        station_entity = query.scalars().unique().one()
        await session.commit()
        return convert_to_station(station_entity)
    except NoResultFound:
        raise DomainException(message=f"Station with id {station_id} not found!")


async def add_station(session: AsyncSession, station: Station) -> Station:
    new_station = StationEntity(
        id=station.id,
        name=station.name,
        order=station.order,
        maschinen_stueckzahl=list(
            [
                MaschinenStueckzahlEntity(
                    maschine=await _get_machine_entity(session, x.maschine.id),
                    stueckzahl=x.stueckzahl,
                )
                for x in station.maschinen_stueckzahl
            ]
        ),
        chargen=list([await get_charge(session, x.id) for x in station.chargen]),
        budget_optimierung=station.budget_optimierung,
    )

    session.add(new_station)
    await session.commit()

    return station


async def remove_station(session: AsyncSession, station_id: str) -> None:
    row = await session.execute(
        select(StationEntity).where(StationEntity.id == station_id)
    )
    try:
        row = row.unique().scalar_one()
    except NoResultFound:
        raise DomainException(message=f"Station with id {station_id} not found!")
    await session.delete(row)
    await session.commit()


async def get_or_create_produktionslinie(
    session: AsyncSession, produktionslinie_id: str
) -> Produktionslinie:
    try:
        return await get_produktionslinie(session, produktionslinie_id)

    except DomainException:
        produktionslinie = Produktionslinie(id=produktionslinie_id, stationen=[])

        session.add(ProduktionslinieEntity(**asdict(produktionslinie)))
        await session.commit()

        return produktionslinie


async def get_produktionslinie(
    session: AsyncSession, produktionslinie_id: str
) -> Produktionslinie:
    query = await session.execute(
        select(ProduktionslinieEntity).filter(
            ProduktionslinieEntity.id == produktionslinie_id
        )
    )
    try:
        produktionslinie_entity = query.scalars().unique().one()
        return Produktionslinie(
            id=produktionslinie_entity.id,
            stationen=list(
                [
                    Station(
                        id=x.id,
                        name=x.name,
                        order=x.order,
                        maschinen_stueckzahl=list(
                            [
                                convert_to_maschinen_stueckzahl(m)
                                for m in x.maschinen_stueckzahl
                            ]
                        ),
                        chargen=list([convert_to_charge(c) for c in x.chargen]),
                        budget_optimierung=x.budget_optimierung,
                    )
                    for x in produktionslinie_entity.stationen
                ]
            ),
        )
    except NoResultFound:
        raise DomainException(
            message=f"Produktionslinie with id {produktionslinie_id} not found!"
        )


async def get_maschine_entities(session: AsyncSession, maschine_ids: List[str]):
    query = await session.execute(
        select(MaschineEntity).filter(MaschineEntity.id.in_(maschine_ids))
    )
    return query.scalars().unique().all()


async def get_maschine_stueckzahl_entities(session: AsyncSession, station_id: str):
    query = await session.execute(
        select(MaschinenStueckzahlEntity).filter(
            MaschinenStueckzahlEntity.station_id == station_id
        )
    )
    return query.scalars().unique().all()


async def update_maschine_stueckzahl_entities(session: AsyncSession, station: Station):
    # first delete all maschinenstueckzahlen that belong to this station
    await session.execute(
        delete(MaschinenStueckzahlEntity).where(MaschinenStueckzahlEntity.station_id == station.id)
    )
    # then add the current state
    if(station.maschinen_stueckzahl):
        stmt = insert(MaschinenStueckzahlEntity).values(
            [
                {
                    "maschine_id": ms.maschine.id,
                    "station_id": station.id,
                    "stueckzahl": ms.stueckzahl,
                }
                for ms in station.maschinen_stueckzahl
            ]
        )
        await session.execute(stmt)

    return await get_maschine_stueckzahl_entities(session, station.id)


async def get_charge_entities(session: AsyncSession, chargen_ids: List[str]):
    query = await session.execute(
        select(ChargeEntity).filter(ChargeEntity.id.in_(chargen_ids))
    )
    return query.scalars().unique().all()


async def add_produktionslinie(
    session: AsyncSession, produktionslinie: Produktionslinie
) -> Produktionslinie:
    new_produktionslinie = ProduktionslinieEntity(
        id=produktionslinie.id,
        stationen=list(
            [
                StationEntity(
                    id=s.id,
                    name=s.name,
                    order=s.order,
                    maschinen_stueckzahl=await get_maschine_stueckzahl_entities(
                        session,
                        s.id,
                    ),
                    chargen=await get_charge_entities(
                        session,
                        [x.id for x in s.chargen],
                    ),
                    budget_optimierung=s.budget_optimierung,
                )
                for s in produktionslinie.stationen
            ]
        ),
    )
    session.add(new_produktionslinie)
    await session.commit()
    return produktionslinie


async def update_produktionslinie(
    session: AsyncSession, produktionslinie: Produktionslinie
) -> Produktionslinie:

    produktionslinie_entity = await session.get(
        ProduktionslinieEntity, produktionslinie.id
    )

    if not produktionslinie_entity:
        raise DomainException(
            message=f"Produktionslinie with id {produktionslinie.id} not found!"
        )

    stationen = [
        StationEntity(
            id=s.id,
            name=s.name,
            order=s.order,
            maschinen_stueckzahl=await update_maschine_stueckzahl_entities(session, s),
            chargen=await get_charge_entities(
                session,
                [x.id for x in s.chargen],
            ),
            budget_optimierung=s.budget_optimierung,
        )
        for s in produktionslinie.stationen
    ]

    stationen = await asyncio.gather(*[session.merge(s) for s in stationen])

    produktionslinie_entity.stationen = list(stationen)

    await session.merge(produktionslinie_entity)

    await session.commit()
    return produktionslinie
