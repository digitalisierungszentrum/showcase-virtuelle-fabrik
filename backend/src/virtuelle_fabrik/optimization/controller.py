from datetime import datetime
from typing import List
import uuid
import pyomo.environ as pyo
from pyomo.opt import SolverFactory, TerminationCondition

from virtuelle_fabrik.domain.exception import DomainException, ProblemInfeasibleException
from virtuelle_fabrik.domain.models import (
    LeistungsErgebnis,
    Maschine,
    Arbeitsschritt,
    MaschinenStueckzahl,
    Maschinenauslastung,
    Optimierung,
    OptimierungsErgebnis,
    Produkt,
    Produktionslinie,
    Station,
    EditorNotification,
    NotificationType,
)

def validate_station(
    station: Station, maschinen: List[Maschine], arbeitsschritte: List[Arbeitsschritt]
) -> List[EditorNotification]:
    if len(station.chargen) < 1:
        raise DomainException(
            message=f"Bitte fügen Sie eine Charge zur Station '{station.name}' hinzu."
        )
    
    if len(maschinen) < 1:
        raise DomainException(
            message="Bitte fügen Sie den Stammdaten Maschinen hinzu."
        )
    
    if len(arbeitsschritte) < 1:
        raise DomainException(
            message="Bitte fügen Sie den Stammdaten Arbeitsschritte hinzu."
        )
    
    # maybe also check maschinen and arbeitsschritte
    
    unzulaessigkeiten = []
    
    produkt = station.chargen[0].produktbedarf[0].produkt
    stueckzahl = station.chargen[0].produktbedarf[0].stueckzahl

    model = get_model(produkt, stueckzahl, station.budget_optimierung, maschinen, arbeitsschritte)
    
    def maschinen_anzahl_init(maschine):
        for m in station.maschinen_stueckzahl:
            if m.maschine.id == maschine.id:
                return m.stueckzahl
        return 0

    maschinen_anzahl = [maschinen_anzahl_init(m) for m in maschinen]

    if sum(model.produkt_arbeitsschritte[a] for a in model.Arbeitsschritte) <= 0:
        raise DomainException(
            message=f"Bitte fügen Sie Arbeitsschritte zum Produkt '{produkt.name}' hinzu."
        )
    
    missing_machines = []
    seperator = ", "
    for a in model.Arbeitsschritte:
        if model.produkt_arbeitsschritte[a] > 0 and sum(model.maschinen_taktraten[m, a] for m in model.Maschinen) <= 0:
            missing_machines.append(arbeitsschritte[a].name)
    if len(missing_machines) > 0:
        raise DomainException(
            message=f"Bitte fügen Sie eine Maschine für folgende Arbeitschritte hinzu: '{seperator.join(missing_machines)}'."
        )

    uncovered_steps = []
    for a in model.Arbeitsschritte:
        if model.produkt_arbeitsschritte[a] > 0 and sum(model.maschinen_taktraten[m, a] * maschinen_anzahl[m] for m in model.Maschinen) / model.produkt_arbeitsschritte[a] < stueckzahl:
            uncovered_steps.append(arbeitsschritte[a].name)

    if len(uncovered_steps):
        unzulaessigkeiten.append(EditorNotification(
            id=uuid.uuid4().hex,
            title="Station " + station.name + " ist unzulässig.",
            details="Die folgenden Arbeitsschritte erfüllen nicht die geforderte Produktionsrate: " + ", ".join(uncovered_steps),
            type=NotificationType.WARN,
            showClose=True,
        ))

    return unzulaessigkeiten


def calc_per_station(
    station: Station, maschinen: List[Maschine], arbeitsschritte: List[Arbeitsschritt]
) -> OptimierungsErgebnis:
    if len(station.chargen) < 1:
        raise DomainException(
            message=f"station '{station.name}' does not have a Charge assigned"
        )

    produkt = station.chargen[0].produktbedarf[0].produkt
    stueckzahl = station.chargen[0].produktbedarf[0].stueckzahl

    current_leistungsergebnis, opt_leistungsergebnis = solve_minlp(produkt, stueckzahl, station.budget_optimierung, maschinen, arbeitsschritte, station.maschinen_stueckzahl)

    return OptimierungsErgebnis(
        station=station,
        gegeben=current_leistungsergebnis,
        optimiert=opt_leistungsergebnis,
    )


def calc_optimization(
    produktionslinie: Produktionslinie,
    maschinen: List[Maschine],
    arbeitsschritte: List[Arbeitsschritt],
):
    optimization = Optimierung(
        id="1",
        ausfuehrung=datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
        produktionslinie=produktionslinie,
        stationen=[
            calc_per_station(s, maschinen, arbeitsschritte)
            for s in produktionslinie.stationen
        ],
    )

    return optimization


def solve_minlp(produkt: Produkt, stueckzahl: int, budget: float, maschinen: List[Maschine], arbeitsschritte: List[Arbeitsschritt], current: List[MaschinenStueckzahl]):

    # Build pyomo model
    model = get_model(produkt, stueckzahl, budget, maschinen, arbeitsschritte)

    # Compute solution with current data
    def current_maschinen_anzahl_init(maschine):
        for m in current:
            if m.maschine.id == maschine.id:
                return m.stueckzahl
        return 0

    current_maschinen_anzahl = [current_maschinen_anzahl_init(m) for m in maschinen]
    current_taktrate_gesamt = min(sum(model.maschinen_taktraten[m, a] * current_maschinen_anzahl[m] for m in model.Maschinen) / model.produkt_arbeitsschritte[a] for a in model.Arbeitsschritte if model.produkt_arbeitsschritte[a] > 0)
    current_produkt_kosten = sum(model.maschinen_kosten_minute[m] * current_maschinen_anzahl[m] for m in model.Maschinen) / current_taktrate_gesamt if current_taktrate_gesamt > 0 else 0

    # Initialize model with current solution
    model.taktrate_gesamt = current_taktrate_gesamt
    model.produkt_kosten = current_produkt_kosten
    for m in model.Maschinen:
        model.maschinen_anzahl[m] = current_maschinen_anzahl[m]

    # Compute current Leistungsergebnis
    current_leistungsergebnis = LeistungsErgebnis(
        kosten_produkt=round(current_produkt_kosten, 2),
        produktions_rate=current_taktrate_gesamt,
        anschaffungskosten=sum(current_maschinen_anzahl[m] * model.maschinen_anschaffungskosten[m] for m in model.Maschinen),
        maschinenauslastung=compute_maschine_usages(model, maschinen, arbeitsschritte),
    )

    # Solve the minlp
    opt = SolverFactory("gurobi")
    opt.options[
        "NonConvex"
    ] = 2  # required by gurobi since the the quadratic matrix of the non-linear constraint is not positive-semidefinite
    result = opt.solve(model, warmstart=True)

    if result.solver.termination_condition == TerminationCondition.infeasible:
        raise ProblemInfeasibleException(message="The problem was proven to be infeasible by the solver.")

    # Compute optimal Leistungsergebnis
    opt_leistungsergebnis = LeistungsErgebnis(
        kosten_produkt=round(pyo.value(model.produkt_kosten), 2),
        produktions_rate=pyo.value(model.taktrate_gesamt),
        anschaffungskosten=sum(pyo.value(model.maschinen_anzahl[m]) * model.maschinen_anschaffungskosten[m] for m in model.Maschinen),
        maschinenauslastung=compute_maschine_usages(model, maschinen, arbeitsschritte),
    )

    return current_leistungsergebnis, opt_leistungsergebnis

def get_model(produkt: Produkt, stueckzahl: int, budget: float, maschinen: List[Maschine], arbeitsschritte: List[Arbeitsschritt]) -> pyo.ConcreteModel:
    
    model = pyo.ConcreteModel()

    # Sets to index parameters and variables
    model.Arbeitsschritte = pyo.RangeSet(0, len(arbeitsschritte) - 1)
    model.Maschinen = pyo.RangeSet(0, len(maschinen) - 1)

    # Init methods for the parameters
    def maschinen_taktraten_init(model, m, a):
        for mb in maschinen[m].maschinenbefaehigungen:
            if mb.schritt_id == arbeitsschritte[a].id:
                return mb.taktrate

        return 0

    def produkt_arbeitsschritte_init(model, a):
        counter = 0
        for ps in produkt.produktionsschritte:
            if ps.arbeitsschritt.id == arbeitsschritte[a].id:
                counter = (
                    counter + 1
                )  # maybe, later on, there's a quantity linked to a product-step relation: counter + ps.anzahl

        return counter

    # Parameters given by the input
    model.maschinen_kosten_minute = pyo.Param(model.Maschinen, initialize=[m.kosten_minute for m in maschinen])
    model.maschinen_anschaffungskosten = pyo.Param(model.Maschinen, initialize=[m.anschaffungskosten for m in maschinen])
    model.maschinen_taktraten = pyo.Param(model.Maschinen * model.Arbeitsschritte, initialize=maschinen_taktraten_init)
    model.produkt_arbeitsschritte = pyo.Param(model.Arbeitsschritte, initialize=produkt_arbeitsschritte_init)
    model.ueberschuss_skalierung = pyo.Param(default=1e-3)

    # Variables that will be optimized
    model.taktrate_gesamt = pyo.Var(
        domain=pyo.Reals, bounds=(stueckzahl, None)
    )  # or as constraint? Should be equivalent for the optimization but not for the initialization.
    model.produkt_kosten = pyo.Var(domain=pyo.NonNegativeReals)
    model.maschinen_anzahl = pyo.Var(
        model.Maschinen, domain=pyo.NonNegativeIntegers
    )  # maybe later we have upper bounds on the machines: ub=get_maschinen_ub

    # Constraints
    model.nonlinear_constraint = pyo.Constraint(rule = lambda model : pyo.dot_product(model.maschinen_anzahl, model.maschinen_kosten_minute, index=model.Maschinen) <= model.produkt_kosten * model.taktrate_gesamt)
    model.taktraten_constraint = pyo.Constraint(model.Arbeitsschritte, rule = lambda model, a : model.taktrate_gesamt * model.produkt_arbeitsschritte[a] <= sum(model.maschinen_anzahl[m] * model.maschinen_taktraten[m,a] for m in model.Maschinen))
    model.budget_constraint = pyo.Constraint(rule = lambda model : pyo.dot_product(model.maschinen_anzahl, model.maschinen_anschaffungskosten, index=model.Maschinen) <= budget)

    # Objective
    model.objective = pyo.Objective(
        rule=lambda model: model.produkt_kosten
        + model.ueberschuss_skalierung * (model.taktrate_gesamt - stueckzahl),
        sense=pyo.minimize,
    )

    return model


def compute_maschine_usages(
    model: pyo.ConcreteModel,
    maschinen: List[Maschine],
    arbeitsschritte: List[Arbeitsschritt],
) -> List[Maschinenauslastung]:
    maschinen_auslastungen: List[Maschinenauslastung] = []

    for a in model.Arbeitsschritte: # if a machine has multiple skills then it will also appear multiple times
        if model.produkt_arbeitsschritte[a] > 0:
            taktrate_arbeitsschritt = sum(pyo.value(model.maschinen_anzahl[m]) * model.maschinen_taktraten[m, a] for m in model.Maschinen) / model.produkt_arbeitsschritte[a]
            maschinen_auslastungen = maschinen_auslastungen + [
                Maschinenauslastung(
                    maschinen_stueckzahl=MaschinenStueckzahl(maschinen[m], round(pyo.value(model.maschinen_anzahl[m]))),
                    arbeitsschritt=arbeitsschritte[a],
                    auslastung=round(pyo.value(model.taktrate_gesamt)/taktrate_arbeitsschritt, 2),
                ) for m in model.Maschinen if model.maschinen_taktraten[m, a] > 0 and pyo.value(model.maschinen_anzahl[m]) > 0
            ]

    return maschinen_auslastungen
