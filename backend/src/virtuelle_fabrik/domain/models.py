from enum import Enum
from typing import List
from attr import define


@define
class MaschinenBefaehigung:
    id: str
    schritt_id: str
    taktrate: float


@define
class Maschine:
    id: str
    name: str
    ruestzeit: float
    kosten_minute: float
    anschaffungskosten: float
    ausfall_wahrscheinlichkeit: float
    mitarbeiter_min: int
    mitarbeiter_max: int
    maschinenbefaehigungen: List[MaschinenBefaehigung]


def no_maschinenbefaehigungen(a, _):
    return a.name != ("maschinenbefaehigungen")


@define
class Material:
    id: str
    name: str
    kosten_stueck: float
    bestand: float
    aufstocken_minute: float


@define
class Materialbedarf:
    id: str
    material: Material
    menge: float


@define
class Arbeitsschritt:
    id: str
    name: str


@define
class Produktionsschritt:
    id: str
    arbeitsschritt: Arbeitsschritt
    schritt: int


@define
class Produkt:
    id: str
    name: str
    verkaufspreis: float
    produktionsschritte: List[Produktionsschritt]
    materialbedarf: List[Materialbedarf]


def produkt_without_relationships(a, _):
    return a.name not in set(("produktionsschritte", "materialbedarf"))


@define
class Produktbedarf:
    id: str
    produkt: Produkt
    stueckzahl: int


@define
class Charge:
    id: str
    name: str
    prioritaet: int
    produktbedarf: List[Produktbedarf]

@define
class MaschinenStueckzahl:
    maschine: Maschine
    stueckzahl: int

@define
class Station:
    id: str
    name: str
    order: int
    maschinen_stueckzahl: List[MaschinenStueckzahl]
    chargen: List[Charge]
    budget_optimierung: float


@define
class Produktionslinie:
    id: str
    stationen: List[Station]


@define
class Maschinenauslastung:
    maschinen_stueckzahl: MaschinenStueckzahl
    arbeitsschritt: Arbeitsschritt
    auslastung: float


@define
class LeistungsErgebnis:
    kosten_produkt: float
    produktions_rate: float
    anschaffungskosten: float
    maschinenauslastung: List[Maschinenauslastung]


@define
class OptimierungsErgebnis:
    station: Station
    gegeben: LeistungsErgebnis
    optimiert: LeistungsErgebnis


@define
class Optimierung:
    id: str
    ausfuehrung: str
    produktionslinie: Produktionslinie
    stationen: List[OptimierungsErgebnis]


class NotificationType(str, Enum):
    ERROR = "Error"
    WARN = "Warn"
    SUCCESS = "Success"
    INFO = "Info"


@define
class EditorNotification:
    title: str | None = None
    id: str | None = None
    details: str | None = None
    type: NotificationType | None = None
    showClose: bool | None = None
