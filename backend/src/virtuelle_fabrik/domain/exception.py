
from attr import define


@define
class DomainException(Exception):
    message: str

@define
class ProblemInfeasibleException(Exception):
    message: str
