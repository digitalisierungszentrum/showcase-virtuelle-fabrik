import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    setupNodeEvents() {
      // implement node event listeners here
    },
  },
  retries: {
    // Configure retry attempts for `cypress run`
    runMode: 3,
    // Configure retry attempts for `cypress open`
    openMode: 0,
  },
});
