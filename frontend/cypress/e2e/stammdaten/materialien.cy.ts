import * as materialien from "../../fixtures/materialien.json";

const material = materialien[0];

describe(
    "Materialien Masken Tests",
    {
      baseUrl: `${Cypress.env("CYPRESS_HOST")}/szenario/1/stammdaten`,
      includeShadowDom: true,
    },
    () => {
        beforeEach(() => {
            cy.on("uncaught:exception", (e) => {
              return false;
            });
      
            cy.registerStammdatenFixtures();
            cy.intercept("GET", "/api/**/materialien/*", { body: material });
            cy.intercept("PUT", "/api/**/materialien/*", {});
            cy.intercept("POST", "/api/**/materialien/*", {});
            cy.intercept("DELETE", "/api/**/materialien/*", {});
          });

          it("Navigation funktioniert", () => {
            cy.visit(`/material/${material.id}?view=material`);
            cy.wait(2000);
      
            cy.get("ui5-button").contains("Bearbeiten").click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/material/${material.id}/edit`
            );
      
            cy.get("ui5-button").contains("Speichern").click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/material/${material.id}`
            );
      
            cy.wait(100);
            cy.findByRole("button", { name: "Schließen" }).click();
            cy.location("pathname").should("eq", "/szenario/1/stammdaten");
      
            cy.wait(100);
            cy.findByRole("button", { name: "neues Material" }).click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/material/new`
            );
          });

        it("Material Details - Alle Informationen des Materials werden angezeigt", () => {
            cy.visit(`/material/${material.id}`);
            cy.wait(500);

            cy.get("ui5-title")
            .contains(material.name)
            .should("be.visible");

            cy.get("ui5-label")
            .contains("ID")
            .next()
            .contains(material.id)
            .should("be.visible");

            cy.get("ui5-label")
            .contains("Kosten/Stück")
            .next()
            .contains(material.kostenStueck)
            .should("be.visible");
    
            cy.get("ui5-label")
            .contains("Lagerbestand")
            .next()
            .contains(material.bestand)
            .should("be.visible");
    
            cy.get("ui5-label")
            .contains("Aufstocken/Minute")
            .next()
            .contains(material.aufstockenMinute)
            .should("be.visible");
    
            
        })

        it("Material neu - speichert korrekte daten", () => {
            cy.visit(`/material/new`);
            cy.wait(500);
      
            const material = materialien[0];
      
            cy.intercept("POST", "/api/**/materialien", {
              body: { id: material.id },
            }).as("post");
      
            
            cy.fillInput("Material", material.name);
            cy.fillInput("Kosten/Stück", material.kostenStueck);
            cy.fillInput("Lagerbestand", material.bestand);
            cy.fillInput("Aufstocken/Minute", material.aufstockenMinute);
      
            
            
               
            cy.findByRole("button", { name: "Speichern" }).click();
      
            cy.wait("@post").then((req) => {
              const expected = { ...material };
              delete expected.id;
              expect(req.request.body).to.deep.eq(expected);
            });
      
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/material/${material.id}`
            );
          });

          it("Material neu - abbruch funktioniert", () => {
            cy.visit(`/material/new?view=material`);
            cy.wait(500);
      
            cy.findByRole("button", { name: "Abbrechen" }).click();
            cy.wait(100);
      
            cy.location("pathname").should("eq", "/szenario/1/stammdaten");
          });

    }
)