import { Maschine } from "../../../src/models/Maschine";
import * as maschinen from "../../fixtures/maschinen.json";

describe(
  "Stammdaten übersicht Tests",
  {
    includeShadowDom: true,
    baseUrl: `${Cypress.env("CYPRESS_HOST")}/szenario/1/stammdaten`,
  },
  () => {
    beforeEach(() => {
      cy.registerStammdatenFixtures();
      cy.intercept("GET", "/api/**/maschinen/", { body: maschinen });
      cy.visit("/");
      cy.wait(500);
    });

    it("Tabs funktionieren", () => {
      cy.get("ui5-title").contains("Stammdaten").should("be.visible");
      cy.get("ui5-title").contains("Maschinen").should("be.visible");

      cy.get("ui5-title").contains("Produkte").should("not.be.visible");

      cy.clickTab("Produkte");

      cy.get("ui5-title").contains("Produkte").should("be.visible");

      cy.get("ui5-title").contains("Materialien").should("not.be.visible");

      cy.clickTab("Materialien");
    });

    it("Maschinen werden angezeigt", () => {
      const checkMaschine = (maschine: Maschine) => {
        cy.get("ui5-table-cell").contains(maschine.id).should("be.visible");
        cy.get("ui5-table-cell").contains(maschine.name).should("be.visible");
        cy.get("ui5-table-cell")
          .contains(maschine.ruestzeit)
          .should("be.visible");
        cy.get("ui5-table-cell")
          .contains(maschine.kostenMinute)
          .should("be.visible");
        cy.get("ui5-table-cell")
          .contains(maschine.mitarbeiterMin)
          .should("be.visible");
        cy.get("ui5-table-cell")
          .contains(maschine.mitarbeiterMax)
          .should("be.visible");
        cy.get("ui5-table-cell")
          .contains(maschine.ausfallWahrscheinlichkeit * 100)
          .should("be.visible");
      };

      checkMaschine(maschinen[0]);
      checkMaschine(maschinen[1]);
      checkMaschine(maschinen[2]);
    });
  }
);
