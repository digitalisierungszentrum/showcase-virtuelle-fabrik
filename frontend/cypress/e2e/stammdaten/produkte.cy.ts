import * as produkte from "../../fixtures/produkte.json";

const produkt = produkte[0];

describe(
    "Produkt Masken Tests",
    {
      baseUrl: `${Cypress.env("CYPRESS_HOST")}/szenario/1/stammdaten`,
      includeShadowDom: true,
    },
    () => {
        beforeEach(() => {
            cy.on("uncaught:exception", (e) => {
              return false;
            });
      
            cy.registerStammdatenFixtures();
            cy.intercept("GET", "/api/**/produkte/*", { body: produkt });
            cy.intercept("PUT", "/api/**/produkte/*", {});
            cy.intercept("POST", "/api/**/produkte/*", {});
            cy.intercept("DELETE", "/api/**/produkte/*", {});
          });

          it("Navigation funktioniert", () => {
            cy.visit(`/produkt/${produkt.id}?view=produkte`);
            cy.wait(2000);
      
            cy.get("ui5-button").contains("Bearbeiten").click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/produkt/${produkt.id}/edit`
            );
      
            cy.get("ui5-button").contains("Speichern").click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/produkt/${produkt.id}`
            );
      
            cy.wait(100);
            cy.findByRole("button", { name: "Schließen" }).click();
            cy.location("pathname").should("eq", "/szenario/1/stammdaten");
      
            cy.wait(100);
            cy.findByRole("button", { name: "neues Produkt" }).click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/produkt/new`
            );
          });

        it("Produkt Details - Alle Informationen des Produkts werden angezeigt", () => {
            cy.visit(`/produkt/${produkt.id}`);
            cy.wait(500);

            cy.get("ui5-title")
            .contains(produkt.name)
            .should("be.visible");

            cy.get("ui5-label")
            .contains("ID")
            .next()
            .contains(produkt.id)
            .should("be.visible");

            cy.get("ui5-label")
            .contains("Verkaufspreis")
            .next()
            .contains(produkt.verkaufspreis)
            .should("be.visible");


            cy.get("ui5-table-cell")
            .contains((produkt.produktionsschritte[0].schritt+1))
            .should("be.visible");
            cy.get("ui5-table-cell").contains("Sägen")
            .should("be.visible");
           
            cy.get("ui5-table-cell")
            .contains(("Buchenholz"))
            .should("be.visible");
            cy.get("ui5-table-cell").contains(produkt.materialbedarf[0].menge)
            .should("be.visible");
               
        })

        
        it("Produkt neu - speichert korrekte daten", () => {
            cy.visit(`/produkt/new`);
            cy.wait(500);
      
            const produkt = produkte[0];
      
            cy.intercept("POST", "/api/**/produkte", {
              body: { id: produkt.id },
            }).as("post");
      
            
            cy.fillInput("Produkt", produkt.name);
            cy.fillInput("Verkaufspreis", produkt.verkaufspreis);
            
            cy.get("div[role='combobox']").eq(0).click({ force: true });
            cy.get("ui5-li").contains("Sägen").click();

            // Materialbedarf
            cy.wait(500);
            cy.get("div[role='combobox']").eq(2).click({ force: true });
            cy.get("ui5-li").contains("Buchenholz").click();
    
            cy.get("input[aria-label='Materialbedarf']").type(
                `{selectAll}${produkt.materialbedarf[0].menge}`
              );
            cy.wait(500);
            
               
            cy.findByRole("button", { name: "Speichern" }).click();
      
            cy.wait("@post").then((req) => {
              const expected = { ...produkt };
              expected.id = "";
              expect(req.request.body).to.deep.eq(expected);
            });
      
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/produkt/${produkt.id}`
            );
          });


          it("Produkt neu - abbruch funktioniert", () => {
            cy.visit(`/produkt/new?view=produkte`);
            cy.wait(500);
      
            cy.findByRole("button", { name: "Abbrechen" }).click();
            cy.wait(100);
      
            cy.location("pathname").should("eq", "/szenario/1/stammdaten");
          });

    }
)