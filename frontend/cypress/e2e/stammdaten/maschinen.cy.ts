import { Maschine } from "../../../src/models/Maschine";
import { secondsToTimeString } from "../../../src/util/misc";
import * as maschinen from "../../fixtures/maschinen.json";

const maschine = maschinen[0];

describe(
  "Maschinen Masken Tests",
  {
    baseUrl: `${Cypress.env("CYPRESS_HOST")}/szenario/1/stammdaten`,
    includeShadowDom: true,
  },
  () => {
    beforeEach(() => {
      cy.on("uncaught:exception", (e) => {
        return false;
      });

      cy.registerStammdatenFixtures();
      cy.intercept("GET", "/api/**/maschinen/*", { body: maschine });
      cy.intercept("PUT", "/api/**/maschinen/*", {});
      cy.intercept("POST", "/api/**/maschinen/*", {});
      cy.intercept("DELETE", "/api/**/maschinen/*", {});
    });

    it("Navigation funktioniert", () => {
      cy.visit(`/maschine/${maschine.id}`);
      cy.wait(2000);

      cy.get("ui5-button").contains("Bearbeiten").click();
      cy.location("pathname").should(
        "eq",
        `/szenario/1/stammdaten/maschine/${maschine.id}/edit`
      );

      cy.get("ui5-button").contains("Speichern").click();
      cy.location("pathname").should(
        "eq",
        `/szenario/1/stammdaten/maschine/${maschine.id}`
      );

      cy.wait(100);
      cy.findByRole("button", { name: "Schließen" }).click();
      cy.location("pathname").should("eq", "/szenario/1/stammdaten");

      cy.wait(100);
      cy.findByRole("button", { name: "neue Maschine" }).click();
      cy.location("pathname").should(
        "eq",
        `/szenario/1/stammdaten/maschine/new`
      );
    });

    it("Maschine Details - Alle Informationen der Maschine werden angezeigt", () => {
      cy.visit(`/maschine/${maschine.id}`);
      cy.wait(500);

      cy.get("ui5-label")
        .contains("Mitarbeiter Mindestanzahl")
        .next()
        .contains(maschine.mitarbeiterMin)
        .should("be.visible");

      cy.get("ui5-label")
        .contains("Mitarbeiter Kapazität")
        .next()
        .contains(maschine.mitarbeiterMax)
        .should("be.visible");

      cy.get("ui5-label")
        .contains("Rüstzeit")
        .next()
        .contains(secondsToTimeString(maschine.ruestzeit))
        .should("be.visible");

      cy.get("ui5-label")
        .contains("Kosten/Minute")
        .next()
        .contains(maschine.kostenMinute)
        .should("be.visible");

      cy.get("ui5-label")
        .contains("Ausfall")
        .next()
        .contains(maschine.ausfallWahrscheinlichkeit * 100)
        .should("be.visible");

      cy.get("ui5-table-cell").contains("Bohren").should("be.visible");
      cy.get("ui5-table-cell")
        .contains(
          secondsToTimeString(maschine.maschinenbefaehigungen[0].taktrate)
        )
        .should("be.visible");
    });

    it("Maschine neu - speichert korrekte daten", () => {
      cy.visit(`/maschine/new`);
      cy.wait(500);

      const maschine = maschinen[0];

      cy.intercept("POST", "/api/**/maschinen", {
        body: { id: maschine.id },
      }).as("post");

      cy.fillInput("Maschine", maschine.name);
      cy.fillInput("Mitarbeiter Mindestanzahl", maschine.mitarbeiterMin);
      cy.fillInput("Mitarbeiter Kapazität", maschine.mitarbeiterMax);
      cy.fillInput("Rüstzeit", maschine.ruestzeit);
      cy.fillInput("Kosten/Minute", maschine.kostenMinute);
      cy.fillInput("Ausfall", maschine.ausfallWahrscheinlichkeit * 100);

      // befähigung auswählen
      cy.get("div[role='combobox']").click({ force: true });
      cy.get("ui5-li").contains("Bohren").click();

      // step input
      cy.get("input[aria-label='Taktrate']").type(
        `{selectAll}${maschine.maschinenbefaehigungen[0].taktrate}`
      );

      cy.findByRole("button", { name: "Speichern" }).click();

      cy.wait("@post").then((req) => {
        const expected = { ...maschine };
        delete expected.id;
        expect(req.request.body).to.deep.eq(expected);
      });

      cy.location("pathname").should(
        "eq",
        `/szenario/1/stammdaten/maschine/${maschine.id}`
      );
    });

    it("Maschine neu - abbruch funktioniert", () => {
      cy.visit(`/maschine/new`);
      cy.wait(500);

      cy.findByRole("button", { name: "Abbrechen" }).click();
      cy.wait(100);

      cy.location("pathname").should("eq", "/szenario/1/stammdaten");
    });
  }
);
