import * as arbeitsschritte from "../../fixtures/arbeitsschritte.json";

const arbeitsschritt = arbeitsschritte[0];

describe(
    "Arbeitsschritte Masken Tests",
    {
      baseUrl: `${Cypress.env("CYPRESS_HOST")}/szenario/1/stammdaten`,
      includeShadowDom: true,
    },
    () => {
        beforeEach(() => {
            cy.on("uncaught:exception", (e) => {
              return false;
            });
      
            cy.registerStammdatenFixtures();
            cy.intercept("GET", "/api/**/arbeitsschritte/*", { body: arbeitsschritt });
            cy.intercept("PUT", "/api/**/arbeitsschritte/*", {});
            cy.intercept("POST", "/api/**/arbeitsschritte/*", {});
            cy.intercept("DELETE", "/api/**/arbeitsschritte/*", {});
          });

          it("Navigation funktioniert", () => {
            cy.visit(`/arbeitsschritt/${arbeitsschritt.id}?view=arbeitsschritt`);
            cy.wait(2000);
      
            cy.get("ui5-button").contains("Bearbeiten").click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/arbeitsschritt/${arbeitsschritt.id}/edit`
            );
      
            cy.get("ui5-button").contains("Speichern").click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/arbeitsschritt/${arbeitsschritt.id}`
            );
      
            cy.wait(100);
            cy.findByRole("button", { name: "Schließen" }).click();
            cy.location("pathname").should("eq", "/szenario/1/stammdaten");
      
            cy.visit(`?view=arbeitsschritte`);

            cy.wait(100);
            cy.findByRole("button", { name: "neuer Arbeitsschritt" }).click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/arbeitsschritt/new`
            );
          });

        it("Arbeitsschritt Details - Alle Informationen des Arbeitsschritts werden angezeigt", () => {
            cy.visit(`/arbeitsschritt/${arbeitsschritt.id}`);
            cy.wait(500);

            cy.get("ui5-title")
            .contains(arbeitsschritt.name)
            .should("be.visible");

            cy.get("ui5-label")
            .contains("ID")
            .next()
            .contains(arbeitsschritt.id)
            .should("be.visible");

           
            
        })

        it("Arbeitsschritt neu - speichert korrekte daten", () => {
            cy.visit(`/arbeitsschritt/new`);
            cy.wait(500);
      
            const arbeitsschritt = arbeitsschritte[0];
      
            cy.intercept("POST", "/api/**/arbeitsschritte", {
              body: { id: arbeitsschritt.id },
            }).as("post");
      
            
            cy.fillInput("Arbeitsschritt", arbeitsschritt.name);
      
            
            
               
            cy.findByRole("button", { name: "Speichern" }).click();
      
            cy.wait("@post").then((req) => {
              const expected = { ...arbeitsschritt };
              delete expected.id;
              expect(req.request.body).to.deep.eq(expected);
            });
      
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/arbeitsschritt/${arbeitsschritt.id}`
            );
          });

          it("Arbeitsschritt neu - abbruch funktioniert", () => {
            cy.visit(`/arbeitsschritt/new?view=arbeitsschritt`);
            cy.wait(500);
      
            cy.findByRole("button", { name: "Abbrechen" }).click();
            cy.wait(100);
      
            cy.location("pathname").should("eq", "/szenario/1/stammdaten");
          });

    }
)