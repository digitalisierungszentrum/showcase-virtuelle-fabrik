import * as chargen from "../../fixtures/chargen.json";

const charge = chargen[0];

describe(
    "Chargen Masken Tests",
    {
      baseUrl: `${Cypress.env("CYPRESS_HOST")}/szenario/1/stammdaten`,
      includeShadowDom: true,
    },
    () => {
        beforeEach(() => {
            cy.on("uncaught:exception", (e) => {
              return false;
            });
      
            cy.registerStammdatenFixtures();
            cy.intercept("GET", "/api/**/chargen/*", { body: charge });
            cy.intercept("PUT", "/api/**/chargen/*", {});
            cy.intercept("POST", "/api/**/chargen/*", {});
            cy.intercept("DELETE", "/api/**/chargen/*", {});
          });

          it("Navigation funktioniert", () => {
            cy.visit(`/chargen/${charge.id}?view=chargen`);
            cy.wait(2000);
      
            cy.get("ui5-button").contains("Bearbeiten").click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/chargen/${charge.id}/edit`
            );
      
            cy.get("ui5-button").contains("Speichern").click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/chargen/${charge.id}`
            );
      
            cy.wait(100);
            cy.findByRole("button", { name: "Schließen" }).click();
            cy.location("pathname").should("eq", "/szenario/1/stammdaten");
      
            cy.wait(100);
            cy.findByRole("button", { name: "neue Charge" }).click();
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/chargen/new`
            );
          });

        it("Chargen Details - Alle Informationen der Charge werden angezeigt", () => {
            cy.visit(`/chargen/${charge.id}?view=chargen`);
            cy.wait(500);

            cy.get("ui5-title")
            .contains(charge.name)
            .should("be.visible");

            cy.get("ui5-label")
            .contains("ID")
            .next()
            .contains(charge.id)
            .should("be.visible");

            cy.get("ui5-label")
            .contains("Priorität")
            .next()
            .contains(charge.prioritaet)
            .should("be.visible");
    
            cy.get("ui5-label")
            .contains("Produkt")
            .next()
            .contains("Tisch")
            .should("be.visible");
    
            cy.get("ui5-label")
            .contains("Stückzahl")
            .next()
            .contains(charge.produktbedarf[0].stueckzahl)
            .should("be.visible");
    
            
        })

        it("Charge neu - speichert korrekte daten", () => {
            cy.visit(`/chargen/new?view=chargen`);
            cy.wait(500);
      
            const charge = chargen[0];
      
            cy.intercept("POST", "/api/**/chargen", {
              body: { id: charge.id },
            }).as("post");
      
            
            cy.fillInput("Charge", charge.name);
            cy.fillInput("Priorität", charge.prioritaet);
            

            cy.get("input[aria-label='Stückzahl']").type(
                `{selectAll}${charge.produktbedarf[0].stueckzahl}`
              );
            

            cy.get("div[role='combobox']").click({ force: true });
            cy.get("ui5-li").contains("Tisch").click();
      
            cy.wait(500);
           
            
               
            cy.findByRole("button", { name: "Speichern" }).click();
      
            cy.wait("@post").then((req) => {
              const expected = { ...charge };
              expected.id ="";              
              expect(req.request.body).to.deep.eq(expected);
            });
      
            cy.location("pathname").should(
              "eq",
              `/szenario/1/stammdaten/chargen/${charge.id}`
            );
          });

          it("Charge neu - abbruch funktioniert", () => {
            cy.visit(`/chargen/new?view=chargen`);
            cy.wait(500);
      
            cy.findByRole("button", { name: "Abbrechen" }).click();
            cy.wait(100);
      
            cy.location("pathname").should("eq", "/szenario/1/stammdaten");
          });

    }
)