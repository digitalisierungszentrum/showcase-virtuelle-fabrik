import "@testing-library/cypress/add-commands";

import * as maschinen from "../fixtures/maschinen.json";

// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

Cypress.Commands.add("clickTab", (name: string) =>
  cy.get("ui5-tabcontainer").shadow().findAllByRole("tab", { name }).click()
);

Cypress.Commands.add("fillInput", (name: string, text) =>
  cy
    .findByRole("textbox", { name })
    .find("input")
    .last()
    .type(`{selectAll}${text}`)
);

Cypress.Commands.add("registerStammdatenFixtures", () => {
  cy.intercept("GET", "/api/**/maschinen", { fixture: "maschinen.json" });
  cy.intercept("GET", "/api/**/mitarbeiter", {
    fixture: "mitarbeiter.json",
  });
  cy.intercept("GET", "/api/**/arbeitsschritte", {
    fixture: "arbeitsschritte.json",
  });
  cy.intercept("GET", "/api/**/materialien", {
    fixture: "materialien.json",
  });
  cy.intercept("GET", "/api/**/produkte", { fixture: "produkte.json" });
  cy.intercept("GET", "/api/**/chargen", { fixture: "chargen.json" });
});

declare global {
  namespace Cypress {
    interface Chainable {
      clickTab(name: string): Chainable<JQuery<HTMLElement>>;

      fillInput(
        name: string,
        text: string | number
      ): Chainable<JQuery<HTMLElement>>;

      registerStammdatenFixtures(): void;
    }
  }
}
