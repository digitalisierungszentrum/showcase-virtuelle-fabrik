/// <reference types="vitest" />
import react from "@vitejs/plugin-react";
import { defineConfig, loadEnv } from "vite";
import svgrPlugin from "vite-plugin-svgr";
import viteTsconfigPaths from "vite-tsconfig-paths";

// https://vitejs.dev/config/
export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  const secure = process.env.VITE_API_SECURE;
  const host = process.env.VITE_API_HOST;

  return defineConfig({
    plugins: [react(), viteTsconfigPaths(), svgrPlugin()],
    server: {
      port: 3000,
      proxy: {
        "/api": {
          target: `${secure ? "https" : "http"}://${host}`,
        },
        "/api/ws/socket.io": {
          target: `${secure ? "wss" : "ws"}://${host}`,
          ws: true,
        },
      },
    },
    build: {
      outDir: "build",
    },
    test: {
      globals: true,
      environment: "jsdom",
      setupFiles: "./src/setupTests.ts",
      coverage: {
        reporter: ["text", "html"],
        exclude: ["node_modules/", "src/setupTests.ts"],
      },
    },
  });
};
