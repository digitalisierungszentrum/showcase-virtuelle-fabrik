import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { ObjectPageSection } from "@ui5/webcomponents-react";

import { appRender } from "../../util/testUtil";
import StammdatenDetailsComponent from "./StammdatenDetailsComponent";

vi.mock("../stammdaten-delete-button/StammdatenDeleteButton");

describe("StammdatenDetailsComponent tests", () => {
  test("should render", async () => {
    appRender(
      <StammdatenDetailsComponent
        header={<span data-testid="header-div" />}
        breadcrumbs={<div data-testid="breadcrumb-div" />}
      >
        <ObjectPageSection id="test" data-testid="children-section">
          <></>
        </ObjectPageSection>
      </StammdatenDetailsComponent>
    );

    expect(screen.getByTestId("header-div")).toBeVisible();
    expect(screen.getByTestId("breadcrumb-div")).toBeVisible();
    expect(screen.getByTestId("children-section")).toBeVisible();
    expect(screen.queryByText("stammdaten.form.deleteButton")).toBeNull();
    expect(screen.queryByText("stammdaten.form.editButton")).toBeNull();
  });

  test("should show actions", async () => {
    const onEditFn = vi.fn();
    const onDeleteFn = vi.fn();

    appRender(
      <StammdatenDetailsComponent
        header={<span data-testid="header-div" />}
        breadcrumbs={<div data-testid="breadcrumb-div" />}
        withActions
        onEdit={onEditFn}
        onDelete={onDeleteFn}
        deleteConfirmationBody=""
        deleteConfirmationTitle=""
      >
        <ObjectPageSection id="test" data-testid="children-section">
          <></>
        </ObjectPageSection>
      </StammdatenDetailsComponent>
    );

    expect(screen.getByText("stammdaten.form.editButton")).toBeVisible();
    expect(screen.getByTestId("deleteButton")).toBeVisible();

    const user = userEvent.setup();

    await user.click(screen.getByText("stammdaten.form.editButton"));
    expect(onEditFn).toBeCalledTimes(1);

    // ui5 fügt den button ein zweites Mal in einem popover hinzu
    // deswegen hier, getAll
    await user.click(screen.getAllByTestId("deleteButton")[1]);
    expect(onEditFn).toBeCalledTimes(1);
  });
});
