import {
  Breadcrumbs,
  BreadcrumbsItem,
  Button,
  ButtonDesign,
  DynamicPageTitle,
  ObjectPage,
  ObjectPageMode,
  ObjectPageSectionPropTypes,
} from "@ui5/webcomponents-react";
import { FC, ReactElement } from "react";
import { useTranslation } from "react-i18next";

import { useNavigateWithParams } from "../../hooks/useNavigateWithParams";
import StammdatenDeleteButton from "../stammdaten-delete-button/StammdatenDeleteButton";

interface StammdatenDetailsComponentProps {
  /**
   * Überschrift der Ansicht.
   */
  header: ReactElement;
  /**
   * Weitere Informationen die in den Kopfbereich sollen.
   */
  headerContent?: ReactElement;
  /**
   * Breadcrubs. das `Stammdaten`-Breadcrumb wird vorbefüllt.
   */
  breadcrumbs: ReactElement;
  /**
   * Optionale Fußzeile.
   */
  footer?: ReactElement;
  /**
   * Optional. Falls true, wird werden in der Kopfzeile Edit und Delete
   * Button angezeit.
   */
  withActions?: boolean;
  /**
   * Funktion für den Edit Button.
   */
  onEdit?: () => void;
  /**
   * Funktion für den Delete Button.
   *
   * Das zurückgegebene Promise sollte erst resolven wenn der Confirmation
   * Dialog geschlossen werden soll.
   */
  onDelete?: () => Promise<void>;
  /**
   * Titel für den DeleteDialog
   */
  deleteConfirmationTitle?: string;
  /**
   * Titel body für den DeleteDialog
   */
  deleteConfirmationBody?: string;
  children:
    | ReactElement<ObjectPageSectionPropTypes>
    | ReactElement<ObjectPageSectionPropTypes>[];
}

/**
 *  Basiskomponente für eine Stammdaten-Detailansicht.
 */
const StammdatenDetailsComponent: FC<StammdatenDetailsComponentProps> = ({
  header,
  headerContent,
  breadcrumbs,
  footer,
  withActions = false,
  onEdit,
  onDelete,
  deleteConfirmationTitle,
  deleteConfirmationBody,
  children,
}) => {
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();

  return (
    <ObjectPage
      headerTitle={
        <DynamicPageTitle
          key={"1"}
          header={header}
          navigationActions={
            <Button
              key="closeButton"
              role="button"
              aria-label={t("stammdaten.closeButton")}
              icon="decline"
              design={ButtonDesign.Transparent}
              onClick={() => navigate("..")}
            />
          }
          breadcrumbs={
            <Breadcrumbs>
              <BreadcrumbsItem>Stammdaten</BreadcrumbsItem>
              {breadcrumbs}
            </Breadcrumbs>
          }
          actions={
            withActions ? (
              <>
                <Button
                  design={ButtonDesign.Emphasized}
                  icon="edit"
                  onClick={onEdit}
                >
                  {t("stammdaten.form.editButton")}
                </Button>
                <StammdatenDeleteButton
                  title={deleteConfirmationTitle ?? ""}
                  body={deleteConfirmationBody ?? ""}
                  onConfirm={onDelete!}
                />
              </>
            ) : (
              <></>
            )
          }
        >
          {headerContent}
        </DynamicPageTitle>
      }
      mode={ObjectPageMode.Default}
      footer={footer}
    >
      {children}
    </ObjectPage>
  );
};

export default StammdatenDetailsComponent;
