import { Card, CardPropTypes } from "@ui5/webcomponents-react";
import { CSSProperties, ReactNode, forwardRef } from "react";

interface HighlightCardProps extends CardPropTypes {
  /**
   * css-Style Properties der Card selber.
   */
  style?: CSSProperties;
  /**
   * wenn true, wird das highlight div gerendert.
   */
  active?: boolean;
  /**
   * css-Style des highlight div.
   */
  activeStyle?: CSSProperties;
  /**
   * children der Card.
   */
  children: ReactNode;
}

/**
 * Card die Einen Rahmen um die Card anzeigt, wenn active true ist.
 *
 * Die ui5-Webcomponent macht es unmöglich die Card direct zu stylen, deswegen
 * wird ein div als overlay drüber gelegt, das gestyled werden kann.
 */
const HighlightCard = forwardRef(
  (props: HighlightCardProps, ref): JSX.Element => {
    const { style, active, activeStyle, children } = props;

    return (
      <div
        style={{
          position: "relative",
          width: style?.width,
          height: style?.height,
          minHeight: style?.minHeight,
          margin: style?.margin,
        }}
      >
        <Card
          ref={ref as any}
          {...props}
          style={{
            ...style,
            margin: undefined,
          }}
        >
          {children}
        </Card>
        {active && (
          <div
            style={{
              pointerEvents: "none",
              position: "absolute",
              top: 0,
              width: "100%",
              height: "100%",
              boxSizing: "border-box",
              ...activeStyle,
            }}
          />
        )}
      </div>
    );
  }
);
HighlightCard.displayName = "HighlightCard";

export default HighlightCard;
