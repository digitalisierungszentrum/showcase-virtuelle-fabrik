import {
  Bar,
  BarDesign,
  Button,
  ButtonDesign,
  Dialog,
  Loader,
} from "@ui5/webcomponents-react";
import { FC, useState } from "react";
import { createPortal } from "react-dom";
import { useTranslation } from "react-i18next";

interface DeleteDialogProps {
  /**
   * Titel des Dialogs.
   */
  title: string;
  /**
   * Body des Dialogs.
   */
  body: string;
  /**
   * Wird aufgerufen, wenn der Nutzer auf Bestätigen klickt.
   *
   * Der Dialog bleibt so lange offen bis das zurückgegebene Promise resolved.
   */
  onConfirm: () => Promise<void>;
}

/**
 * Delete Button für die Stammdaten-Detailansichten.
 *
 * Bei Klick auf den Button wird ein Confirmation Dialog geöffnet.
 */
const StammdatenDeleteButton: FC<DeleteDialogProps> = ({
  title,
  body,
  onConfirm,
}) => {
  const { t } = useTranslation();

  const [dialogIsOpen, setDialogIsOpen] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  async function onDelete() {
    setIsSubmitting(true);

    await onConfirm();

    setDialogIsOpen(false);
    setIsSubmitting(false);
  }

  const dialog = (
    <Dialog
      open={dialogIsOpen}
      header={<Bar>{title}</Bar>}
      footer={
        <>
          {isSubmitting && (
            <Loader
              style={{
                position: "absolute",
                left: 0,
                bottom: "var(--_ui5_dialog_footer_height)",
              }}
            />
          )}
          <Bar
            design={BarDesign.Footer}
            endContent={
              <>
                <Button
                  design={ButtonDesign.Negative}
                  disabled={isSubmitting}
                  onClick={onDelete}
                >
                  {t("stammdaten.form.deleteButton")}
                </Button>
                <Button
                  design={ButtonDesign.Transparent}
                  onClick={() => setDialogIsOpen(false)}
                >
                  {t("stammdaten.form.cancelButton")}
                </Button>
              </>
            }
          />
        </>
      }
    >
      {body}
    </Dialog>
  );

  return (
    <>
      <Button
        design={ButtonDesign.Transparent}
        icon="delete"
        onClick={() => setDialogIsOpen(true)}
      >
        {t("stammdaten.form.deleteButton")}
      </Button>
      {createPortal(dialog, document.body)}
    </>
  );
};

export default StammdatenDeleteButton;
