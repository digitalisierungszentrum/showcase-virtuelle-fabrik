import { screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import { appRender } from "../../util/testUtil";
import StammdatenDeleteButton from "./StammdatenDeleteButton";

describe("StamdatenDeleteButton tests", () => {
  test("Should render", async () => {
    appRender(
      <StammdatenDeleteButton
        title="testTitle"
        body="testBody"
        onConfirm={() => Promise.resolve()}
      />
    );

    const user = userEvent.setup();

    await user.click(screen.getAllByText("stammdaten.form.deleteButton")[1]);

    expect(screen.getByText("testTitle")).toBeVisible();
    expect(screen.getByText("testBody")).toBeVisible();

    expect(screen.getByText("stammdaten.form.cancelButton")).toBeVisible();
    expect(screen.getAllByText("stammdaten.form.deleteButton")).toHaveLength(2);
  });

  test("Delete Confirmation should call function", async () => {
    const deleteFn = vi.fn();

    appRender(
      <StammdatenDeleteButton
        title={"testTitle"}
        body={""}
        onConfirm={deleteFn}
      />
    );

    const user = userEvent.setup();

    await user.click(screen.getAllByText("stammdaten.form.deleteButton")[1]);

    await user.click(
      (
        await screen.findAllByText("stammdaten.form.deleteButton")
      )[0]
    );

    await waitFor(() => expect(deleteFn).toHaveBeenCalledTimes(1));
  });
});
