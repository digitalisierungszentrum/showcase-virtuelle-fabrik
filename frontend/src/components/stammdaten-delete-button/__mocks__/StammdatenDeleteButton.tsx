const StammdatenDeleteButtonMock = ({ onConfirm }: any) => {
  return (
    <button data-testid="deleteButton" onClick={() => onConfirm()}>
      test
    </button>
  );
};

export default StammdatenDeleteButtonMock;
