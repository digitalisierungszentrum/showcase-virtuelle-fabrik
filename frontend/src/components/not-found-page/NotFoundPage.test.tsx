import { appRender } from "../../util/testUtil";
import NotFoundPage from "./NotFoundPage";

describe("NotFoundPage tests", () => {
  test("should render", async () => {
    appRender(<NotFoundPage />);

    expect(true).toBeTruthy();
  });
});
