import {
  Button,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  IllustratedMessage,
} from "@ui5/webcomponents-react";
import { FC } from "react";
import { useTranslation } from "react-i18next";

import { useNavigateWithParams } from "../../hooks/useNavigateWithParams";

interface NotFoundPageProps {}

/**
 * Placeholder für eine Stammdaten-Detailansicht, falls das anzuzeigende Element
 * nicht gefunden werden kann.
 */
const NotFoundPage: FC<NotFoundPageProps> = () => {
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();

  return (
    <FlexBox
      direction={FlexBoxDirection.Column}
      justifyContent={FlexBoxJustifyContent.Center}
      alignItems={FlexBoxAlignItems.Center}
      style={{ width: "100%" }}
    >
      {
        <div className="position-top">
          <IllustratedMessage name="PageNotFound" />

          <h1 style={{ color: "grey", fontSize: 100 }}>
            {t("stammdaten.notfound.textFailure")}
          </h1>
          <h3>{t("stammdaten.notfound.textView")}</h3>
          <Button onClick={() => navigate("..")}>
            {t("stammdaten.notfound.textBtn")}
          </Button>
        </div>
      }
    </FlexBox>
  );
};

export default NotFoundPage;
