import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { UserEvent } from "@testing-library/user-event/setup/setup";

import { appRender } from "../../util/testUtil";
import FormBar from "./FormBar";

describe("FormBar Tests", () => {
  let user: UserEvent;

  beforeEach(() => {
    user = userEvent.setup();
  });

  test("should render", async () => {
    appRender(
      <FormBar onSubmit={() => {}} onCancel={() => {}} errorPrefix="" />
    );

    expect(screen.queryByText("stammdaten.form.cancelButton")).toBeVisible();
    expect(screen.queryByText("stammdaten.form.cancelButton")).toBeVisible();
    expect(screen.queryByTestId("error-btn")).toBeNull();
  });

  test("functions should be called", async () => {
    const onSubmitFn = vi.fn();
    const onCancelFn = vi.fn();

    appRender(
      <FormBar onSubmit={onSubmitFn} onCancel={onCancelFn} errorPrefix="" />
    );

    await user.click(screen.getByText("stammdaten.form.submitButton"));
    expect(onSubmitFn).toBeCalledTimes(1);

    await user.click(screen.getByText("stammdaten.form.cancelButton"));
    expect(onCancelFn).toBeCalledTimes(1);
  });

  test("Submit button should be disabled", async () => {
    const onSubmitFn = vi.fn();

    appRender(
      <FormBar
        onSubmit={onSubmitFn}
        submitEnabled={false}
        errorPrefix=""
        onCancel={() => {}}
      />
    );

    const submitBtn = screen.getByText("stammdaten.form.submitButton");

    expect(submitBtn).toBeDisabled();
  });

  test("should show error popover", async () => {
    appRender(
      <FormBar
        onSubmit={() => {}}
        onCancel={() => {}}
        errorPrefix=""
        errors={{
          a: "error 1",
          b: "error 2",
          c: "error 3",
        }}
      />
    );

    expect(
      screen.getByRole("button", {
        name: "stammdaten.form.errors",
      })
    ).toHaveTextContent("3");

    await user.click(
      screen.getByRole("button", {
        name: "stammdaten.form.errors",
      })
    );

    expect(await screen.findByText("error 1")).toBeVisible();
    expect(await screen.findByText("error 2")).toBeVisible();
    expect(await screen.findByText("error 3")).toBeVisible();
  });
});
