import {
  Bar,
  BarDesign,
  Button,
  ButtonDesign,
  FlexBox,
  FlexBoxAlignItems,
  List,
  MessageItem,
  MessageView,
  PopoverPlacementType,
  ResponsivePopover,
  Title,
  TitleLevel,
} from "@ui5/webcomponents-react";
import { FormikErrors } from "formik";
import { FC, useRef, useState } from "react";
import { createPortal } from "react-dom";
import { useTranslation } from "react-i18next";

import { useParseErrors } from "../../util/errorUtil";

interface FormBarProps {
  /**
   * Content des Error Popovers.
   */
  errors?: FormikErrors<any>;

  errorPrefix: string;
  /**
   * Bestimmt ob der Submitbutton aktiviert ist.
   */
  submitEnabled?: boolean;
  /**
   * Funktion des Submit Button.
   */
  onSubmit: () => void;
  /**
   * Funktion des Cancel Button.
   */
  onCancel: () => void;
}

/**
 * Footer für Stammdaten-Form.
 */
const FormBar: FC<FormBarProps> = ({
  submitEnabled = true,
  errors,
  errorPrefix,
  onSubmit,
  onCancel,
}) => {
  const { t } = useTranslation();
  const parsedErrors = useParseErrors<any>(errors, errorPrefix);
  const hasErrors = parsedErrors?.length !== 0;

  const [popoverOpen, setPopoverOpen] = useState(false);
  const popoverId = "errorPopoverButton";

  const messageViewRef = useRef<any>();
  const [isOnDetailsPage, setIsOnDetailsPage] = useState(false);

  const messageDialog = (
    <ResponsivePopover
      open={popoverOpen}
      opener={popoverId}
      onAfterClose={() => setPopoverOpen(false)}
      placementType={PopoverPlacementType.Top}
      hideArrow
      header={
        <Bar
          startContent={
            <FlexBox alignItems={FlexBoxAlignItems.Center}>
              {isOnDetailsPage && (
                <Button
                  design={ButtonDesign.Transparent}
                  icon="slim-arrow-left"
                  onClick={() => {
                    setIsOnDetailsPage(false);
                    messageViewRef.current.navigateBack();
                  }}
                />
              )}
              <Title level={TitleLevel.H4}>Messages</Title>
            </FlexBox>
          }
          endContent={
            <Button
              role="button"
              design={ButtonDesign.Transparent}
              icon="decline"
              onClick={() => setPopoverOpen(false)}
            />
          }
        />
      }
    >
      <MessageView
        ref={messageViewRef}
        onItemSelect={() => {
          setIsOnDetailsPage(true);
        }}
      >
        <List>
          {parsedErrors &&
            parsedErrors.map(({ count, groupName, subtitle, title, type }) => (
              <MessageItem
                key={`${title}/${subtitle}`}
                counter={count}
                groupName={groupName}
                type={type}
                titleText={title}
                subtitleText={subtitle}
              >
                {title}
              </MessageItem>
            ))}
        </List>
      </MessageView>
    </ResponsivePopover>
  );

  return (
    <>
      <Bar
        design={BarDesign.Footer}
        startContent={
          hasErrors && (
            <>
              <Button
                id={popoverId}
                role="button"
                aria-label={t("stammdaten.form.errors")}
                design={ButtonDesign.Attention}
                icon="error"
                style={{
                  background: "var(--sapNegativeElementColor)",
                  color: "var(--sapContent_ContrastTextColor)",
                }}
                onClick={() => setPopoverOpen(true)}
              >
                {parsedErrors?.length ?? 0}
              </Button>
            </>
          )
        }
        endContent={
          <>
            <Button
              role="button"
              disabled={!submitEnabled}
              design={ButtonDesign.Emphasized}
              onClick={onSubmit}
            >
              {t("stammdaten.form.submitButton")}
            </Button>
            <Button
              role="button"
              design={ButtonDesign.Transparent}
              onClick={onCancel}
            >
              {t("stammdaten.form.cancelButton")}
            </Button>
          </>
        }
      />
      {hasErrors && createPortal(messageDialog, document.body)}
    </>
  );
};

export default FormBar;
