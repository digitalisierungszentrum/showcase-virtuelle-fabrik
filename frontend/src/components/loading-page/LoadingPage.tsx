import {
  BusyIndicator,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
} from "@ui5/webcomponents-react";
import { FC } from "react";

interface LoadingPageProps {}

/**
 * Placeholder für Stammdaten-Detailansicht, während das Element gerade geladen
 * wird.
 */
const LoadingPage: FC<LoadingPageProps> = () => {
  return (
    <FlexBox
      direction={FlexBoxDirection.Column}
      justifyContent={FlexBoxJustifyContent.Center}
      alignItems={FlexBoxAlignItems.Center}
      style={{ width: "100%" }}
    >
      <BusyIndicator active />
    </FlexBox>
  );
};

export default LoadingPage;
