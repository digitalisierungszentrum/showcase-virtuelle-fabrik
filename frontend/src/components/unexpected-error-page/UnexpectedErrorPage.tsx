import {
  Button,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
} from "@ui5/webcomponents-react";
import { FC } from "react";
import { useTranslation } from "react-i18next";

import { useNavigateWithParams } from "../../hooks/useNavigateWithParams";

interface UnexpectedErrorPageProps {}

/**
 * Placeholder für eine Stammdaten-Detailansicht, falls ein unerwarteter Fehler
 * auftritt.
 */
const UnexpectedErrorPage: FC<UnexpectedErrorPageProps> = () => {
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();
  return (
    <FlexBox
      direction={FlexBoxDirection.Column}
      justifyContent={FlexBoxJustifyContent.Center}
      alignItems={FlexBoxAlignItems.Center}
      style={{ width: "100%" }}
    >
      {
        <div className="position-top">
          <h1>{t("stammdaten.unexpected.textFailure")}</h1>
          <h3>{t("stammdaten.unexpected.textView")}</h3>
          <Button onClick={() => navigate("..")}>
            {t("stammdaten.unexpected.textBtn")}
          </Button>
        </div>
      }
    </FlexBox>
  );
};
export default UnexpectedErrorPage;
