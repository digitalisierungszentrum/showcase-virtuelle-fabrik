import { Icon } from "@ui5/webcomponents-react";

import "./FloatingActionButton.css";

interface FloatingActionButtonProps {
  /**
   * Icon das von dem FAB angezeigt werden soll.
   */
  icon: string;
  /**
   * Bei manchen icons, zum Beispiel play, sieht es komisch aus, wenn das Icon zentriert ist.
   */
  iconOffset?: true;
  /**
   * Funktion die ausgeführt werden soll, wenn der FAB gedrückt wird.
   */
  onClick?: () => void;
}

/**
 * Material Design Floating Action Button.
 */
const FloatingActionButton = ({
  icon,
  iconOffset,
  onClick,
}: FloatingActionButtonProps): JSX.Element => {
  return (
    <>
      <div
        className={`floating-action-button ${iconOffset ? "icon-offset" : ""}`}
        onClick={onClick}
      >
        <Icon interactive name={icon} style={{}} />
      </div>
    </>
  );
};

export default FloatingActionButton;
