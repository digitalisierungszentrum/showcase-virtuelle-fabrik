import { ShellBar, StandardListItem } from "@ui5/webcomponents-react";
import { FunctionComponent, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useMatch, useNavigate } from "react-router-dom";

import Logo from "../../assets/logo-light.svg";
import { useNavbarRef } from "../../hooks/useNavbarRef";
import {
  AppRoutes,
  SzenarioRoutes,
  buildRoute,
  getRoute,
} from "../../routes/appRoutes";

/**
 * Navigationsleiste der Anwendung.
 */
const Navbar: FunctionComponent = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const ref = useNavbarRef();

  const match = useMatch({
    path: buildRoute(AppRoutes.SzenarioDetails, ":match"),
    end: false,
  });

  const title = useMemo(() => {
    switch (match?.params?.match) {
      case SzenarioRoutes.Stammdaten:
        return t("stammdaten.title");
      case SzenarioRoutes.Editor:
        return t("editor.title");
      case SzenarioRoutes.Dashboard:
        return t("dashboard.title");
    }
  }, [t, match]);

  return (
    <ShellBar
      ref={ref}
      id="navbar"
      logo={<img height={30} width={30} src={Logo} alt="" />}
      primaryTitle={title}
      onMenuItemClick={(e) => {
        navigate(
          getRoute(
            buildRoute(
              AppRoutes.SzenarioDetails,
              e.detail.item.dataset.route as string
            ),
            {
              szenarioId: "1",
            }
          )
        );
      }}
      menuItems={
        <>
          <StandardListItem data-route={SzenarioRoutes.Stammdaten}>
            {t("stammdaten.title")}
          </StandardListItem>
          <StandardListItem data-route={SzenarioRoutes.Editor}>
            {t("editor.title")}
          </StandardListItem>
          {/*<StandardListItem data-route={SzenarioRoutes.Dashboard}>
            {t("dashboard.title")}
          </StandardListItem>*/}
        </>
      }
    />
  );
};

export default Navbar;
