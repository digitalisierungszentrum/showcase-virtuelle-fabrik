import {
  Option,
  Select,
  Ui5CustomEvent,
  ValueState,
} from "@ui5/webcomponents-react";
import { ThemingParameters } from "@ui5/webcomponents-react-base";
import { SelectDomRef } from "@ui5/webcomponents-react/dist/webComponents/Select";
import { CSSProperties } from "react";

interface AppSelectProps<T extends { id: string; name: string }> {
  /**
   * name-parameter des HTML-Elements
   */
  name: string;
  /**
   * Placeholder, falls keine Option selektiert ist.
   */
  placeholder?: string;
  /**
   * die ausgewählte Option.
   */
  selected: string;
  /**
   * Funktion die aufgerufen wird, wenn sich die ausgewählte Option ändert.
   */
  onChange: (elem?: T) => void;

  data: T[];
  /**
   * Reduziert die Größe des Elements auf Inhalt herunter
   */
  fitContent?: boolean;
  /**
   * vallueState des UI5-Elements.
   */
  valueState?: ValueState;
  /**
   * valueStateMessage des UI5-Elements.
   */
  valueStateMessage?: JSX.Element;
}

const style: { [key: string]: CSSProperties } = {
  select: {
    maxWidth: "0",
  },
  selectPlaceholder: {
    borderColor: ThemingParameters.sapContent_Placeholderloading_Background,
    maxWidth: "11em",
  },
  placeholder: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: ".5em",
    top: 0,
    left: 0,
    height: "2.5em",
    pointerEvents: "none",
    color: ThemingParameters.sapField_PlaceholderTextColor,
    fontStyle: "italic",
    whiteSpace: "nowrap",
  },
};

/**
 * Select das die gegebenen Arbeitsschritte als Auswahlmöglichkeiten anzeigt.
 */
const AppSelect = <T extends { id: string; name: string }>({
  name,
  placeholder,
  selected,
  data,
  fitContent,
  onChange,
  valueState,
  valueStateMessage,
}: AppSelectProps<T>): JSX.Element => {
  const onChangeWrapper = (
    event: Ui5CustomEvent<SelectDomRef, { selectedOption: HTMLElement }>
  ) => {
    const id = event.detail.selectedOption.dataset.id;
    const item = data.find((s) => s.id === id);

    onChange(item);
  };

  return (
    <div
      style={{
        position: "relative",
      }}
    >
      <Select
        className="placeholderSelect"
        name={name}
        onClick={(e) => e.stopPropagation()}
        onChange={onChangeWrapper}
        valueState={valueState}
        valueStateMessage={valueStateMessage}
        style={{
          ...(placeholder && valueState !== "Error"
            ? style.selectPlaceholder
            : style.select),
          ...(fitContent
            ? {
                minWidth: "fit-content",
                whiteSpace: "nowrap",
                border: "none",
              }
            : {}),
        }}
        accessibleName={placeholder}
      >
        {placeholder && <Option />}
        {data?.map((m) => (
          <Option
            key={m.id}
            data-id={m.id}
            value={m.id}
            selected={selected === m.id}
          >
            {m.name}
          </Option>
        ))}
      </Select>
      {placeholder && !selected && (
        <div style={style.placeholder}>{placeholder}</div>
      )}
    </div>
  );
};

export default AppSelect;
