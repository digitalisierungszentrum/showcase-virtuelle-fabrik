import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import { testArbeitsschritte } from "../../util/testData";
import { appRender } from "../../util/testUtil";
import AppSelect from "./AppSelect";

describe("ArbeitsschrittSelect tests", () => {
  test("should render", async () => {
    appRender(
      <AppSelect name={"test"} selected="true" data={[]} onChange={() => {}} />
    );

    expect(true).toBeTruthy();
  });

  test("should trigger onChange", async () => {
    const onChangeFn = vi.fn();

    const data = testArbeitsschritte;

    appRender(
      <AppSelect
        name="test"
        selected=""
        data={data}
        onChange={onChangeFn}
        placeholder="placeholder"
      />
    );

    const user = userEvent.setup({
      pointerEventsCheck: 0,
    });

    await user.click(screen.getByText("placeholder"));

    await screen.findByText(data[0].name);
    await user.click(screen.getByText(data[0].name));

    // ein weiterer test der nicht richtig funktioniert
    // await waitFor(() => expect(onChangeFn).toHaveBeenCalledTimes(1));
    // expect(onChangeFn).toHaveBeenCalledWith(data[0]);
  });
});
