import { AxiosError } from "axios";
import { useQueryClient } from "react-query";
import create from "zustand";

import { useSzenarioApi } from "../../../api";
import { EditorSocket } from "../../../api/szenario/editor/produktionslinienRequests";
import { allChargenQueryOptions } from "../../../hooks/useChargen";
import { allMaschinenQueryOptions } from "../../../hooks/useMaschinen";
import { Charge } from "../../../models/Charge";
import { EditorNotification } from "../../../models/EditorNotification";
import { Maschine } from "../../../models/Maschine";
import { NotificationType } from "../../../models/NotificationType";
import { Produktionslinie } from "../../../models/Produktionslinie";
import { Station } from "../../../models/Station";
import {
  dtoToProduktionslinie,
  produktionslinieToDto,
} from "../../../models/mappers/produktionslinienMapper";
import {
  removeNonPersistentNotifications,
  replaceNotification,
} from "../../../util/notificationUtil";
import { Stueckzahl } from "../../../models/Stueckzahl";
import { Optimierung } from "../../../openapi";

/**
 * Globaler State des Editors.
 */
export interface EditorState {
  produktionslinie: Produktionslinie;
  simulationState: "running" | "canceled" | "finished" | "idle";
  isLoading: boolean;
  isError: boolean;
  error?: AxiosError;
  optimizationResult?: Optimierung;
  notifications: EditorNotification[];
  _socket?: EditorSocket;
}

/**
 * Initialer State des EditorStores.
 */
const initialState: EditorState = {
  produktionslinie: {
    id: "",
    stationen: [],
  },
  notifications: [],
  simulationState: "idle",
  isLoading: true,
  isError: false,
  error: undefined,
  _socket: undefined,
};

/**
 * Actions des EditorStores.
 */
export interface EditorActions {
  /**
   * Bewegt die gegebene Maschine in die gegebene Station.
   */
  moveMaschineToStation: (
    maschine_stueckzahl: Stueckzahl<Maschine>,
    station: Station
  ) => Promise<void>;
  /**
   * Entfernt die gegebene Maschine aus allen Stationen
   */
  resetMaschine: (maschine_stueckzahl: Stueckzahl<Maschine>) => Promise<void>;

  /**
   * Bewegt die gegebene Charge in die gegebene Station.
   */
  moveChargeToStation: (charge: Charge, station: Station) => Promise<void>;
  /**
   * Fügt die gegebene Station zur Produktionslinie hinzu.
   * Falls in der payload Maschine oder Charge gegeben ist, wird diese
   * der Station hinzugefügt.
   */
  addStation: (
    station: Station,
  ) => Promise<void>;
  /**
   * Aktualisiert die gegebene Station.
   */
  editStation: (station: Station) => Promise<void>;
  /**
   * Entfernt die gegebene Station aus der Produktionslinie.
   */
  removeStation: (station: Station) => Promise<void>;
  /**
   * Validiert die gegebene Produktionslinie, und startet eine neue Simulation, falls valide.
   */
  startSimulation: () => Promise<void>;
  /**
   * Entfernt die Notification mit der gegebenen ID.
   */
  removeNotification: (id: string) => Promise<void>;
  /**
   * initialisiert den editor state, indem Maschinen und Produktionslinie
   * von der Api gequeried werden
   */
  initState: () => Promise<void>;
  /**
   * Cleanup, wenn die View verlassen wird.
   */
  destroyState: () => Promise<void>;
}

/**
 * Erzeugt die Store Komponente für den EditorStore Provider.
 */
export const useCreateEditorState = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return () => {
    return create<EditorState & EditorActions>()((set, get) => ({
      ...initialState,

      async moveMaschineToStation(maschine_stueckzahl: Stueckzahl<Maschine>, station: Station) {
        const { produktionslinie, _socket } = get();

        // um move zwischen stationen abzudecken, Maschine entfernen.
        const cleanProduktionslinie = removeMaschineFromProduktionslinie(
          maschine_stueckzahl,
          produktionslinie
        );

        const newProduktionslinie = addMaschineToProduktionslinie(
          maschine_stueckzahl,
          station,
          cleanProduktionslinie
        );

        set({ produktionslinie: newProduktionslinie });

        _socket?.emit(
          "changeProduktionslinie",
          produktionslinieToDto(newProduktionslinie)
        );
      },

      async moveChargeToStation(charge: Charge, station: Station) {
        const { produktionslinie, _socket } = get();

        // um move zwischen stationen abzudecken, Charge entfernen.
        // const cleanProduktionslinie = removeChargeFromProduktionslinie(
        //   charge,
        //   produktionslinie
        // );

        const newProduktionslinie = addChargeToProduktionslinie(
          charge,
          station,
          produktionslinie
        );

        set({ produktionslinie: newProduktionslinie });

        _socket?.emit(
          "changeProduktionslinie",
          produktionslinieToDto(newProduktionslinie)
        );
      },

      async resetMaschine(maschine_stueckzahl) {
        const { produktionslinie, _socket } = get();

        const newProduktionslinie = removeMaschineFromProduktionslinie(
          maschine_stueckzahl,
          produktionslinie
        );

        set({ produktionslinie: newProduktionslinie });

        _socket?.emit(
          "changeProduktionslinie",
          produktionslinieToDto(newProduktionslinie)
        );
      },

      async addStation(station) {
        const { produktionslinie, _socket } = get();

        const newStation: Station = {
          ...station,
          maschinen_stueckzahl: [],
          chargen: [],
        };

        const newProduktionslinie: Produktionslinie = {
          ...produktionslinie,
          stationen: [...produktionslinie.stationen, newStation],
        };

        set({ produktionslinie: newProduktionslinie });

        _socket?.emit(
          "changeProduktionslinie",
          produktionslinieToDto(newProduktionslinie)
        );
      },

      async editStation(station) {
        const { produktionslinie, _socket } = get();

        const replacedProduktionslinie: Produktionslinie = {
          ...produktionslinie,
          stationen: produktionslinie.stationen.map((s) =>
            station.id === s.id ? station : s
          ),
        };

        set({ produktionslinie: replacedProduktionslinie });

        _socket?.emit(
          "changeProduktionslinie",
          produktionslinieToDto(replacedProduktionslinie)
        );
      },

      async removeStation(station) {
        const { produktionslinie, _socket } = get();

        const cleanProduktionslinie: Produktionslinie = {
          ...produktionslinie,
          stationen: produktionslinie.stationen.filter(
            (s) => s.id !== station.id
          ),
        };

        set({ produktionslinie: cleanProduktionslinie });

        _socket?.emit(
          "changeProduktionslinie",
          produktionslinieToDto(cleanProduktionslinie)
        );
      },

      async startSimulation() {
        const { _socket, produktionslinie } = get();

        _socket!.emit(
          "validateProduktionslinie",
          produktionslinieToDto(produktionslinie),
          (errors) => {
            set(({ notifications }) => ({
              notifications: [
                ...removeNonPersistentNotifications(notifications),
                ...errors,
              ],
            }));
            if (errors.filter((notification) => notification.type == NotificationType.Error).length == 0) {
              _socket?.emit(
                "startSimulation",
                produktionslinieToDto(produktionslinie)
              );
            }  
          }
        );
      },

      async removeNotification(id) {
        set(({ notifications }) => ({
          notifications: notifications.filter((n) => n.id !== id),
        }));
      },

      async initState() {
        set(initialState);

        const socket = api.socket();

        socket.on("simulationRunning", (id) => {
          const { notifications } = get();

          if (notifications.find((n) => n.id === id)) {
            set({ simulationState: "running" });
          } else {
            set({
              simulationState: "running",
              notifications: [
                ...notifications,
                {
                  id,
                  title: "editor.notifications.simulationRunning.title",
                  details: "editor.notifications.simulationRunning.details",
                  type: NotificationType.Info,
                  showProgressIndicator: true,
                  persistent: true,
                },
              ],
            });
          }
        });

        socket.on("simulationCanceled", (id) => {
          set(({ notifications }) => ({
            simulationState: "canceled",
            notifications: replaceNotification(notifications, {
              id,
              title: "editor.notifications.simulationCanceled.title",
              details: "editor.notifications.simulationCanceled.details",
              type: NotificationType.Info,
            }),
          }));
        });

        socket.on("simulationInfeasible", (id) => {
          set(({ notifications }) => ({
            simulationState: "canceled",
            notifications: replaceNotification(notifications, {
              id,
              title: "editor.notifications.simulationInfeasible.title",
              details: "editor.notifications.simulationInfeasible.details",
              type: NotificationType.Info,
            }),
          }));
        });

        socket.on("incompleteData", (id, details) => {
          set(({ notifications }) => ({
            simulationState: "canceled",
            notifications: replaceNotification(notifications, {
              id,
              title: "stammdaten.incomplete",
              details: details,
              type: NotificationType.Info,
            }),
          }));
        });

        socket.on("simulationFinished", (id, notificationDetails, optimizationResult) => {
          set(({ notifications }) => ({
            simulationState: "finished",
            notifications: replaceNotification(notifications, {
              title: "editor.notifications.simulationFinished.title",
              details: "",
              type: NotificationType.Success,
              showClose: true,
              ...notificationDetails,
              id,
            }),
            optimizationResult: optimizationResult
          }));
        });

        socket.on("produktionslinieChanged", async (produktionslinieDto) => {
          try {
            const [maschinen, chargen] = await Promise.all([
              queryClient.fetchQuery(
                allMaschinenQueryOptions(api, queryClient)
              ),
              queryClient.fetchQuery(allChargenQueryOptions(api, queryClient)),
            ]);

            const produktionslinie = dtoToProduktionslinie(
              produktionslinieDto,
              maschinen,
              chargen
            );

            set({
              produktionslinie,
              isLoading: false,
            });
          } catch (e) {
            set({ isLoading: false, isError: true, error: e as AxiosError });
          }
        });

        socket.connect();

        set({ _socket: socket });
      },

      async destroyState() {
        const { _socket } = get();

        _socket?.disconnect();
      },
    }));
  };
};

/**
 * Helper funktion welche die gegebene Maschine der gegebenen
 * Station in der gegebenen Produktionslinie hinzufügt.
 * @param maschine Zuzuordnende Maschine.
 * @param station Station der die Maschine zugewiesen werden soll.
 * @param produktionslinie Produktionslinie.
 * @returns
 */
function addMaschineToProduktionslinie(
  maschine_stueckzahl: Stueckzahl<Maschine>,
  station: Station,
  produktionslinie: Produktionslinie
): Produktionslinie {
  return {
    ...produktionslinie,
    stationen: produktionslinie.stationen.map((s) => {
      if (station?.id === s.id) {
        return {
          ...s,
          maschinen_stueckzahl: [...s.maschinen_stueckzahl, maschine_stueckzahl],
        };
      } else {
        return s;
      }
    }),
  };
}

/**
 * Helper funktion welche die gegebene Charge der gegebenen
 * Station in der gegebenen Produktionslinie hinzufügt.
 * @param charge Zuzuordnende Charge.
 * @param station Station der die Charge zugewiesen werden soll.
 * @param produktionslinie Produktionslinie.
 * @returns
 */
function addChargeToProduktionslinie(
  charge: Charge,
  station: Station,
  produktionslinie: Produktionslinie
): Produktionslinie {
  return {
    ...produktionslinie,
    stationen: produktionslinie.stationen.map((s) => {
      if (station?.id === s.id) {
        return {
          ...s,
          chargen: [...s.chargen, charge],
        };
      } else {
        return s;
      }
    }),
  };
}

/**
 * Entfernt die gegebene Maschine aus allen Stationen in der Produktionslinie.
 *
 * @param maschine Maschine die entfernt werden soll.
 * @param produktionslinie Produktionslinie aus der die Maschine entfernt
 *                         werden soll.
 * @returns Produktionslinie ohne die gegebene Maschine.
 */
function removeMaschineFromProduktionslinie(
  maschine_stueckzahl: Stueckzahl<Maschine>,
  produktionslinie: Produktionslinie
): Produktionslinie {
  return {
    ...produktionslinie,
    stationen: produktionslinie.stationen.map((station) => ({
      ...station,
      maschinen_stueckzahl: station.maschinen_stueckzahl.filter((x) => maschine_stueckzahl.objekt.id !== x.objekt.id),
    })),
  };
}

/**
 * Entfernt die gegebene Charge aus allen Stationen in der Produktionslinie.
 *
 * @param charge Charge die entfernt werden soll.
 * @param produktionslinie Produktionslinie aus der die Charge entfernt
 *                         werden soll.
 * @returns Produktionslinie ohne die gegebene Charge.
 */
// function removeChargeFromProduktionslinie(
//   charge: Charge,
//   produktionslinie: Produktionslinie
// ): Produktionslinie {
//   return {
//     ...produktionslinie,
//     stationen: produktionslinie.stationen.map((station) => ({
//       ...station,
//       chargen: station.chargen.filter(({ id }) => charge.id !== id),
//     })),
//   };
// }
