import { PropsWithChildren } from "react";
import { StoreApi } from "zustand";
import createContext from "zustand/context";

import {
  EditorActions,
  EditorState,
  useCreateEditorState,
} from "./editorState";

const { Provider, useStore } =
  createContext<StoreApi<EditorState & EditorActions>>();

/**
 * Hook der den EditorStore zur verfügung stellt.
 */
export const useEditorStore = useStore;

/**
 * Provider für den EditorStore
 */
export const EditorStateProvider = ({
  children,
}: PropsWithChildren): JSX.Element => {
  const createEditorState = useCreateEditorState();

  return <Provider createStore={createEditorState}>{children}</Provider>;
};
