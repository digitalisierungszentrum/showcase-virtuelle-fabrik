import { useDroppable } from "@dnd-kit/core";
import { Card, Title } from "@ui5/webcomponents-react";
import { useState } from "react";
import { useTranslation } from "react-i18next";

import { Produktionslinie } from "../../../../models/Produktionslinie";
import { DragData } from "../../DragData";
import { DropData } from "../../DropData";
import { ElementTypes } from "../ElementTypes";
import ChargeElement from "../charge/ChargeElement";
import PlaceholderCard from "../station/PlaceholderCard";
import UeChargenHeader from "./UebergreifendeChargenHeader";

interface UeChargenElementProps {
  /**
   * Produktionslinie, von der die übergreifenden Chargen angezeigt werden sollen.
   */
  produktionslinie: Produktionslinie;
  /**
   * Wenn true, wird die Station mit display=none gerendert. Das ist nötig damit
   * die Drop-Animation bei einer neuen Station richtig funktioniert.
   */
  hidden?: boolean;
}

/**
 * ÜbergreifendesChargenElement im Editor.
 */
const UeChargenElement = ({
  produktionslinie,
  hidden,
}: UeChargenElementProps): JSX.Element => {
  const { t } = useTranslation();
  const { isOver, setNodeRef, active } = useDroppable({
    id: "Unabhängige Chargen",
    data: {
      type: ElementTypes.UeChargen,
    } as DropData,
  });

  // Bestimmt ob die Station collapsed angezeigt werden soll.
  const [collapsed, setCollapsed] = useState(false);

  const ueChargenHeader = (
    <UeChargenHeader
      title={<Title>{t("editor.ueChargenName")}</Title>}
      collapsed={collapsed}
      onCollapse={() => setCollapsed(!collapsed)}
    />
  );

  if (collapsed) {
    return (
      <Card
        ref={setNodeRef as any}
        style={{
          margin: "1em auto",
          width: "25em",
          height: "9em",
          display: hidden ? "none" : "flex",
          flexDirection: "column",
          justifyContent: "stretch",
          alignItems: "stretch",
        }}
        header={ueChargenHeader}
      >
        <div
          style={{
            width: "auto",
            height: "10px",
            display: "grid",
            gridTemplateColumns: "100%",
            columnGap: "0.5em",
            padding: "1em 2em 0em 2em",
            justifyContent: "center",
            boxSizing: "border-box",
          }}
        >
          <Card
            style={{
              width: "100%",
              height: "200%",
              display: "flex",
              gridTemplateColumns: "100%",
              columnGap: "1em",
              justifyContent: "center",
              textAlign: "left",
              alignContent: "center",
              alignItems: "center",
              fontWeight: "bold",
              position: "relative",
              top: "50%",
            }}
          >
            <div
              style={{
                width: "100%",
                height: "auto",
                position: "relative",
                transform: "translateY(10px)",
                padding: "0px 20px",
              }}
            >
              {produktionslinie.chargen.length}{" "}
              {t("editor.chargenSectionHeader")}
            </div>
          </Card>
        </div>
      </Card>
    );
  }

  return (
    <Card
      ref={setNodeRef as any}
      style={{
        margin: "1em auto",
        width: "25em",
        display: hidden ? "none" : "flex",
        flexDirection: "column",
        justifyContent: "stretch",
        alignItems: "stretch",
      }}
      header={ueChargenHeader}
    >
      <div
        style={{
          width: "auto",
          minHeight: "21.5em",
          display: "grid",
          gridTemplateColumns: "100%",
          columnGap: "1em",
          padding: "1em 2em 1em 2em",
          justifyContent: "center",
        }}
      >
        <PlaceholderCard
          titleText={t("editor.chargenSectionHeader")}
          placeholderText={t("editor.noChargenPlaceholder")}
          isOver={
            (active?.data?.current as DragData)?.type === ElementTypes.Charge &&
            isOver
          }
        >
          {produktionslinie.chargen.map((charge) => (
            <ChargeElement key={charge.id} charge={charge} />
          ))}
        </PlaceholderCard>
      </div>
    </Card>
  );
};

export default UeChargenElement;
