import { Button, ButtonDesign, FlexBox } from "@ui5/webcomponents-react";
import { ReactNode } from "react";

interface UeChargenHeaderProps {
  /**
   * Titel Element.
   */
  title: ReactNode;
  /**
   * Slot wird von ui5 benötigt.
   */
  slot?: any;
  /**
   * bestimmt, wie der collapse button angezeigt wird.
   */
  collapsed?: boolean;
  /**
   * Wird aufgerufen, wenn der pfeil iconButton gedrückt wird.
   */
  onCollapse: () => void;
}

/**
 * Header für für das Editor ÜbergreifendeChargenElement.
 */
const UeChargenHeader = ({
  slot,
  title,
  collapsed,
  onCollapse,
}: UeChargenHeaderProps): JSX.Element => {
  return (
    <FlexBox
      slot={slot}
      direction="Row"
      justifyContent="SpaceBetween"
      alignItems="Center"
      style={{
        padding: "1em 1em",
      }}
    >
      {title}

      <div>
        <Button
          design={ButtonDesign.Transparent}
          icon={collapsed ? "navigation-down-arrow" : "navigation-up-arrow"}
          onClick={onCollapse}
        />
      </div>
    </FlexBox>
  );
};

export default UeChargenHeader;
