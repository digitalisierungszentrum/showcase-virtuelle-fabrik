import { ResponsiveGridLayout } from "@ui5/webcomponents-react";
import { useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { v4 as uuidv4 } from "uuid";

import { Station } from "../../../../models/Station";
import { useEditorStore } from "../../store/EditorStateProvider";
import NeueStationElement from "../neue-station/NeueStationElement";
import StationElement from "../station/StationElement";

/**
 * Erzeugt eine ID für eine neue Station.
 *
 * @param produktionslinie Produktionslinie in der geschaut wird,
 * ob die generierte ID schon existiert.
 * @returns Neuer Stationsname.
 */
function getIdForNewStation(): string {
  return uuidv4();
}

/**
 * Die Area des Editors.
 *
 * Hier wird bereits eine neueStation erzeugt, die von NeueStationElement
 * hinzugefügt wird. Grund: Damit Drop Animation beim fallen lassen eines
 * Elements ,auf dem NeueStationElement, richtig funktioniert, muss die Station
 * die erstellt wird, bereits gerendert werden. Deswegen wird die neue Station
 * bereits hinzugefügt und auf hidden gestellt.
 */
const Area = (): JSX.Element => {
  const { t } = useTranslation();
  const isLoading = useEditorStore((s) => s.isLoading);
  const produktionslinie = useEditorStore((s) => s.produktionslinie);
  const { stationen } = produktionslinie;
  const addStation = useEditorStore((s) => s.addStation);

  const newStation: Station = useMemo(
    () => ({
      id: getIdForNewStation(),
      name: t("editor.neueStationDefaultName"),
      order: stationen.length ?? 0,
      maschinen_stueckzahl: [],
      chargen: [],
      budget_optimierung: 0,
    }),
    [t, produktionslinie]
  );

  useEffect(() => {
    if(stationen.length === 0) {
      addStation(newStation);
    }
  }, [stationen]);

  return (
    <ResponsiveGridLayout
      style={{
        alignItems: "start",
        justifyContent: "start",
        marginLeft: "35em",
        width: "calc(100% - 15em)",
        height: "100%",
        gridAutoRows: "min-content",
      }}
      rowGap="1rem"
      columnsS={1}
      columnsM={1}
      columnsL={2}
      columnsXL={3}
      columnSpanS={1}
      columnSpanM={1}
      columnSpanL={1}
      columnSpanXL={1}
    >
      {isLoading ||
        stationen
          .sort((a, b) => a.order - b.order)
          .map((station, i) => (
            <StationElement
              key={station.id}
              station={station}
              // hidden={i === stationen.length}
            />
          ))}
      {/* <NeueStationElement station={newStation} /> */}
      {/* <UeChargenElement produktionslinie={produktionslinie} /> */}
    </ResponsiveGridLayout>
  );
};

export default Area;
