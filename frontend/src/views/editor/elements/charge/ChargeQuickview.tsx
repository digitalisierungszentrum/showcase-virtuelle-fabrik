import {
  Form,
  FormItem,
  Label,
  ResponsivePopover,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import { useArbeitsschritte } from "../../../../hooks/useArbeitsschritte";
import { useProdukte } from "../../../../hooks/useProdukte";
import { Charge } from "../../../../models/Charge";

interface ChargeQuickviewProps {
  /**
   * Charge die angezeigt werden soll.
   */
  charge: Charge;
  /**
   * Ob die Quickview angezeigt werden soll.
   */
  open?: boolean;
  /**
   * ID des Elements, zu dem diese Quickview gehört
   */
  opener: string;
  /**
   * Wird aufgerufen wenn die Quickview sich schließt.
   */
  onClose: () => void;
}

/**
 * Quickview eines Chargen-Elements im Editor. Kann geöffnet werden durch
 * klicken auf ein Chargen-Element.
 */
const ChargeQuickview = ({
  charge,
  open,
  opener,
  onClose,
}: ChargeQuickviewProps): JSX.Element => {
  const { t } = useTranslation();
  const arbeitsschrittQuery = useArbeitsschritte();
  const produkteQuery = useProdukte();

  const produktBedarf = useMemo(
    () => ({
      ...charge.produktbedarf[0],
      ...produkteQuery.data?.find(
        ({ id }) => id === charge.produktbedarf[0]?.produktId
      ),
    }),
    [charge, produkteQuery]
  );

  // baut eine Liste der Produktionsschritte, des Produkts zusammen
  const produktionsSchritte = useMemo(
    () =>
      produktBedarf?.produktionsschritte?.map((schritt) => ({
        schritt: schritt.schritt,
        ...arbeitsschrittQuery.data?.find(
          (arbeitsschritt) => arbeitsschritt.id === schritt.id
        ),
      })),
    [produktBedarf, arbeitsschrittQuery]
  );

  return (
    <ResponsivePopover
      open={open}
      opener={opener}
      onAfterClose={onClose}
      headerText={charge.name}
    >
      <Form
        columnsL={1}
        columnsXL={1}
        columnsM={1}
        columnsS={1}
        labelSpanXL={12}
        labelSpanL={12}
        labelSpanM={12}
      >
        <FormItem label={t("stammdaten.chargen.stueckzahl")}>
          <Text>{produktBedarf.stueckzahl ?? 0}</Text>
        </FormItem>

        <FormItem label={t("stammdaten.chargen.produktName")}>
          <Text>{produktBedarf?.name ?? ""}</Text>
        </FormItem>

        <FormItem label={t("stammdaten.produkte.verkaufspreis")}>
          <Text>{produktBedarf?.verkaufspreis ?? ""} &euro;</Text>
        </FormItem>

        <FormItem label={t("stammdaten.produkte.energiekosten")}>
          <Text>{produktBedarf?.energiekosten ?? ""} &euro;</Text>
        </FormItem>

        <FormItem label={t("stammdaten.chargen.gesamtVerkaufspreis")}>
          <Text>
            {(produktBedarf?.stueckzahl ?? 0) *
              (produktBedarf?.verkaufspreis ?? 0)}{" "}
            &euro;
          </Text>
        </FormItem>

        <FormItem label={t("stammdaten.chargen.gesamtEnergiekosten")}>
          <Text>
            {(produktBedarf?.stueckzahl ?? 0) *
              (produktBedarf?.energiekosten ?? 0)}{" "}
            &euro;
          </Text>
        </FormItem>
      </Form>

      <Title
        level="H3"
        style={{
          margin: "1em 0",
        }}
      >
        {t("stammdaten.produkte.sectionBefaehigungen")}
      </Title>

      <Table
        busy={arbeitsschrittQuery.isLoading}
        noDataText={t("stammdaten.arbeitsschritte.noArbeitsschritte")}
        style={{
          maxWidth: "20em",
        }}
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.produkte.schrittOrder")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.produkte.arbeitsschritt")}</Label>
            </TableColumn>
          </>
        }
      >
        {produktionsSchritte?.map(({ name }, i) => (
          <TableRow key={i}>
            <TableCell>{i + 1}</TableCell>
            <TableCell>{name}</TableCell>
          </TableRow>
        ))}
      </Table>
    </ResponsivePopover>
  );
};

export default ChargeQuickview;
