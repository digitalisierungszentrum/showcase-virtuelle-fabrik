import { useDraggable } from "@dnd-kit/core";
import { StandardListItem } from "@ui5/webcomponents-react";
import { ThemingParameters } from "@ui5/webcomponents-react-base";
import { useEffect, useState } from "react";
import { createPortal } from "react-dom";

import { Charge } from "../../../../models/Charge";
import { DragData } from "../../DragData";
import { ElementTypes } from "../ElementTypes";
import ChargeQuickview from "./ChargeQuickview";

interface ChargeElementProps {
  /**
   * Charge die von diesem Element dar gestellt werden soll.
   */
  charge: Charge;

  /**
   * Wenn true, wird das Element als Element für das Draggen dar gestellt.
   */
  isOverlay?: boolean;
}

/**
 * Chargen Element im Editor.
 */
const ChargeElement = ({
  charge,
  isOverlay,
}: ChargeElementProps): JSX.Element => {
  const { attributes, listeners, setNodeRef, isDragging } = useDraggable({
    id: charge.id,
    data: {
      type: ElementTypes.Charge,
      charge,
    } as DragData,
  });

  const [showQuickview, setShowQuickview] = useState(false);

  useEffect(() => {
    if (isDragging) {
      setShowQuickview(false);
    }
  }, [isDragging, setShowQuickview]);

  return (
    <>
      <StandardListItem
        id={charge.id}
        ref={setNodeRef as any}
        {...listeners}
        {...attributes}
        key={charge.id}
        onClick={(e) => {
          e.stopPropagation();
          setShowQuickview(!showQuickview);
        }}
        icon="vertical-grip"
        style={{
          borderRadius: ".25em",
          ...(isDragging
            ? {
                opacity: 0,
              }
            : {}),
          ...(isOverlay
            ? {
                backgroundColor: ThemingParameters.sapNeutralBackground,
              }
            : {}),
        }}
      >
        {charge.name}
      </StandardListItem>

      {createPortal(
        <ChargeQuickview
          charge={charge}
          open={showQuickview}
          opener={charge.id}
          onClose={() => setShowQuickview(false)}
        />,
        document.body
      )}
    </>
  );
};

export default ChargeElement;
