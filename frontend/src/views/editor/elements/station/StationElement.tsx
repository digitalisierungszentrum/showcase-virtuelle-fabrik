import { useDroppable } from "@dnd-kit/core";
import { Card, FlexBox, FormItem, Input, InputType, Label, List, StandardListItem, Title } from "@ui5/webcomponents-react";
import { useEffect, useMemo, useState } from "react";
import { createPortal } from "react-dom";
import { useTranslation } from "react-i18next";

import AppSelect from "../../../../components/arbeitsschritt-select/AppSelect";
import { useChargen } from "../../../../hooks/useChargen";
import { Charge } from "../../../../models/Charge";
import { Station } from "../../../../models/Station";
import { DragData } from "../../DragData";
import { DropData } from "../../DropData";
import { useEditorStore } from "../../store/EditorStateProvider";
import { ElementTypes } from "../ElementTypes";
import MaschineElement from "../maschine/MaschineElement";
import PlaceholderCard from "./PlaceholderCard";
import StationHeader from "./StationHeader";
import StationQuickview from "./StationQuickview";

interface StationElementProps {
  /**
   * Station die von dieser Component dar gestellt werden soll.
   */
  station: Station;
  /**
   * Wenn true, wird die Station mit display=none gerendert. Das ist nötig damit
   * die Drop-Animation bei einer neuen Station richtig funktioniert.
   */
  hidden?: boolean;
}

/**
 * StationElement im Editor.
 */
const StationElement = ({
  station,
  hidden,
}: StationElementProps): JSX.Element => {
  const { t } = useTranslation();
  const {
    isOver,
    node: nodeRef,
    setNodeRef,
    active,
  } = useDroppable({
    id: station.id,
    data: {
      type: ElementTypes.Station,
      station: station,
    } as DropData,
  });

  const { data: chargen } = useChargen();

  const [showQuickview, setShowQuickview] = useState(false);

  useEffect(() => {
    if (active) {
      setShowQuickview(false);
    }
  }, [active, setShowQuickview]);

  const editStation = useEditorStore((s) => s.editStation);
  const moveChargeToStation = useEditorStore((s) => s.moveChargeToStation);
  const removeStation = useEditorStore((s) => s.removeStation);

  // Bestimmt ob der Rename Input angezeigt werden soll
  const [showRename, setShowRename] = useState(false);
  // beinhaltet den neuen namen der Station, bei einem rename
  const [newName, setNewName] = useState(station.name);

  // Bestimmt ob die Station collapsed angezeigt werden soll.
  const [collapsed, setCollapsed] = useState(false);

  // Berechnet die Anschaffungskosten der aktuellen Maschinenauswahl. 
  const currentAnschaffungskosten = useMemo(
    () =>
      station.maschinen_stueckzahl
        .reduce((acc, x) => acc + x.stueckzahl * x.objekt.anschaffungskosten, 0),
    [station]
  );

  // editiert die Station und schlißt das name Input Feld.
  function renameStation() {
    if (newName !== station.name) {
      editStation({
        ...station,
        name: newName,
      });
    }
    setShowRename(false);
  }

  function setCharge(charge?: Charge) {
    editStation({
      ...station,
      chargen: charge ? [charge] : [],
    });
  }

  function updateStueckzahl(maschineId: string, newStueckzahl: number) {
    editStation({
      ...station,
      maschinen_stueckzahl: station.maschinen_stueckzahl.map((x) => {
        if (x.objekt.id === maschineId) {
          return { objekt: x.objekt, stueckzahl: newStueckzahl };
        }
        return x;
      }),
    });
  }

  function updateOptimierungsbudget(budget: number) {
    editStation({
      ...station,
      budget_optimierung: budget,
    })
  }

  const stationHeader = () => (
    <StationHeader
      title={
        <FlexBox
          direction="Column"
          alignItems="Start"
          style={{
            minWidth: "80%",
            maxWidth: "80%",
          }}
        >
          {showRename ? (
            <Input
              ref={(input) => input?.focus()}
              value={newName}
              onInput={(e) => setNewName(e.target.value ?? "")}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  renameStation();
                }
              }}
              onBlur={renameStation}
            />
          ) : (
            <Title
              wrappingType="None"
              style={{
                maxWidth: "80%",
              }}
              onClick={(e) => {
                e.stopPropagation();
                setShowRename(true);
              }}
            >
              {station.name}
            </Title>
          )}

          {!collapsed && chargen && (
            <FlexBox alignItems="Center">
              <Label>{t("stammdaten.chargen.name")}:</Label>
              <AppSelect
                name="select"
                onChange={setCharge}
                data={chargen}
                placeholder={t("editor.selectChargePlaceholder")}
                selected={station.chargen[0]?.id ?? ""}
                fitContent
              />
            </FlexBox>
          )}

          {!collapsed && chargen && (
            <FlexBox style={{marginBottom: "0.5em"}}>
              <Label>{t("editor.anschaffungskosten.aktuell") + ":"}</Label>
              <text style={{marginLeft: "0.2em"}}> {currentAnschaffungskosten} &euro;</text>
            </FlexBox>
          )}

          {!collapsed && chargen && (
            <FormItem label={t("editor.anschaffungskosten.optimierung")}>
              <Input
              name={"OptimierungsbudgetInput"}
              aria-labelledby={"OptimierungsbudgetInput"}
              role="textbox"
              type={InputType.Number}
              icon={<span>&euro;</span>}
              onInput={(event) => updateOptimierungsbudget(parseFloat(event.target.value ?? "0"))}
              onClick={(e) => {
                e.stopPropagation();
              }}
              value={station.budget_optimierung + ""}
            />
            </FormItem>
          )}
        </FlexBox>
      }
      collapsed={collapsed}
      onCollapse={() => setCollapsed(!collapsed)}
      onRemove={() => {
        return;
      }}
    />
  );

  const collapsedStation = () => (
    <Card
      id={station.id}
      ref={setNodeRef as any}
      onClick={() => setShowQuickview(!showQuickview)}
      style={{
        margin: "1em auto",
        width: "25em",
        display: hidden ? "none" : "flex",
        flexDirection: "column",
        justifyContent: "stretch",
        alignItems: "stretch",
        borderRadius: ".25em",
      }}
      header={stationHeader()}
    >
      <div
        style={{
          width: "auto",
          display: "flex",
          padding: "0em 2em 0em 2em",
          justifyContent: "center",
          boxSizing: "border-box",
        }}
      >
        <Card
          style={{
            width: "100%",
            display: "flex",
            margin: "1em",
            justifyContent: "center",
            textAlign: "left",
            alignContent: "center",
            fontWeight: "bold",
          }}
        >
          <List>
            {station.maschinen_stueckzahl.map((x) => (
              <StandardListItem key={x.objekt.name}>
                {x.stueckzahl}x {x.objekt.name}
              </StandardListItem>
            ))}
          </List>
        </Card>
      </div>
    </Card>
  );

  const expandedStation = () => (
    <Card
      ref={setNodeRef as any}
      id={station.id}
      onClick={() => setShowQuickview(!showQuickview)}
      style={{
        margin: "1em auto",
        width: "25em",
        display: hidden ? "none" : "flex",
        flexDirection: "column",
        justifyContent: "stretch",
        alignItems: "stretch",
      }}
      header={stationHeader()}
    >
      <div
        style={{
          width: "auto",
          minHeight: "20em",
          display: "grid",
          gridTemplateColumns: "100%",
          columnGap: "1em",
          padding: "1em 2em 1em 2em",
          justifyContent: "center",
        }}
      >
        <PlaceholderCard
          titleText={t("editor.maschinenSectionHeader")}
          placeholderText={t("editor.noMaschinenPlaceholder")}
          isOver={
            (active?.data?.current as DragData)?.type ===
              ElementTypes.Maschine && isOver
          }
        >
          {station.maschinen_stueckzahl.map((x) => {
            return (
              <MaschineElement
                key={x.objekt.id}
                maschine_stueckzahl={x}
                onStueckzahlChange={updateStueckzahl}
              />
            );
          })}
        </PlaceholderCard>
      </div>
    </Card>
  );

  return (
    <>
      {collapsed ? collapsedStation() : expandedStation()}
      {createPortal(
        <StationQuickview
          station={station}
          open={showQuickview}
          opener={station.id}
          onClose={() => setShowQuickview(false)}
        />,
        document.body
      )}
    </>
  );
};

export default StationElement;
