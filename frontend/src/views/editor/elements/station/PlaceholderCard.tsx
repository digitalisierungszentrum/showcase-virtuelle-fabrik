import { CardHeader, List } from "@ui5/webcomponents-react";
import { ThemingParameters } from "@ui5/webcomponents-react-base";
import { ReactNode } from "react";

import HighlightCard from "../../../../components/highlight-card/HightlightCard";

interface PlaceholderCardProps {
  /**
   * zeight den Highlight Rahmen wenn true
   */
  isOver?: boolean;
  /**
   * Text der als Titel der Card angezeigt wird
   */
  titleText: string;
  /**
   * Text der angezeigt werden soll wenn das array leer ist.
   */
  placeholderText: string;
  /**
   * Listen elemente die Angezeigt werden sollen
   */
  children: ReactNode[];
}

/**
 * Placeholder Card für das Editor StationElement.
 * Wenn das children array leer ist wird ein placeholder angezeigt.
 * Bietet zusätzlich highlighting für drag and drop.
 */
const PlaceholderCard = ({
  isOver,
  titleText,
  placeholderText,
  children,
}: PlaceholderCardProps): JSX.Element => {
  const placeholder = (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        padding: "0.5em 0.5em",
        textAlign: "center",
        color: "#999",
        borderRadius: "0.5em",
        borderWidth: "4px",
        borderStyle: "dashed",
        borderColor: "#999",
        boxSizing: "border-box",
        ...(isOver
          ? {
              borderColor: ThemingParameters.sapPositiveColor,
              color: ThemingParameters.sapPositiveColor,
            }
          : {}),
      }}
    >
      {placeholderText}
    </div>
  );

  return (
    <div>
      {children.length > 0 || placeholder}
      <HighlightCard
        header={<CardHeader titleText={titleText}></CardHeader>}
        style={{
          display: children.length > 0 ? "flex" : "none",
          width: "100%",
          height: "100%",
          opacity: children.length > 0 ? "inherit" : "0",
        }}
        active={children.length > 0 && isOver}
        activeStyle={{
          borderWidth: "4px",
          borderStyle: "solid",
          borderColor: ThemingParameters.sapPositiveColor,
          borderRadius: ThemingParameters.sapElement_BorderCornerRadius,
        }}
      >
        <List>{children}</List>
      </HighlightCard>
    </div>
  );
};

export default PlaceholderCard;
