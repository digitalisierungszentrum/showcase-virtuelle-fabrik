import {
  FormItem,
  Label,
  ResponsivePopover,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import { useArbeitsschritte } from "../../../../hooks/useArbeitsschritte";
import { Station } from "../../../../models/Station";

interface StationQuickviewProps {
  /**
   * Station die angezeigt werden soll.
   */
  station: Station;
  /**
   * Ob die Quickview angezeigt werden soll.
   */
  open?: boolean;
  /**
   * ID des Elements, zu dem diese Quickview gehört
   */
  opener: string;
  /**
   * Wird aufgerufen wenn die Quickview sich schließt.
   */
  onClose: () => void;
}

/**
 * Quickview eines Stations-Elements im Editor. Kann geöffnet werden durch
 * klicken auf ein Stations-Element.
 */
const StationQuickview = ({
  station,
  open,
  opener,
  onClose,
}: StationQuickviewProps): JSX.Element => {
  const { t } = useTranslation();
  const { data: arbeitsschritte, isLoading, isError } = useArbeitsschritte();

  const maxKosten = useMemo(
    () =>
      station.maschinen_stueckzahl
        .map(({objekt}) => objekt.kostenMinute)
        .reduce((acc, b) => acc + b, 0),
    [station]
  );

  const befaehigungen = useMemo(
    () =>
      station.maschinen_stueckzahl
        .flatMap((m) =>
          m.objekt.maschinenbefaehigungen.map((b) => ({
            ...b,
            maschine: m.objekt.name,
            name: arbeitsschritte?.find(({ id }) => id === b.schrittId)?.name,
          }))
        )
        .sort((a, b) => a.schrittId.localeCompare(b.schrittId)),
    [station, arbeitsschritte]
  );

  return (
    <ResponsivePopover
      open={open}
      opener={opener}
      onAfterClose={onClose}
      headerText={station.name}
    >
      <FormItem label={t("editor.stationMaxKosten")}>
        <Text>{maxKosten} &euro;</Text>
      </FormItem>

      <Title
        level="H3"
        style={{
          margin: "1em 0",
        }}
      >
        {t("stammdaten.maschinen.befaehigungen")}
      </Title>

      <Table
        busy={isLoading}
        noDataText={t(
          isError
            ? "stammdaten.tableUnexpectedError"
            : "stammdaten.maschinen.noBefaehigungen"
        )}
        style={{
          maxWidth: "20em",
        }}
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.maschinen.befaehigungName")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.maschinen.name")}</Label>
            </TableColumn>
            <TableColumn style={{ textAlign: "end", paddingRight: "1em" }}>
              <Label
                style={{
                  width: "calc(100% - .5em)",
                  textAlign: "end",
                  marginRight: "1em",
                }}
              >
                {t("stammdaten.maschinen.befaehigungTaktrate")}
              </Label>
            </TableColumn>
          </>
        }
      >
        {befaehigungen.map(({ taktrate, maschine, name }, index) => (
          <TableRow key={index}>
            <TableCell>{name ?? ""}</TableCell>
            <TableCell>{maschine}</TableCell>
            <TableCell style={{ textAlign: "end", paddingRight: "1em" }}>
              {taktrate}
            </TableCell>
          </TableRow>
        ))}
      </Table>
    </ResponsivePopover>
  );
};

export default StationQuickview;
