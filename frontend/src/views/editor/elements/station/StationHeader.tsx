import { Button, ButtonDesign, FlexBox } from "@ui5/webcomponents-react";
import { ReactNode } from "react";

interface StationHeaderProps {
  /**
   * Titel Element.
   */
  title: ReactNode;
  /**
   * Slot wird von ui5 benötigt.
   */
  slot?: any;
  /**
   * bestimmt, wie der collapse button angezeigt wird.
   */
  collapsed?: boolean;
  /**
   * Wird aufgerufen, wenn der decline IconButton gedrückt wird.
   */
  onRemove: () => void;
  /**
   * Wird aufgerufen, wenn der pfeil iconButton gedrückt wird.
   */
  onCollapse: () => void;
}

/**
 * Header für für das Editor StationElement.
 */
const StationHeader = ({
  slot,
  title,
  collapsed,
  onRemove,
  onCollapse,
}: StationHeaderProps): JSX.Element => {
  return (
    <FlexBox
      slot={slot}
      direction="Row"
      justifyContent="SpaceBetween"
      alignItems="Start"
      style={{
        padding: "1em 1em",
      }}
    >
      {title}
      <div>
        <Button
          design={ButtonDesign.Transparent}
          icon={collapsed ? "navigation-down-arrow" : "navigation-up-arrow"}
          onClick={(e) => {
            e.stopPropagation();
            onCollapse();
          }}
        />
        <Button
          design={ButtonDesign.Transparent}
          icon="decline"
          onClick={(e) => {
            e.stopPropagation();
            onRemove();
          }}
        />
      </div>
    </FlexBox>
  );
};

export default StationHeader;
