import {
  Title,
} from "@ui5/webcomponents-react";
import { ReactElement} from "react";
import { useEditorStore } from "../../store/EditorStateProvider";
import OptimierungComponent2 from "./OptimizationComponent2";

const Dashboard2 = (): JSX.Element => {

  const optimizationResult = useEditorStore((s) => s.optimizationResult);

  return (
    <div style={{ padding:"16px"}}>
      {!optimizationResult &&
      <div>
        <Title style={{marginBottom: "16px"}}>
          Optimierungsergebnis
        </Title>
        <text style={{color: "grey", padding: "3px"}}>
          Hier wird das Ergebnis der Optimierung angezeigt nachdem die Simulation erfolgreich beendet wurde.
        </text>
      </div>
      }
      {optimizationResult &&
        optimizationResult.stationen
          .map((station, i) => {
            return (
              <div
                key={station.station}
                id={station.station}
              >
                <Title
                  style={{
                    ...(i === 0
                      ? {}
                      : {
                          marginTop: "-1em",
                        }),
                    marginBottom: "1em",
                  }}
                >
                  {station.name}
                </Title>
                <OptimierungComponent2 optimierungsergebnis={station} />
              </div>
            );
          })
          .filter((e): e is ReactElement => !!e)
      }
    </div>
  );
};

export default Dashboard2;
