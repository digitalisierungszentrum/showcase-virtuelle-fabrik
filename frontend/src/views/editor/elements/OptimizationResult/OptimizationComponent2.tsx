import { FlexBox, ResponsiveGridLayout } from "@ui5/webcomponents-react";

import { OptimierungsErgebnis } from "../../../../openapi";
import ErgebnisComponent2 from "./ErgebnisComponent2";
import {ThemingParameters} from "@ui5/webcomponents-react-base";

interface OptimierungComponentProps {
  optimierungsergebnis: OptimierungsErgebnis;
}

const OptimierungComponent2 = ({
  optimierungsergebnis,
}: OptimierungComponentProps) => {

  const isOptimal =
    optimierungsergebnis.gegeben.kostenProdukt <=
    optimierungsergebnis.optimiert.kostenProdukt;

  return (
    <>
      <FlexBox justifyContent="SpaceBetween">
        <div style={{ marginBottom: "2em" }}>
          <div>
            Charge:{" "}
            <span style={{ fontWeight: "600" }}>{optimierungsergebnis.charge}</span>
          </div>
          <div>
            <span>Produkt: {optimierungsergebnis.produkt}</span>
          </div>
          <div>
            <span>
              Produktbedarf: {optimierungsergebnis.stueckzahl} Stk/Minute
            </span>
          </div>
          <div style={{marginTop: "1em"}}>
            {isOptimal ? (
          <span style={{color: ThemingParameters.sapWarningColor}}>
            Die erstellte Produktionslinie kann nicht mehr weiter optimiert
            werden!
          </span>
        ) : (
          <span style={{color: ThemingParameters.sapPositiveColor}}>
            Es wurde eine optimierte Konfiguration gefunden!
          </span>
        )}
          </div>
        </div>
        <div />
      </FlexBox>

      <ResponsiveGridLayout columnsXL={2} columnsL={2} columnsM={2} columnsS={1}>
        <div>
          <ErgebnisComponent2
            title="Erstellt"
            ergebnis={optimierungsergebnis.gegeben}
            good={isOptimal}
          />
        </div>
        <div>
          <ErgebnisComponent2
            title="Optimiert"
            ergebnis={optimierungsergebnis.optimiert}
            good
          />
        </div>
      </ResponsiveGridLayout>
    </>
  );
};

export default OptimierungComponent2;
