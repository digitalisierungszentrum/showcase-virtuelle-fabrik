/**
 * Enum aller Elemente mit Drag and Drop interaktion im Editor.
 */
export enum ElementTypes {
  Station = "Station",
  Charge = "Charge",
  Maschine = "Maschine",
  NeueStation = "NeueStation",
  UeChargen = "UeChargen",
}
