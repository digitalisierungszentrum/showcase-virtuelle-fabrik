import {
  Form,
  FormItem,
  Label,
  ResponsivePopover,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { useTranslation } from "react-i18next";

import { useArbeitsschritte } from "../../../../hooks/useArbeitsschritte";
import { Maschine } from "../../../../models/Maschine";
import { secondsToTimeString } from "../../../../util/misc";

interface MaschineQuickviewProps {
  /**
   * Maschine die angezeigt werden soll.
   */
  maschine: Maschine;
  /**
   * Ob die Quickview angezeigt werden soll.
   */
  open?: boolean;
  /**
   * ID des Elements, zu dem diese Quickview gehört
   */
  opener: string;
  /**
   * Wird aufgerufen wenn die Quickview sich schließt.
   */
  onClose: () => void;
}

/**
 * Quickview eines Maschinen-Elements im Editor. Kann geöffnet werden durch
 * klicken auf ein Maschinen-Element.
 */
const MaschineQuickview = ({
  maschine,
  open,
  opener,
  onClose,
}: MaschineQuickviewProps): JSX.Element => {
  const { t } = useTranslation();
  const arbeitsschrittQuery = useArbeitsschritte();

  return (
    <ResponsivePopover
      open={open}
      opener={opener}
      onAfterClose={onClose}
      headerText={maschine.name}
    >
      <Form
        columnsL={1}
        columnsXL={1}
        columnsM={1}
        columnsS={1}
        labelSpanXL={12}
        labelSpanL={12}
        labelSpanM={12}
      >
        <FormItem label={t("stammdaten.maschinen.mitarbeiterMin")}>
          <Text>{maschine.mitarbeiterMin}</Text>
        </FormItem>
        <FormItem label={t("stammdaten.maschinen.mitarbeiterMax")}>
          <Text>{maschine.mitarbeiterMax}</Text>
        </FormItem>
        <FormItem label={t("stammdaten.maschinen.ruestzeit")}>
          <Text>{secondsToTimeString(maschine.ruestzeit)}</Text>
        </FormItem>
        <FormItem label={t("stammdaten.maschinen.kostenMinute")}>
          <Text>{maschine.kostenMinute} &euro;</Text>
        </FormItem>
        <FormItem label={t("stammdaten.maschinen.anschaffungskosten")}>
          <Text>{maschine.anschaffungskosten} &euro;</Text>
        </FormItem>
        <FormItem label={t("stammdaten.maschinen.ausfallWahrscheinlichkeit")}>
          <Text>{maschine.ausfallWahrscheinlichkeit * 100} %</Text>
        </FormItem>
      </Form>

      <Title
        level="H3"
        style={{
          margin: "1em 0",
        }}
      >
        {t("stammdaten.maschinen.befaehigungen")}
      </Title>

      <Table
        busy={arbeitsschrittQuery.isLoading}
        noDataText={t("stammdaten.maschinen.noBefaehigungen")}
        style={{
          maxWidth: "20em",
        }}
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.maschinen.befaehigungName")}</Label>
            </TableColumn>
            <TableColumn style={{ textAlign: "end", paddingRight: "1em" }}>
              <Label
                style={{
                  width: "calc(100% - .5em)",
                  textAlign: "end",
                  marginRight: "1em",
                }}
              >
                {t("stammdaten.maschinen.befaehigungTaktrate")}
              </Label>
            </TableColumn>
          </>
        }
      >
        {maschine.maschinenbefaehigungen.map(({ schrittId, taktrate }) => (
          <TableRow key={schrittId}>
            <TableCell>
              {arbeitsschrittQuery.data?.find(
                (schritt) => schritt.id === schrittId
              )?.name || ""}
            </TableCell>
            <TableCell style={{ textAlign: "end", paddingRight: "1em" }}>
              {taktrate}
            </TableCell>
          </TableRow>
        ))}
      </Table>
    </ResponsivePopover>
  );
};

export default MaschineQuickview;
