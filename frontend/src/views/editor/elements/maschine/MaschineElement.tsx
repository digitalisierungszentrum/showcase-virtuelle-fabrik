import { useDraggable } from "@dnd-kit/core";
import { StandardListItem, StepInput } from "@ui5/webcomponents-react";
import { ThemingParameters } from "@ui5/webcomponents-react-base";
import { useEffect, useState } from "react";
import { createPortal } from "react-dom";

import { Maschine } from "../../../../models/Maschine";
import { Stueckzahl } from "../../../../models/Stueckzahl";
import { DragData } from "../../DragData";
import { ElementTypes } from "../ElementTypes";
import MaschineQuickview from "./MaschineQuickview";

interface MaschineStueckzahlElementProps {
  /**
   * Maschine die von diesem Element dar gestellt werden soll.
   */
  maschine_stueckzahl: Stueckzahl<Maschine>;
  /**
   * Wenn true, wird das Element als Element für das Draggen dar gestellt.
   */
  isOverlay?: boolean;
  onStueckzahlChange: (maschineId: string, newStueckzahl: number) => void;
}

/**
 * Maschinen Element im Editor.
 */
const MaschineElement = ({
  maschine_stueckzahl,
  isOverlay,
  onStueckzahlChange,
}: MaschineStueckzahlElementProps): JSX.Element => {
  const { attributes, listeners, setNodeRef, isDragging } = useDraggable({
    id: maschine_stueckzahl.objekt.id,
    data: {
      type: ElementTypes.Maschine,
      maschine_stueckzahl: maschine_stueckzahl,
    } as DragData,
  });

  const [showQuickview, setShowQuickview] = useState(false);

  useEffect(() => {
    if (isDragging) {
      setShowQuickview(false);
    }
  }, [isDragging, setShowQuickview]);

  return (
    <div
      style={{
        display: "grid",
        gridTemplateColumns: "1fr 120px",
      }}
    >
      <StandardListItem
        id={maschine_stueckzahl.objekt.id}
        ref={setNodeRef as any}
        {...listeners}
        {...attributes}
        key={maschine_stueckzahl.objekt.id}
        onClick={(e) => {
          e.stopPropagation();
          setShowQuickview(!showQuickview);
        }}
        icon="vertical-grip"
        style={{
          display: "flex",
          flexDirection: "column",
          borderRadius: ".25em",
          ...(isDragging
            ? {
                opacity: 0,
              }
            : {}),
          ...(isOverlay
            ? {
                backgroundColor: ThemingParameters.sapNeutralBackground,
              }
            : {}),
        }}
      >
        {maschine_stueckzahl.objekt.name}
      </StandardListItem>
      <StepInput
        style={{ maxWidth: "70px" }}
        value={maschine_stueckzahl.stueckzahl}
        step={1}
        min={1}
        max={10000}
        onClick={(event) => event.stopPropagation()}
        onChange={(event) => {
          onStueckzahlChange(
            maschine_stueckzahl.objekt.id,
            event?.target?.value ?? maschine_stueckzahl.stueckzahl
          );
          event.stopPropagation();
        }}
      />
      {createPortal(
        <MaschineQuickview
          maschine={maschine_stueckzahl.objekt}
          open={showQuickview}
          opener={maschine_stueckzahl.objekt.id}
          onClose={() => setShowQuickview(false)}
        />,
        document.body
      )}
    </div>
  );
};

export default MaschineElement;
