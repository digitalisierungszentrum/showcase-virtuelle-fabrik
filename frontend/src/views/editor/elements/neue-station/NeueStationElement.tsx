import { useDroppable } from "@dnd-kit/core";
import { Icon, Title } from "@ui5/webcomponents-react";
import { ThemingParameters } from "@ui5/webcomponents-react-base";
import { useTranslation } from "react-i18next";

import HighlightCard from "../../../../components/highlight-card/HightlightCard";
import { Station } from "../../../../models/Station";
import { DropData } from "../../DropData";
import { useEditorStore } from "../../store/EditorStateProvider";
import { ElementTypes } from "../ElementTypes";

/**
 * Element über das neue Stationen zur Produktionslinie hinzugefügt werden
 * können. Entwerder durch klicken oder durch draggen eines Elements.
 */
const NeueStationElement = ({ station }: { station: Station }): JSX.Element => {
  const { t } = useTranslation();
  const addStation = useEditorStore((s) => s.addStation);

  const { isOver, setNodeRef } = useDroppable({
    id: "NeueStation",
    data: {
      type: ElementTypes.NeueStation,
      station: station,
    } as DropData,
  });

  return (
    <>
      <HighlightCard
        ref={setNodeRef}
        style={{
          position: "absolute",
          margin: "1em auto",
          width: "25em",
          height: "25em",
          display: "flex",
          flexDirection: "column",
          justifyContent: "stretch",
          alignItems: "stretch",
        }}
        active={isOver}
        activeStyle={{
          borderWidth: "4px",
          borderStyle: "solid",
          borderColor: ThemingParameters.sapPositiveColor,
          borderRadius: ThemingParameters.sapElement_BorderCornerRadius,
        }}
        onClick={() => addStation(station)}
      >
        <div
          style={{
            height: "25em",
            display: "flex",
            padding: "1em",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "stretch",
          }}
        >
          <Title
            level="H3"
            style={{
              ...(isOver
                ? {
                    color: ThemingParameters.sapPositiveColor,
                  }
                : {}),
            }}
          >
            {t("editor.neueStationHeader")}
          </Title>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              flexGrow: "2",
            }}
          >
            <Icon
              name="add"
              style={{
                scale: "1000%",
                ...(isOver
                  ? {
                      color: ThemingParameters.sapPositiveColor,
                    }
                  : {}),
              }}
            ></Icon>
          </div>
        </div>
      </HighlightCard>
    </>
  );
};

export default NeueStationElement;
