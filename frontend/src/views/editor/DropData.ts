import { Station } from "../../models/Station";
import { ElementTypes } from "./elements/ElementTypes";

interface NeueStationData {
  type: ElementTypes.NeueStation;
  station: Station;
}

interface StationData {
  type: ElementTypes.Station;
  station: Station;
}

interface UeChargenData {
  type: ElementTypes.UeChargen;
}

/**
 * Datentyp von Editor DropTargets
 */
export type DropData = NeueStationData | StationData | UeChargenData;
