import {
  DndContext,
  DragEndEvent,
  DragOverlay,
  DragStartEvent,
  MouseSensor,
  pointerWithin,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import { BusyIndicator } from "@ui5/webcomponents-react";
import { ThemingParameters } from "@ui5/webcomponents-react-base";
import { useEffect, useState } from "react";

import FloatingActionButton from "../../components/floating-action-button/FloatingActionButton";
import { DragData } from "./DragData";
import { DropData } from "./DropData";
import { ElementTypes } from "./elements/ElementTypes";
import Dashboard2 from "./elements/OptimizationResult/Dashboard2";
import Area from "./elements/area/Area";
import ChargeElement from "./elements/charge/ChargeElement";
import MaschineElement from "./elements/maschine/MaschineElement";
import EditorNotifications from "./notifications/EditorNotifications";
import Sidebar from "./sidebar/Sidebar";
import {
  EditorStateProvider,
  useEditorStore,
} from "./store/EditorStateProvider";

/**
 * Einstiegspunkt für dei Editor View
 */
const Editor = (): JSX.Element => {
  /**
   * MouseSensor mit activationConstraint. Damit man ein drag element immer
   * noch normal anklicken kann um die Quickview zu öffnen
   */
  const mouseSensor = useSensor(MouseSensor, {
    activationConstraint: {
      distance: 10,
    },
  });

  const { isLoading } = useEditorStore();
  const sensors = useSensors(mouseSensor);

  const moveMaschineToStation = useEditorStore((s) => s.moveMaschineToStation);
  const resetMaschine = useEditorStore((s) => s.resetMaschine);
  const startSimulation = useEditorStore((s) => s.startSimulation);
  const addStation = useEditorStore((s) => s.addStation);
  const initState = useEditorStore((s) => s.initState);
  const destroyState = useEditorStore((s) => s.destroyState);

  /**
   * Data des Elements das momentan gedragged wird.
   */
  const [dragData, setDragData] = useState<DragData | undefined>(undefined);

  // Store initialisieren beim start der Komponente
  useEffect(() => {
    initState();
    return () => {
      destroyState();
    };
  }, [initState, destroyState]);

  /**
   * Bestimmt, ob und was im DragOverlay gerendert werden soll.
   */
  function renderDragOverlay() {
    if (!dragData) {
      return null;
    }
    if (dragData.type === ElementTypes.Maschine) {
      return (
        <MaschineElement
          maschine_stueckzahl={dragData.maschine_stueckzahl}
          isOverlay
          onStueckzahlChange={() => {
            return;
          }}
        />
      );
    }

    if (dragData.type === ElementTypes.Charge) {
      return <ChargeElement charge={dragData.charge} isOverlay />;
    }

    return null;
  }

  function handleDragStart(event: DragStartEvent) {
    setDragData(event.active.data.current as DragData);
  }

  /**
   * Wird aufgerufen wenn ein Drag Vorgang beendet wird.
   * Kümmert sich um die Tatsächliche veränderung des States.
   */
  function handleDragEnd(event: DragEndEvent) {
    setDragData(undefined);

    if (!event.active) {
      return;
    }

    // wenn drag nicht null
    const dragged = event.active.data.current as DragData;

    // Fall: Maschine zu undefined
    if (dragged.type === ElementTypes.Maschine && !event.over) {
      resetMaschine(dragged.maschine_stueckzahl);
      return;
    }

    if (!event.over) {
      return;
    }

    // wenn drop nicht null
    const dropped = event.over.data.current as DropData;

    // Fall: Drag auf neue Station
    if (dropped.type === ElementTypes.NeueStation) {
      addStation(dropped.station);
      return;
    }

    // Fall: Maschine zu Station
    if (
      dragged.type === ElementTypes.Maschine &&
      dropped.type === ElementTypes.Station
    ) {
      moveMaschineToStation(dragged.maschine_stueckzahl, dropped.station);
      return;
    }
  }

  return (
    <>
      <DndContext
        autoScroll
        collisionDetection={pointerWithin}
        onDragStart={handleDragStart}
        onDragEnd={handleDragEnd}
        sensors={sensors}
      >
        <div
          style={{
            position: "relative",
            width: "100%",
            backgroundColor: ThemingParameters.sapNeutralBackground,
          }}
        >
          <div style={{ display: "flex", padding: "16px" }}>
            <Sidebar />
            <Area />
          </div>

          <DragOverlay>{renderDragOverlay()}</DragOverlay>
          <FloatingActionButton
            icon="media-play"
            iconOffset
            onClick={startSimulation}
          />
        </div>
        <EditorNotifications />
      </DndContext>

      <Dashboard2 />

      {isLoading && (
        <div
          id="overlay"
          style={{
            position: "absolute",
            content: "",
            width: "100%",
            opacity: "70%",
            background: "#EBEBEB",
            top: "2.7em",
            left: 0,
            bottom: 0,
            zIndex: "1",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <BusyIndicator active delay={0} />
        </div>
      )}
    </>
  );
};

const connectedEditor = () => (
  <EditorStateProvider>
    <Editor />
  </EditorStateProvider>
);

export default connectedEditor;
