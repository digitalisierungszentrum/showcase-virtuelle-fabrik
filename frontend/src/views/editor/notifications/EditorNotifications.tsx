import {
  Bar,
  List,
  Loader,
  NotificationListItem,
  PopoverPlacementType,
  ResponsivePopover,
  ShellBarItem,
  Title,
  TitleLevel,
  Toast,
} from "@ui5/webcomponents-react";
import { useEffect, useRef, useState } from "react";
import { createPortal } from "react-dom";
import { useTranslation } from "react-i18next";

import {
  useNavbarActionsPortal,
  useNavbarRef,
} from "../../../hooks/useNavbarRef";
import { notificationTypeToPriority } from "../../../util/notificationUtil";
import { useEditorStore } from "../store/EditorStateProvider";

/**
 * Ist dafür verantwortlich Notifications in der Editor view anzuzeigen.
 * Zeigt das Notifications-Popover, sowie Toasts.
 */
const EditorNotifications = (): JSX.Element => {
  const { t } = useTranslation();
  const navbarPortal = useNavbarActionsPortal();
  const navbarRef = useNavbarRef();

  const simulationState = useEditorStore((s) => s.simulationState);
  const notifications = useEditorStore((s) => s.notifications);
  const removeNotification = useEditorStore((s) => s.removeNotification);

  const toast = useRef<any>();
  const popoverRef = useRef<any>(null);

  // Um herauszufinden, wann der Toast angezeigt werden soll, wird auf einen statechange gewartet.
  const [currentState, setCurrentState] =
    useState<typeof simulationState>("idle");
  useEffect(() => {
    if (currentState === simulationState) {
      return;
    }
    if (currentState === "running" && simulationState === "finished") {
      toast.current.show();
    }
    setCurrentState(simulationState);
  }, [simulationState, currentState]);

  useEffect(() => {
    // öffnen des Notifications-Popover, wenn es notifications gibt.
    if (notifications.length) {
      // setTimeout als workaround, weil sonst das Navbar Item nicht rechtzeitig da ist.
      setTimeout(() => {
        // Da ui5 das ShellbarItem kaputt macht in der ShadowDom, ist das der Einzige weg um an das element
        // heranzukommen.
        const target = navbarRef?.current?.shadowRoot?.querySelector(
          'ui5-button[icon="bell"]'
        );

        popoverRef.current!.showAt(target);
      }, 0);
    }
  }, [notifications, navbarRef]);

  const notificationsPopover = (
    <ResponsivePopover
      id={"whatever"}
      ref={popoverRef}
      placementType={PopoverPlacementType.Bottom}
      style={{ width: "30em" }}
      header={
        <Bar
          startContent={
            <Title level={TitleLevel.H4}>
              {t("editor.notifications.popoverTitle")}
            </Title>
          }
        />
      }
    >
      <List>
        {notifications &&
          notifications.map(
            ({
              id,
              title,
              details,
              type,
              showClose,
              showProgressIndicator,
            }) => (
              <NotificationListItem
                key={id}
                titleText={t(title)}
                priority={notificationTypeToPriority(type)}
                showClose={showClose}
                onClose={() => removeNotification(id)}
              >
                {showProgressIndicator && <Loader />}
                {t(details)}
              </NotificationListItem>
            )
          )}
      </List>
    </ResponsivePopover>
  );

  return (
    <>
      {createPortal(
        <Toast ref={toast}>
          {t("editor.notifications.simulationFinished.title")}
        </Toast>,
        document.body
      )}
      {notifications.length > 0 &&
        // Navbar Action für das NotificationsPopover hinzufügen.
        navbarPortal(
          <ShellBarItem
            icon="bell"
            text="add"
            onClick={(e: any) => {
              popoverRef.current!.showAt(e.detail.targetRef as any);
            }}
            count={notifications.length + ""}
          />
        )}
      {notifications.length > 0 &&
        createPortal(notificationsPopover, document.body)}
    </>
  );
};

export default EditorNotifications;
