import { FlexBox } from "@ui5/webcomponents-react";
import { ReactNode } from "react";

interface SidebarHeaderProps {
  /**
   * Titel Element.
   */
  title: ReactNode;
  /**
   * Slot wird von ui5 benötigt.
   */
  slot?: any;
}

/**
 * Header für für das Editor SidebarElement.
 */
const SidebarHeader = ({ slot, title }: SidebarHeaderProps): JSX.Element => {
  return (
    <FlexBox
      slot={slot}
      direction="Row"
      justifyContent="Center"
      alignItems="Center"
      style={{
        padding: "0.3em 0.3em",
        fontWeight: "bold",
        fontSize: "1.8em",
        height: "1.5em",
      }}
    >
      {title}
    </FlexBox>
  );
};

export default SidebarHeader;
