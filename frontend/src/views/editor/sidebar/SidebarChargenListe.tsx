import { FlexBox, Icon, Input, Label, List } from "@ui5/webcomponents-react";
import "@ui5/webcomponents/dist/features/InputSuggestions.js";
import { useMemo, useState } from "react";

import { useChargen } from "../../../hooks/useChargen";
import { Charge } from "../../../models/Charge";
import ChargeElement from "../elements/charge/ChargeElement";
import { useEditorStore } from "../store/EditorStateProvider";

const SidebarChargenListe = (): JSX.Element => {
  const chargenQuery = useChargen();
  const produktionslinie = useEditorStore((s) => s.produktionslinie);
  const editorIsLoading = useEditorStore((s) => s.isLoading);
  const editorIsError = useEditorStore((s) => s.isError);

  const isLoading = useMemo(
    () => chargenQuery.isLoading || editorIsLoading,
    [chargenQuery, editorIsLoading]
  );

  const isError = useMemo(
    () => chargenQuery.isError || editorIsError,
    [chargenQuery, editorIsError]
  );

  const [filterStr, setFilterStr] = useState("");

  function filter(m: Charge): boolean {
    return (
      filterStr === "" ||
      Object.values(m).find((v) =>
        String(v).toLowerCase().includes(filterStr.toLowerCase())
      )
    );
  }

  // Mnemonic das für die Sidebar relevante Chargen filtert
  const filteredChargen: Charge[] | undefined = useMemo(() => {
    if (!chargenQuery.data) {
      return;
    }
    const usedChargen = produktionslinie.stationen
      .flatMap((s) => s.chargen)
      .concat(produktionslinie.chargen)
      .map((m) => m.id);

    return chargenQuery.data.filter((m) => !usedChargen.includes(m.id));
  }, [chargenQuery, produktionslinie]);

  if (isLoading) {
    return <></>;
  }

  if (isError) {
    return <div>todo</div>;
  }

  return (
    <FlexBox
      style={{
        width: "100%",
        flexDirection: "column",
        alignItems: "strech",
        justifyContent: "center",
        margin: "0px 0px 5px 0px",
      }}
    >
      <Label
        style={{
          textAlign: "start",
          margin: "8px 0 5px 15px",
          padding: "0",
          fontWeight: "bold",
          fontSize: "1.2em",
          height: "1.5em",
        }}
      >
        Chargen
      </Label>
      <Input
        icon={<Icon name="search" />}
        onInput={(e) => setFilterStr(e.target.value ?? "")}
        showSuggestions
        placeholder="Search"
        style={{
          width: "100%",
          border: "none",
        }}
      ></Input>
      <FlexBox
        style={{
          height: "19em",
        }}
      >
        <List>
          {(filteredChargen && filteredChargen.filter(filter))?.map(
            (charge) => (
              <ChargeElement key={charge.id} charge={charge} />
            )
          )}
        </List>
      </FlexBox>
    </FlexBox>
  );
};

export default SidebarChargenListe;
