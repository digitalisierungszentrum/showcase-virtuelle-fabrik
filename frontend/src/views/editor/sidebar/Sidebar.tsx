import { Card } from "@ui5/webcomponents-react";

import SidebarHeader from "./SidebarHeader";
import SidebarMaschinenListe from "./SidebarMaschinenListe";

const Sidebar = (): JSX.Element => {
  return (
    <Card
      style={{
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "start",
        //position: "absolute",
        left: "1em",
        top: "1em",
        width: "27em",
        height: "40em",
        maxHeight: "calc(100% - 2em)",
      }}
      header={<SidebarHeader title="Workflow" />}
    >
      <SidebarMaschinenListe />

      {/* <Toolbar
        style={{
          width: "100%",
          height: "1px",
        }}
      ></Toolbar>

      <SidebarChargenListe /> */}
    </Card>
  );
};

export default Sidebar;
