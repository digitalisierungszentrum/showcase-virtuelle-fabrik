import { FlexBox, Icon, Input, Label, List } from "@ui5/webcomponents-react";
import "@ui5/webcomponents/dist/features/InputSuggestions.js";
import { useMemo, useState } from "react";

import { useAllMaschinen } from "../../../hooks/useMaschinen";
import { Maschine } from "../../../models/Maschine";
import MaschineElement from "../elements/maschine/MaschineElement";
import { useEditorStore } from "../store/EditorStateProvider";
import { Stueckzahl } from "../../../models/Stueckzahl";
import { Direction } from "@dnd-kit/core/dist/types";

const SidebarMaschinenListe = (): JSX.Element => {
  const maschinenQuery = useAllMaschinen();
  const produktionslinie = useEditorStore((s) => s.produktionslinie);
  const editorIsLoading = useEditorStore((s) => s.isLoading);
  const editorIsError = useEditorStore((s) => s.isError);

  const isLoading = useMemo(
    () => maschinenQuery.isLoading || editorIsLoading,
    [maschinenQuery, editorIsLoading]
  );

  const isError = useMemo(
    () => maschinenQuery.isError || editorIsError,
    [maschinenQuery, editorIsError]
  );

  const [filterStr, setFilterStr] = useState("");

  function filter(ms: Stueckzahl<Maschine>): boolean {
    return (
      filterStr === "" ||
      Object.values(ms).map((x) => {return x.objekt}).find((v) =>
        String(v).toLowerCase().includes(filterStr.toLowerCase())
      )
    );
  }

  // Mnemonic das für die Sidebar relevante Maschinen filtert
  const filteredMaschinen: Stueckzahl<Maschine>[] | undefined = useMemo(() => {
    if (!maschinenQuery.data) {
      return;
    }
    const usedMaschinen = produktionslinie.stationen
      .flatMap((s) => s.maschinen_stueckzahl)
      .map((m) => m.objekt.id);

    return maschinenQuery.data.filter((m) => !usedMaschinen.includes(m.id)).map((x) => ({stueckzahl: 1, objekt: x}));
  }, [maschinenQuery, produktionslinie]);

  if (isLoading) {
    return <></>;
  }

  if (isError) {
    return <div>todo</div>;
  }

  return (
    <FlexBox
      style={{
        width: "100%",
        flexDirection: "column",
        alignItems: "strech",
        justifyContent: "center",
        margin: "0px 0px 5px 0px",
      }}
    >
      <Label
        style={{
          textAlign: "start",
          margin: "8px 0 5px 15px",
          padding: "0",
          fontWeight: "bold",
          fontSize: "1.2em",
          height: "1.5em",
        }}
      >
        Maschinen
      </Label>
      <Input
        icon={<Icon name="search" />}
        onInput={(e) => setFilterStr(e.target.value ?? "")}
        showSuggestions
        placeholder="Search"
        style={{
          width: "100%",
          border: "none",
        }}
      ></Input>

      <FlexBox
        style={{
          height: "35em",
        }}
      >
        <List>
          {(filteredMaschinen && filteredMaschinen.filter(filter))?.map(
            (ms) => (
                <MaschineElement key={ms.objekt.id} maschine_stueckzahl={ms} onStueckzahlChange={() => {}}/>
            )
          )}
        </List>
      </FlexBox>
    </FlexBox>
  );
};

export default SidebarMaschinenListe;
