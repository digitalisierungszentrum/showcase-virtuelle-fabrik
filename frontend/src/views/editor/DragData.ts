import { Charge } from "../../models/Charge";
import { Maschine } from "../../models/Maschine";
import { Stueckzahl } from "../../models/Stueckzahl";
import { ElementTypes } from "./elements/ElementTypes";

interface ChargeData {
  type: ElementTypes.Charge;
  charge: Charge;
}

interface MaschineData {
  type: ElementTypes.Maschine;
  maschine_stueckzahl: Stueckzahl<Maschine>;
}

/**
 * Datentyp von Editor DragEvents
 */
export type DragData = ChargeData | MaschineData;
