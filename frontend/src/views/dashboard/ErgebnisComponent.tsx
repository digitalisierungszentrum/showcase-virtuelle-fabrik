import { Card, FlexBox, Title } from "@ui5/webcomponents-react";
import { ThemingParameters } from "@ui5/webcomponents-react-base";

import { useArbeitsschritte } from "../../hooks/useArbeitsschritte";
import { useAllMaschinen } from "../../hooks/useMaschinen";
import { LeistungsErgebnis } from "../../openapi";
import { combineQueries } from "../../util/queryUtil";

interface ErgebnisComponentProps {
  ergebnis: LeistungsErgebnis;
  title: string;
  good?: boolean;
}

const ErgebnisComponent = ({
  ergebnis,
  title,
  good,
}: ErgebnisComponentProps): JSX.Element => {
  const maschinenQuery = useAllMaschinen();
  const arbeitsschritteQuery = useArbeitsschritte();
  const { data } = combineQueries(
    maschinenQuery,
    arbeitsschritteQuery,
    (m, a) => {return {maschinen: m ?? [], arbeitsschritte: a ?? []}}
  );

  if (!data) {
    return <></>;
  }

  const {maschinen, arbeitsschritte} = data;

  return (
    <div style={{ marginRight: "3em" }}>
      <Card>
        <FlexBox
          justifyContent="Start"
          alignItems="Baseline"
          style={{ padding: "1em"}}
        >
          <Title level="H2" style={{ marginRight: "2em" }}>
            {title}
          </Title>
          <div>
            Kosten/Stk:{" "}
            <span
              style={{
                fontWeight: "600",
                color: good
                  ? ThemingParameters.sapPositiveColor
                  : ThemingParameters.sapWarningColor,
                marginRight: '1em'
              }}
            >
              {ergebnis.kostenProdukt.toFixed(2)}€
            </span>
            Stk/Minute:{" "}
            <span
              style={{
                fontWeight: "600",marginRight: '1em'
              }}
            >
              {ergebnis.produktionsRate.toFixed(2)}</span>
              Anschaffungskosten:{" "}
            <span
              style={{
                fontWeight: "600",
              }}
            >
              {ergebnis.anschaffungskosten.toFixed(2)}€
            </span>
          </div>
        </FlexBox>

        <FlexBox wrap="Wrap" justifyContent="SpaceAround" style={{ gap: "1em", padding: "1em" }}>
          {ergebnis.maschinenauslastung?.map((a, i) => {
            const maschine = maschinen.find(({ id }) => id === a.maschinenStueckzahl.maschine);
            const arbeitsschritt = arbeitsschritte.find(
              ({ id }) => id === a.arbeitsschritt
            );
            return (
              <Card key={i}>
                <FlexBox
                  direction="Column"
                  justifyContent="SpaceBetween"
                  style={{
                    height: "5em",
                    padding: "1em",
                    color:
                      a.auslastung === 0
                        ? ThemingParameters.sapContent_DisabledTextColor
                        : "",
                  }}
                >
                  <Title
                    level="H4"
                    wrappingType="Normal"
                    style={{
                      color:
                        a.auslastung === 0
                          ? ThemingParameters.sapContent_DisabledTextColor
                          : "",
                    }}
                  >
                    {`${a.maschinenStueckzahl.stueckzahl}x ${maschine?.name}`}
                  </Title>
                  {arbeitsschritt?.name}

                  <div>Auslastung: {Math.round(a.auslastung * 100)}%</div>
                </FlexBox>
              </Card>
            );
          })}
        </FlexBox>
      </Card>
    </div>
  );
};

export default ErgebnisComponent;
