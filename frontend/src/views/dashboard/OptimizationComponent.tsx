import { FlexBox, ResponsiveGridLayout } from "@ui5/webcomponents-react";

import { useCharge } from "../../hooks/useChargen";
import { useProdukt } from "../../hooks/useProdukte";
import { OptimierungsErgebnis } from "../../openapi";
import ErgebnisComponent from "./ErgebnisComponent";
import {ThemingParameters} from "@ui5/webcomponents-react-base";

interface OptimierungComponentProps {
  optimierungsergebnis: OptimierungsErgebnis;
}

const OptimierungComponent = ({
  optimierungsergebnis,
}: OptimierungComponentProps) => {
  const chargeQuery = useCharge(optimierungsergebnis.charge);
  const produktQuery = useProdukt(chargeQuery.data?.produktbedarf[0].produktId);

  if (chargeQuery.isLoading || produktQuery.isLoading) {
    return <></>;
  }

  const isOptimal =
    optimierungsergebnis.gegeben.kostenProdukt <=
    optimierungsergebnis.optimiert.kostenProdukt;

  return (
    <>
      <FlexBox justifyContent="SpaceBetween">
        <div style={{ marginBottom: "2em" }}>
          <div>
            Charge:{" "}
            <span style={{ fontWeight: "600" }}>{chargeQuery.data?.name}</span>
          </div>
          <div>
            <span style={{ marginRight: "1em" }}>
              {chargeQuery.data?.produktbedarf[0].stueckzahl} Stk/Minute
            </span>
            <span>{produktQuery.data?.name}</span>
          </div>
          <div>
            {isOptimal ? (
          <span style={{color: ThemingParameters.sapWarningColor}}>
            Die erstellte Produktionslinie kann nicht mehr weiter optimiert
            werden!
          </span>
        ) : (
          <span style={{color: ThemingParameters.sapPositiveColor}}>
            Es wurde eine optimierte Konfiguration gefunden!
          </span>
        )}
          </div>
        </div>
        <div />
      </FlexBox>

      <ResponsiveGridLayout columnsXL={2} columnsL={2} columnsM={2} columnsS={1}>
        <div>
          <ErgebnisComponent
            title="Erstellt"
            ergebnis={optimierungsergebnis.gegeben}
            good={isOptimal}
          />
        </div>
        <div>
          <ErgebnisComponent
            title="Optimiert"
            ergebnis={optimierungsergebnis.optimiert}
            good
          />
        </div>
      </ResponsiveGridLayout>
    </>
  );
};

export default OptimierungComponent;
