import {
  DynamicPageTitle,
  ObjectPage,
  ObjectPageSection,
  Option,
  Select,
  Title,
} from "@ui5/webcomponents-react";
import { ThemingParameters } from "@ui5/webcomponents-react-base";
import { format } from "date-fns";
import { ReactElement, useEffect, useMemo } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { useOptimierungen } from "../../hooks/useOptimierungen";
import {
  DashboardRoutes,
  SzenarioRoutes,
  buildRoute,
  getRoute,
} from "../../routes/appRoutes";
import OptimierungComponent from "./OptimizationComponent";

const Dashboard = (): JSX.Element => {
  const navigate = useNavigate();
  const { simulationId } = useParams();

  const { data, isLoading } = useOptimierungen();

  const optimierungen = useMemo(() => {
    if (!data) {
      return undefined;
    }

    return data.sort(
      (a, b) =>
        new Date(b.ausfuehrung).getTime() - new Date(a.ausfuehrung).getTime()
    );
  }, [data]);

  const optimierung = useMemo(() => {
    if (!simulationId || !optimierungen) {
      return undefined;
    }

    return optimierungen.find((s) => s.id === simulationId);
  }, [simulationId, optimierungen]);

  useEffect(() => {
    if (simulationId || !optimierungen || optimierungen.length < 1) {
      return;
    }

    const route = buildRoute(
      "../",
      SzenarioRoutes.Dashboard,
      getRoute(DashboardRoutes.Simulation, {
        simulationId: optimierungen[0].id,
      })
    );

    navigate(route);
  }, [navigate, simulationId, optimierungen]);

  if (isLoading) {
    return (
      <ObjectPage
        headerTitle={<DynamicPageTitle header={""}></DynamicPageTitle>}
      ></ObjectPage>
    );
  }

  return (
    <ObjectPage
      headerTitle={
        <DynamicPageTitle
          header={
            <Select
              onChange={(e) => {
                const simulationId = e.detail.selectedOption.dataset.id;
                const route = buildRoute(
                  "../../",
                  SzenarioRoutes.Dashboard,
                  getRoute(DashboardRoutes.Simulation, { simulationId })
                );

                navigate(route);
              }}
              style={{
                border: "none",
                minWidth: "fit-content",
                color: "green",
                fontSize: "2em",
              }}
            >
              {optimierungen?.map(({ id, ausfuehrung }) => (
                <Option key={id} data-id={id} selected={id === simulationId}>
                  Simulation {id} (
                  {format(new Date(ausfuehrung), "dd.MM.yy HH:mm")})
                </Option>
              ))}
            </Select>
          }
        ></DynamicPageTitle>
      }
    >
      {optimierung &&
        optimierung.stationen
          .map((station, i) => {
            const isOptimal =
              station.gegeben.kostenProdukt <= station.optimiert.kostenProdukt;

            return (
              <ObjectPageSection
                key={station.station}
                id={station.station}
                titleText=""
              >
                <Title
                  style={{
                    ...(i === 0
                      ? {}
                      : {
                          marginTop: "-1em",
                        }),
                    color: isOptimal
                      ? ThemingParameters.sapPositiveColor
                      : ThemingParameters.sapWarningColor,
                    marginBottom: "1em",
                  }}
                >
                  {station.name}
                </Title>
                <OptimierungComponent optimierungsergebnis={station} />
              </ObjectPageSection>
            );
          })
          .filter((e): e is ReactElement => !!e)}
    </ObjectPage>
  );
};

export default Dashboard;
