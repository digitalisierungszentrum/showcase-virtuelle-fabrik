import { FC } from "react";

import { useAddArbeitsschritt } from "../../../hooks/useArbeitsschritte";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import { StammdatenRoutes, getRoute } from "../../../routes/appRoutes";
import ArbeitsschrittForm from "./ArbeitsschrittForm";

/**
 * Ansicht zum Anlegen eines neuen Arbeitsschritts.
 */
export const ArbeitsschrittNew: FC = () => {
  const navigate = useNavigateWithParams();

  const add = useAddArbeitsschritt();

  const initialValues: Arbeitsschritt = {
    id: "",
    name: "",
  };

  function onSubmit(values: Arbeitsschritt) {
    add.mutateAsync(values).then(({ id }) =>
      navigate(
        getRoute("../" + StammdatenRoutes.ArbeitsschritteDetails, {
          arbeitsschrittId: id,
        })
      )
    );
  }

  return (
    <ArbeitsschrittForm
      initialValues={initialValues}
      onSubmit={onSubmit}
      onCancel={() => navigate("..")}
    />
  );
};
