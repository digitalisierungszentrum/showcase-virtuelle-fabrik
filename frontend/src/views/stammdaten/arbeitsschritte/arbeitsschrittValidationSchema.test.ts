import { ValidationError } from "yup";

import {
  testArbeitsschritt1,
  testArbeitsschritt2,
  testArbeitsschritt3,
} from "../../../util/testData";
import { arbeitsschrittValidationSchema } from "./arbeitsschrittValidationSchema";

describe("Arbeitsschritt Validation tests", () => {
  const validationSchema = arbeitsschrittValidationSchema(() => {});

  test("Erkennt korrekte Objekte", async () => {
    expect(await validationSchema.isValid(testArbeitsschritt1)).toBeTruthy();
    expect(await validationSchema.isValid(testArbeitsschritt2)).toBeTruthy();
    expect(await validationSchema.isValid(testArbeitsschritt3)).toBeTruthy();
  });

  test("Erkennt fehlende Attribute", async () => {
    expect(() =>
      validationSchema.validateSync({
        id: "test",
      })
    ).toThrow(ValidationError);
  });

  test("Edgecases Number", async () => {
    expect(
      await validationSchema.isValid(
        {
          ...testArbeitsschritt2,
          name: "schleifen",
        },
        {}
      )
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testArbeitsschritt2,
        name: "",
      })
    ).toBeFalsy();
  });

  test("erkennt undefined", async () => {
    expect(
      await validationSchema.isValid({
        ...testArbeitsschritt3,
        id: undefined,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testArbeitsschritt3,
        name: undefined,
      })
    ).toBeFalsy();
  });

  test("erkennt ob ID falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testArbeitsschritt2,
        id: "",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testArbeitsschritt2,
        id: 455,
      })
    ).toBeTruthy();
  });

  test("erkennt ob Feld Name falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testArbeitsschritt2,
        name: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testArbeitsschritt2,
        name: "löten",
      })
    ).toBeTruthy();
  });
});

export {};
