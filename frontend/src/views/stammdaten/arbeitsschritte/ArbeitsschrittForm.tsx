import {
  BreadcrumbsItem,
  Input,
  InputType,
  Loader,
  ObjectPageSection,
  Title,
} from "@ui5/webcomponents-react";
import { Formik, FormikHelpers } from "formik";
import { FC } from "react";
import { useTranslation } from "react-i18next";

import FormBar from "../../../components/form-bar/FormBar";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import { useHandleFieldState } from "../../../util/errorUtil";
import { useArbeitsschrittBalidationSchema as useArbeitsschrittValidationSchema } from "./arbeitsschrittValidationSchema";

interface ArbeitsschrittFormProps {
  /**
   * Initiale Werte für das Form.
   */
  initialValues: Arbeitsschritt;
  /**
   * Funktion wenn das Form Submitted wird.
   */
  onSubmit: (
    values: Arbeitsschritt,
    helpers: FormikHelpers<Arbeitsschritt>
  ) => void;
  /**
   * Funktion des Cancel-Buttons.
   */
  onCancel: () => void;
}

/**
 * Gemeinsame Basis für die {@link ArbeitsschrittEdit} und {@link ArbeitsschrittNew} Views.
 */
const ArbeitsschrittForm: FC<ArbeitsschrittFormProps> = ({
  initialValues,
  onSubmit,
  onCancel,
}) => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();

  const arbeitsschrittValidationSchema = useArbeitsschrittValidationSchema();

  // Values müssen noch geparsed werden
  function sanitizeSubmit(
    values: Arbeitsschritt,
    helpers: FormikHelpers<Arbeitsschritt>
  ) {
    const parsed = arbeitsschrittValidationSchema.cast(values, {
      stripUnknown: true,
    });

    return onSubmit(parsed as Arbeitsschritt, helpers);
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={sanitizeSubmit}
      validationSchema={arbeitsschrittValidationSchema}
      validateOnChange={false}
      validateOnBlur={false}
    >
      {({ values, handleChange, handleSubmit, errors, isSubmitting }) => {
        return (
          <StammdatenDetailsComponent
            header={
              <Title>
                <Input
                  name="name"
                  aria-label={t("stammdaten.arbeitsschritte.name")}
                  role="textbox"
                  placeholder={t("stammdaten.arbeitsschritte.name")}
                  type={InputType.Text}
                  onInput={handleChange}
                  value={values.name}
                  {...handleFieldState(errors.name)}
                />
              </Title>
            }
            breadcrumbs={<BreadcrumbsItem>Arbeitsschritte</BreadcrumbsItem>}
            footer={
              <>
                {isSubmitting && <Loader />}
                <FormBar
                  submitEnabled={!isSubmitting}
                  errors={errors}
                  errorPrefix="stammdaten.arbeitsschritte"
                  onSubmit={handleSubmit}
                  onCancel={onCancel}
                />
              </>
            }
          >
            <ObjectPageSection id={"placeholder"}>
              <></>
            </ObjectPageSection>
            <ObjectPageSection id={""}>
              <></>
            </ObjectPageSection>
          </StammdatenDetailsComponent>
        );
      }}
    </Formik>
  );
};

export default ArbeitsschrittForm;
