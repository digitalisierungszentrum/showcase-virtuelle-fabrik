import {
  Button,
  ButtonDesign,
  Icon,
  Input,
  Label,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  TableRowType,
  Title,
  Toolbar,
  ToolbarSpacer,
} from "@ui5/webcomponents-react";
import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";

/**
 * Liste aller Arbeitsschritte in der Stammdatenübersicht.
 */
const ArbeitsschritteListe: FC = () => {
  const { arbeitsschrittId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();

  const {
    data: arbeitsschritte,
    isLoading,
    isError,
    isRefetching,
  } = useArbeitsschritte();
  const [filterStr, setFilterStr] = useState("");

  function filter(m: Arbeitsschritt): boolean {
    return (
      filterStr === "" ||
      Object.values(m).find((v) =>
        String(v).toLowerCase().includes(filterStr.toLowerCase())
      )
    );
  }

  return (
    <>
      <Toolbar>
        <Title>{t("stammdaten.arbeitsschritte.title")}</Title>
        <ToolbarSpacer />
        <Input
          icon={<Icon name="search" />}
          onInput={(e) => setFilterStr(e.target.value ?? "")}
        />
        <Button
          role="button"
          aria-label={t("stammdaten.arbeitsschritte.newBtn")}
          design={ButtonDesign.Transparent}
          icon="add"
          onClick={() => navigate(StammdatenRoutes.ArbeitsschritteNew)}
        ></Button>
      </Toolbar>
      <Table
        busy={isRefetching || isLoading}
        busyDelay={0}
        noDataText={
          isLoading
            ? ""
            : isError
            ? t("stammdaten.tableUnexpectedError")
            : t("stammdaten.tableEmpty")
        }
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.arbeitsschritte.id")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.arbeitsschritte.name")}</Label>
            </TableColumn>
            <TableColumn></TableColumn>
          </>
        }
      >
        {arbeitsschritte &&
          arbeitsschritte.filter(filter).map((arbeitsschritt) => (
            <TableRow
              key={arbeitsschritt.id}
              type={TableRowType.Active}
              onClick={() =>
                navigate(
                  getRoute(StammdatenRoutes.ArbeitsschritteDetails, {
                    arbeitsschrittId: arbeitsschritt.id,
                  })
                )
              }
              selected={arbeitsschrittId === arbeitsschritt.id}
            >
              <TableCell>{arbeitsschritt.id}</TableCell>
              <TableCell>{arbeitsschritt.name}</TableCell>
              <TableCell>
                <Icon name="feeder-arrow" />
              </TableCell>
            </TableRow>
          ))}
      </Table>
    </>
  );
};

export default ArbeitsschritteListe;
