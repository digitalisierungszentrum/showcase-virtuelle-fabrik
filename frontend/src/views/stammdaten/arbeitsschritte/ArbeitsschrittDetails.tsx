import {
  BreadcrumbsItem,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  Label,
  Loader,
  ObjectPageSection,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { FC } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import {
  useArbeitsschritt,
  useDeleteArbeitsschritt,
} from "../../../hooks/useArbeitsschritte";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { AppParams } from "../../../routes/appRoutes";

/**
 * Detailansicht eines Arbeitsschritts.
 * Der anzuzeigende Arbeitsschritt wird den Route-Params entnommen.
 */
const ArbeitsschrittDetails: FC = () => {
  const navigate = useNavigateWithParams();
  const { arbeitsschrittId } = useParams<AppParams>();
  const { t } = useTranslation();

  const {
    data: arbeitsschritt,
    isLoading,
    isError,
    error,
  } = useArbeitsschritt(arbeitsschrittId!);

  const deleteMutation = useDeleteArbeitsschritt();

  async function onDelete() {
    await deleteMutation.mutateAsync({ id: arbeitsschritt!.id });

    navigate("..");
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!arbeitsschritt || isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <StammdatenDetailsComponent
      breadcrumbs={
        <BreadcrumbsItem>
          {t("stammdaten.arbeitsschritte.title")}
        </BreadcrumbsItem>
      }
      header={<Title wrappingType="Normal">{arbeitsschritt.name}</Title>}
      headerContent={
        <FlexBox
          direction={FlexBoxDirection.Column}
          alignItems={FlexBoxAlignItems.Start}
          justifyContent={FlexBoxJustifyContent.Center}
          style={{ marginLeft: "1em" }}
        >
          <Label>{t("stammdaten.arbeitsschritte.id")}</Label>
          <Text>{arbeitsschritt.id}</Text>
        </FlexBox>
      }
      withActions
      onEdit={() => navigate("edit")}
      onDelete={onDelete}
      deleteConfirmationTitle={t(
        "stammdaten.arbeitsschritte.deleteConfirmationTitle"
      )}
      deleteConfirmationBody={t(
        "stammdaten.arbeitsschritte.deleteConfirmationBody",
        {
          name: arbeitsschritt.name,
        }
      )}
      footer={
        <div style={{ margin: "0 -0.5em" }}>
          {deleteMutation.isLoading && <Loader />}
        </div>
      }
    >
      <ObjectPageSection
        id="general"
        titleText={t("stammdaten.arbeitsschritte.sectionGeneral")}
        titleTextUppercase={false}
      >
        <></>
      </ObjectPageSection>
    </StammdatenDetailsComponent>
  );
};

export default ArbeitsschrittDetails;
