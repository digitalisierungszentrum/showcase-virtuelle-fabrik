import { FC } from "react";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import {
  useArbeitsschritt,
  useEditArbeitsschritt,
} from "../../../hooks/useArbeitsschritte";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";
import ArbeitsschrittForm from "./ArbeitsschrittForm";

/**
 * Editieransicht für einen bestehenden Arbeitsschritt.
 * Der zu editierende Arbeitsschritt wird den Route-Params entnommen.
 */
export const ArbeitsschrittEdit: FC = () => {
  const { arbeitsschrittId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();

  const {
    data: arbeitsschritt,
    isLoading,
    isError,
    error,
  } = useArbeitsschritt(arbeitsschrittId!);
  const edit = useEditArbeitsschritt();

  function onSubmit(values: Arbeitsschritt) {
    edit.mutateAsync(values).then(() =>
      navigate(
        getRoute("../" + StammdatenRoutes.ArbeitsschritteDetails, {
          arbeitsschrittId: arbeitsschrittId,
        })
      )
    );
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!arbeitsschritt || isError) {
    return <UnexpectedErrorPage />;
  }
  return (
    <ArbeitsschrittForm
      initialValues={arbeitsschritt!}
      onSubmit={onSubmit}
      onCancel={() =>
        navigate(
          "../" +
            getRoute(StammdatenRoutes.ArbeitsschritteDetails, {
              arbeitsschrittId,
            })
        )
      }
    />
  );
};
