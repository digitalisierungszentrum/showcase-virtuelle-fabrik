import { TFunction } from "i18next";
import { useTranslation } from "react-i18next";
import { ObjectSchema, object, string } from "yup";

import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import { NewArbeitsschritt } from "../../../models/NewArbeitsschritt";

/**
 * ValidationSchema für das {@link ArbeitsschritteForm}.
 */
export const arbeitsschrittValidationSchema: (
  t: TFunction
) => ObjectSchema<NewArbeitsschritt | Arbeitsschritt> = (t) =>
  object({
    id: string().transform((value) => (value === "" ? undefined : value)),

    name: string().required(t("stammdaten.form.validation.required")),
  });

/**
 * Stellt das ValidationSchema für Arbeitsschritte zur verfügung.
 *
 * @returns ArbeitsschrittValidationSchema.
 */
export const useArbeitsschrittBalidationSchema = () => {
  const { t } = useTranslation();

  return arbeitsschrittValidationSchema(t);
};
