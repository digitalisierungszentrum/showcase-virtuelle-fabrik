import {
  BreadcrumbsItem,
  Form,
  FormItem,
  Input,
  InputType,
  Label,
  Loader,
  ObjectPageSection,
  Title,
} from "@ui5/webcomponents-react";
import { Formik, FormikHelpers } from "formik";
import { FC, ReactElement } from "react";
import { useTranslation } from "react-i18next";

import FormBar from "../../../components/form-bar/FormBar";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import { Produkt } from "../../../models/Produkt";
import { useHandleFieldState } from "../../../util/errorUtil";
import MaterialbedarfField from "./MaterialbedarfField";
import ProduktionsschrittField from "./ProduktionsschrittField";
import { useProduktValidationSchema } from "./produktValidationSchema";

interface ProduktFormProps {
  /**
   * Initiale Werte für das Form.
   */
  initialValues: Produkt;
  /**
   * Funktion wenn das Form Submitted wird.
   */
  onSubmit: (values: Produkt, helpers: FormikHelpers<Produkt>) => void;
  /**
   * Funktion des Cencel-Buttons.
   */
  onCancel: () => void;
}

/**
 * Gemeinsame Basis für die {@link ProduktEdit} und {@link ProduktNew} Views.
 */
const ProduktForm: FC<ProduktFormProps> = ({
  initialValues,
  onSubmit,
  onCancel,
}) => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();

  const { data: produktValidationSchema } = useProduktValidationSchema();

  // Values m�ssen noch geparsed werden
  function sanitizeSubmit(values: Produkt, helpers: FormikHelpers<Produkt>) {
    const parsed = produktValidationSchema!.cast(values, {
      stripUnknown: true,
    });

    return onSubmit(parsed as Produkt, helpers);
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={sanitizeSubmit}
      validationSchema={produktValidationSchema}
      validateOnChange={false}
      validateOnBlur={false}
    >
      {({ values, handleChange, handleSubmit, errors, isSubmitting }) => {
        // helper function für Textinputs. Kann leider nicht in eigene
        // Component ausgelagert werden, weil das ui5 verwirrt.
        const formItem = (
          name: keyof Produkt,
          type: InputType,
          icon?: ReactElement
        ) => (
          <FormItem
            label={
              <Label id={`${name}Label`}>
                {t(`stammdaten.produkte.${name}`)}
              </Label>
            }
          >
            <Input
              name={name}
              aria-labelledby={`${name}Label`}
              role="textbox"
              type={type}
              icon={icon}
              onInput={handleChange}
              value={values[name] + ""}
              {...handleFieldState(errors[name] as string)}
            />
          </FormItem>
        );

        return (
          <StammdatenDetailsComponent
            header={
              <Title>
                <Input
                  name="name"
                  aria-label={t("stammdaten.produkte.name")}
                  role="textbox"
                  placeholder={t("stammdaten.produkte.name")}
                  type={InputType.Text}
                  onInput={handleChange}
                  value={values.name}
                  {...handleFieldState(errors.name)}
                />
              </Title>
            }
            breadcrumbs={<BreadcrumbsItem>Produkte</BreadcrumbsItem>}
            footer={
              <>
                {isSubmitting && <Loader />}
                <FormBar
                  submitEnabled={!isSubmitting}
                  errors={errors}
                  errorPrefix="stammdaten.produkte"
                  onSubmit={handleSubmit}
                  onCancel={onCancel}
                />
              </>
            }
          >
            <ObjectPageSection
              id="general"
              titleText={t("stammdaten.produkte.sectionGeneral")}
              titleTextUppercase={false}
            >
              <Form
                onSubmit={() => handleSubmit()}
                columnsL={3}
                columnsXL={3}
                columnsM={3}
                columnsS={2}
                labelSpanXL={12}
                labelSpanL={12}
                labelSpanM={12}
              >
                {formItem(
                  "verkaufspreis",
                  InputType.Number,
                  <span>&euro;</span>
                )}
              </Form>
            </ObjectPageSection>
            <ObjectPageSection
              id="befaehigungen"
              titleText={t("stammdaten.produkte.sectionBefaehigungen")}
              titleTextUppercase={false}
            >
              <ProduktionsschrittField />
            </ObjectPageSection>

            <ObjectPageSection
              id="materialbefaehigungen"
              titleText={t("stammdaten.produkte.sectionMaterialBedarf")}
              titleTextUppercase={false}
            >
              <MaterialbedarfField />
            </ObjectPageSection>
          </StammdatenDetailsComponent>
        );
      }}
    </Formik>
  );
};

export default ProduktForm;
