import {
  Button,
  ButtonDesign,
  Icon,
  Input,
  Label,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  TableRowType,
  Title,
  Toolbar,
  ToolbarSpacer,
} from "@ui5/webcomponents-react";
import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { useProdukte } from "../../../hooks/useProdukte";
import { Produkt } from "../../../models/Produkt";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";

/**
 * Liste aller Maschinen in der Stammdatenübersicht.
 */
const ProdukteListe: FC = () => {
  const { produktId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();

  const { data: produkte, isLoading, isError, isRefetching } = useProdukte();

  const [filterStr, setFilterStr] = useState("");

  function filter(m: Produkt): boolean {
    return (
      filterStr === "" ||
      Object.values(m).find((v) =>
        String(v).toLowerCase().includes(filterStr.toLowerCase())
      )
    );
  }

  return (
    <>
      <Toolbar>
        <Title>{t("stammdaten.produkte.title")}</Title>
        <ToolbarSpacer />
        <Input
          icon={<Icon name="search" />}
          onInput={(e) => setFilterStr(e.target.value ?? "")}
        />
        <Button
          role="button"
          aria-label={t("stammdaten.produkte.newBtn")}
          design={ButtonDesign.Transparent}
          icon="add"
          onClick={() => navigate(StammdatenRoutes.ProduktNew)}
        ></Button>
      </Toolbar>
      <Table
        busy={isRefetching || isLoading}
        busyDelay={0}
        noDataText={
          isLoading
            ? ""
            : isError
            ? t("stammdaten.tableUnexpectedError")
            : t("stammdaten.tableEmpty")
        }
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.produkte.id")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.produkte.name")}</Label>
            </TableColumn>
            <TableColumn>
              <Label style={{ display: "block", textAlign: "right" }}>
                {t("stammdaten.produkte.verkaufspreisWaehrung")}
              </Label>
            </TableColumn>

            <TableColumn></TableColumn>
          </>
        }
      >
        {produkte &&
          produkte.filter(filter).map((produkt) => (
            <TableRow
              key={produkt.id}
              type={TableRowType.Active}
              onClick={() =>
                navigate(
                  getRoute(StammdatenRoutes.ProduktDetails, {
                    produktId: produkt.id,
                  })
                )
              }
              selected={produktId === produkt.id}
            >
              <TableCell style={{ minWidth: "10em" }}>{produkt.id}</TableCell>
              <TableCell style={{ minWidth: "10em" }}>{produkt.name}</TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {produkt.verkaufspreis}
              </TableCell>

              <TableCell>
                <Icon name="feeder-arrow" />
              </TableCell>
            </TableRow>
          ))}
      </Table>
    </>
  );
};

export default ProdukteListe;
