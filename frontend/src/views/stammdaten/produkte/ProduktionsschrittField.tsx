import {
  Button,
  ButtonDesign,
  Label,
  Table,
  TableCell,
  TableColumn,
  TableRow,
} from "@ui5/webcomponents-react";
import { FieldArray, useFormikContext } from "formik";
import { FC } from "react";
import { useTranslation } from "react-i18next";

import AppSelect from "../../../components/arbeitsschritt-select/AppSelect";
import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import { Produkt } from "../../../models/Produkt";
import { Produktionsschritt } from "../../../models/Produktionsschritt";
import { useHandleFieldState } from "../../../util/errorUtil";

/**
 * Table zum Editieren und Anlegen von Maschinenbefähigungen
 * für {@link ProduktForm}.
 */
const ProduktionsschrittField: FC = () => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();
  const {
    values: { produktionsschritte },
    setFieldValue,
    errors,
  } = useFormikContext<Produkt>();

  const { data: arbeitsschritte, isLoading, isError } = useArbeitsschritte();

  return (
    <Table
      busy={isLoading}
      busyDelay={0}
      noDataText={isError ? t("stammdaten.tableUnexpectedError") : ""}
      columns={
        <>
          <TableColumn>
            <Label>{t("stammdaten.produkte.schrittOrder")}</Label>
          </TableColumn>
          <TableColumn>
            <Label>{t("stammdaten.produkte.arbeitsschritt")}</Label>
          </TableColumn>
          <TableColumn></TableColumn>
        </>
      }
    >
      <FieldArray name="produktionsschritte">
        {({ remove, push }) => (
          <>
            {isLoading ||
              produktionsschritte.sort((a, b) => a.schritt - b.schritt)
                .map(({ arbeitsschrittId, schritt }, index) => (
                  <TableRow key={index}>
                    <TableCell>{schritt + 1}</TableCell>
                    <TableCell>
                      <AppSelect
                        name={`produktionsschritte.${index}.arbeitsschrittId`}
                        selected={arbeitsschrittId}
                        data={arbeitsschritte ?? []}
                        onChange={(s) => {
                          setFieldValue(
                            `produktionsschritte.${index}.arbeitsschrittId`,
                            s?.id
                          );
                        }}
                        {...handleFieldState(
                          (errors as any)?.befaehigungen?.[index]?.schrittId
                        )}
                      />
                    </TableCell>
                    <TableCell>
                      <Button
                        design={ButtonDesign.Transparent}
                        icon="delete"
                        onClick={() => {
                          produktionsschritte.forEach((p) => {
                            if (p.schritt > schritt){
                              p.schritt = p.schritt - 1
                            }
                          })
                          remove(index)
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))
                .concat(
                  <TableRow key={produktionsschritte.length}>
                    <TableCell></TableCell>
                    <TableCell>
                      <AppSelect
                        name={`produktionsschritte.${produktionsschritte.length}.arbeitsschrittId`}
                        selected=""
                        placeholder={t(
                          "stammdaten.produkte.arbeitsschrittPlaceholder"
                        )}
                        data={arbeitsschritte!}
                        onChange={(s) => {
                          push({
                            arbeitsschrittId: s?.id,
                            schritt: produktionsschritte.length,
                          } as Produktionsschritt);
                        }}
                      />
                    </TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                )}
          </>
        )}
      </FieldArray>
    </Table>
  );
};

export default ProduktionsschrittField;
