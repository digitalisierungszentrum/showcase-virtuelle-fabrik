import { FC } from "react";

import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { useAddProdukte } from "../../../hooks/useProdukte";
import { Produkt } from "../../../models/Produkt";
import { StammdatenRoutes, getRoute } from "../../../routes/appRoutes";
import ProduktForm from "./ProduktForm";

/**
 * Ansicht zum Anlegen eines neuen Produktes.
 */
export const ProduktNew: FC = () => {
  const navigate = useNavigateWithParams();

  const add = useAddProdukte();

  const initialValues: Produkt = {
    id: "",
    name: "",
    verkaufspreis: 0,
    produktionsschritte: [],
    materialbedarf: [],
  };

  function onSubmit(values: Produkt) {
    add.mutateAsync(values).then(({ id }) =>
      navigate(
        getRoute("../" + StammdatenRoutes.ProduktDetails, {
          produktId: id,
        })
      )
    );
  }

  return (
    <ProduktForm
      initialValues={initialValues}
      onSubmit={onSubmit}
      onCancel={() => navigate("..")}
    />
  );
};
