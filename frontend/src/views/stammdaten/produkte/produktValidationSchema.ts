import { TFunction, useTranslation } from "react-i18next";
import {
  ObjectSchema,
  ValidationError,
  array,
  number,
  object,
  string,
} from "yup";

import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import { useMaterialien } from "../../../hooks/useMaterial";
import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import { Material } from "../../../models/Material";
import { Materialbedarf } from "../../../models/Materialbedarf";
import { NewProdukt } from "../../../models/NewProdukt";
import { Produkt } from "../../../models/Produkt";
import { Produktionsschritt } from "../../../models/Produktionsschritt";
import { combineQueries } from "../../../util/queryUtil";

export const materialBedarfValidationSchema: (
  t: TFunction
) => ObjectSchema<Materialbedarf> = (t) =>
  object({
    materialId: string().defined(),
    menge: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.required")),
  });

export const produktionsschrittValidationSchema: (
  t: TFunction
) => ObjectSchema<Produktionsschritt> = () =>
  object({
    arbeitsschrittId: string().defined(),
    schritt: number().defined(),
  });

/**
 * ValidationSchema für das {@link ProdukteForm}.
 */
export const produktValidationSchema: (
  arbeitsschritte: Arbeitsschritt[],
  materialien: Material[],
  t: TFunction
) => ObjectSchema<NewProdukt | Produkt> = (arbeitsschritte, materialien, t) =>
  object({
    id: string(),
    name: string().required(t("stammdaten.form.validation.required")),

    verkaufspreis: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),

    materialbedarf: array()
      .of(materialBedarfValidationSchema(t))
      // Testet ob das gleiche Material in mehreren Befähigungen vorkommt
      .test((bedarf, context) => {
        if (!bedarf) {
          return true;
        }
        const createError = (i: number, name?: string) =>
          context.createError({
            message: t("stammdaten.produkte.validation.materialUnique", {
              name,
            }),
            path: `materialbedarf.${i}.materialId`,
          });

        const errors: ValidationError[] = [];
        // map aller ArbeitsschrittIDs
        const schrittMap: { [id: string]: number } = {};

        bedarf.forEach(({ materialId }, index) => {
          // schaue für jede befähigung, ob schon eine andere befähigung mit
          // der gleichen SchrittId existiert
          if (schrittMap[materialId] !== undefined) {
            // falls ja, erzeuge error für beide befähigungen
            const name = materialien.find((m) => m.id === materialId)?.name;
            errors.push(createError(schrittMap[materialId], name));
            errors.push(createError(index, name));
          }

          // füge eintrag zur schrittMap hinzu
          schrittMap[materialId] = index;
        });

        if (errors.length > 0) {
          return new ValidationError(errors);
        }

        return true;
      })
      .default([]),

    produktionsschritte: array()
      .of(produktionsschrittValidationSchema(t))
      .default([]),
  });

/**
 * Stellt das ValidationSchema für Produkte zur verfügung.
 * Da das ValidationSchema von Arbeitsschritten und Materialien abhängig ist,
 * wird das Ergebnis in ein QueryResult gewrappt.
 *
 * @returns QueryResult mit ProduktValidationSchema als data.
 */
export const useProduktValidationSchema = () => {
  const arbeitsschritte = useArbeitsschritte();
  const materialien = useMaterialien();
  const { t } = useTranslation();

  return combineQueries(arbeitsschritte, materialien, (a, m) =>
    produktValidationSchema(a ?? [], m ?? [], t)
  );
};
