import {
  BreadcrumbsItem,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  Form,
  FormItem,
  Label,
  Loader,
  ObjectPageSection,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { FC } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import { useMaterialien } from "../../../hooks/useMaterial";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { useDeleteProdukt, useProdukt } from "../../../hooks/useProdukte";
import { AppParams } from "../../../routes/appRoutes";

/**
 * Detailansicht eines Produkts.
 * Das anzuzeigende Produkt wird den Route-Params entnommen.
 */

const ProduktDetails: FC = () => {
  const navigate = useNavigateWithParams();
  const { produktId } = useParams<AppParams>();
  const { t } = useTranslation();

  const { data: produkt, isLoading, isError, error } = useProdukt(produktId!);

  const arbeitsschrittQuery = useArbeitsschritte();
  const materialienQuery = useMaterialien();

  const deleteMutation = useDeleteProdukt();

  async function onDelete() {
    await deleteMutation.mutateAsync({ id: produkt!.id });

    navigate("..");
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!produkt || isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <StammdatenDetailsComponent
      breadcrumbs={
        <BreadcrumbsItem>{t("stammdaten.produkte.title")}</BreadcrumbsItem>
      }
      header={<Title wrappingType="Normal">{produkt.name}</Title>}
      headerContent={
        <FlexBox
          direction={FlexBoxDirection.Column}
          alignItems={FlexBoxAlignItems.Start}
          justifyContent={FlexBoxJustifyContent.Center}
          style={{ marginLeft: "1em" }}
        >
          <Label>{t("stammdaten.produkte.id")}</Label>
          <Text>{produkt.id}</Text>
        </FlexBox>
      }
      withActions
      onEdit={() => navigate("edit")}
      onDelete={onDelete}
      deleteConfirmationTitle={t("stammdaten.produkte.deleteConfirmationTitle")}
      deleteConfirmationBody={t("stammdaten.produkte.deleteConfirmationBody", {
        name: produkt.name,
      })}
      footer={
        <div style={{ margin: "0 -0.5em" }}>
          {deleteMutation.isLoading && <Loader />}
        </div>
      }
    >
      <ObjectPageSection
        id="general"
        titleText={t("stammdaten.produkte.sectionGeneral")}
        titleTextUppercase={false}
      >
        <Form
          columnsL={3}
          columnsXL={3}
          columnsM={3}
          columnsS={2}
          labelSpanXL={12}
          labelSpanL={12}
          labelSpanM={12}
        >
          <FormItem label={t("stammdaten.produkte.verkaufspreis")}>
            <Text>{produkt.verkaufspreis} &euro;</Text>
          </FormItem>
        </Form>
      </ObjectPageSection>
      <ObjectPageSection
        id="rezept"
        titleText={t("stammdaten.produkte.sectionBefaehigungen")}
        titleTextUppercase={false}
      >
        <Table
          busy={arbeitsschrittQuery.isLoading}
          noDataText={t("stammdaten.produkte.noArbeitsschritte")}
          style={{
            maxWidth: "20em",
          }}
          columns={
            <>
              <TableColumn>
                <Label>{t("stammdaten.produkte.schrittOrder")}</Label>
              </TableColumn>

              <TableColumn>
                <Label>{t("stammdaten.produkte.arbeitsschritt")}</Label>
              </TableColumn>
            </>
          }
        >
          {produkt.produktionsschritte.sort((a, b) => a.schritt - b.schritt).map(
            ({ arbeitsschrittId, schritt }, i) => (
              <TableRow key={i}>
                <TableCell>{schritt + 1}</TableCell>
                <TableCell>
                  {arbeitsschrittQuery.data?.find(
                    (s) => s.id === arbeitsschrittId
                  )?.name || ""}
                </TableCell>
              </TableRow>
            )
          )}
        </Table>
      </ObjectPageSection>

      <ObjectPageSection
        id="materialbedarf"
        titleText={t("stammdaten.produkte.materialbedarf")}
        titleTextUppercase={false}
      >
        <Table
          busy={materialienQuery.isLoading}
          noDataText={t("stammdaten.produkte.noMaterial")}
          style={{
            maxWidth: "20em",
          }}
          columns={
            <>
              <TableColumn>
                <Label>{t("stammdaten.produkte.materialName")}</Label>
              </TableColumn>

              <TableColumn
                style={{
                  textAlign: "end",
                  paddingRight: "1em",
                }}
              >
                <Label
                  style={{
                    width: "calc(100% - .5em)",
                    textAlign: "end",
                    marginRight: "1em",
                  }}
                >
                  {t("stammdaten.produkte.menge")}
                </Label>
              </TableColumn>
            </>
          }
        >
          {produkt.materialbedarf.map(({ materialId, menge }, i) => (
            <TableRow key={i}>
              <TableCell>
                {materialienQuery.data?.find((m) => m.id === materialId)?.name}
              </TableCell>
              <TableCell
                style={{
                  textAlign: "end",
                  paddingRight: "1em",
                }}
              >
                {menge}
              </TableCell>
            </TableRow>
          ))}
        </Table>
      </ObjectPageSection>
    </StammdatenDetailsComponent>
  );
};

export default ProduktDetails;
