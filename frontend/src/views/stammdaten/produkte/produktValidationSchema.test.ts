import { ValidationError } from "yup";
import { Produktionsschritt } from "../../../models/Produktionsschritt";

import {
  testMaterialien,
  testProdukt1,
  testProdukt2,
  testProdukt3,
} from "../../../util/testData";
import { testArbeitsschritte } from "../../../util/testData";
import { produktValidationSchema } from "./produktValidationSchema";

describe("Produkt Validation tests", () => {
  const validationSchema = produktValidationSchema(
    testArbeitsschritte,
    testMaterialien,
    () => {}
  );

  test("Erkennt korrekte Objekte", async () => {
    expect(await validationSchema.isValid(testProdukt1)).toBeTruthy();
    expect(await validationSchema.isValid(testProdukt2)).toBeTruthy();
    expect(await validationSchema.isValid(testProdukt3)).toBeTruthy();
  });

  test("Erkennt fehlende Attribute", async () => {
    expect(() =>
      validationSchema.validateSync({
        id: "test",
      })
    ).toThrow(ValidationError);
  });

  test("Strings in Number Feldern", async () => {
    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        verkaufspreis: "bad",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        produktionsschritte: [
          {
            arbeitsschrittId: "4578",
            schritt: "2",
          },
        ],
      })
    ).toBeTruthy();
  });

  test("Edgecases Number", async () => {
    expect(
      await validationSchema.isValid(
        {
          ...testProdukt1,
          name: "K",
        },
        {}
      )
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        name: "",
      })
    ).toBeFalsy();
  });

  test("erkennt undefined", async () => {
    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        id: undefined,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        name: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        verkaufspreis: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        produktionsschritte: [
          {
            arbeitsschrittId: undefined,
            schritt: 4,
          },
          {
            arbeitsschrittId: "4",
            schritt: undefined,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        materialbedarf: [
          {
            materialId: undefined,
            menge: 96,
          },

          {
            materialId: "2",
            menge: undefined,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        materialbedarf: [
          {
            materialId: "",
            menge: undefined,
          },

          {
            materialId: undefined,
            menge: "",
          },
        ],
      })
    ).toBeFalsy();
  });

  test("erkennt doppelte Produktionsschritte", async () => {
    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        produktionsschritte: [
          {
            arbeitsschrittId: "458",
            schritt: 4,
          },
          {
            arbeitsschrittId: "458",
            schritt: 4,
          },
        ],
      })
    ).toBeTruthy();
  });

  test("erkennt doppelte Materialschritte", async () => {
    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        materialbedarf: [
          {
            materialId: 58,
            menge: 44,
          },

          {
            materialId: 58,
            menge: 44,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        materialbedarf: [
          {
            materialId: 5,
            menge: 66,
          },

          {
            materialId: 8,
            menge: 44,
          },
        ],
      })
    ).toBeTruthy();
  });

  test("erkennt ob Feld Name falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        name: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        name: "Standfuß",
      })
    ).toBeTruthy();
  });

  test("erkennt ob Verkaufspreis leer oder negative Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        verkaufspreis: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        verkaufspreis: "Standfuß",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        verkaufspreis: "-4",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt1,
        verkaufspreis: 4.5,
      })
    ).toBeTruthy();
  });

  test("erkennt ob Produktionsschritte falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testProdukt2,
        produktionsschritte: [{}],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt2,
        produktionsschritte: [
          {
            schritt: "5",
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt2,
        produktionsschritte: [
          {
            arbeitsschrittId: "4578",
            schritt: "2",
          },
        ],
      })
    ).toBeTruthy();
  });

  test("erkennt ob Materialschritte falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testProdukt2,
        materialbedarf: [{}],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testProdukt2,
        materialbedarf: [
          {
            materialId: "",
            menge: 96,
          },
        ],
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testProdukt2,
        materialbedarf: [
          {
            menge: "",
          },
        ],
      })
    ).toBeFalsy();
  });
});

export {};
