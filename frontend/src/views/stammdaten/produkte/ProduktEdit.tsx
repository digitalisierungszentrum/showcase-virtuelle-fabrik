import { FC } from "react";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { useEditProdukt, useProdukt } from "../../../hooks/useProdukte";
import { Produkt } from "../../../models/Produkt";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";
import ProduktForm from "./ProduktForm";

/**
 * Editieransicht für ein bestehendes Produkt.
 * Das zu editierende Produkt wird den Route-Params entnommen.
 */
export const ProduktEdit: FC = () => {
  const { produktId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();

  const { data: produkt, isLoading, isError, error } = useProdukt(produktId!);
  const edit = useEditProdukt();

  function onSubmit(values: Produkt) {
    edit.mutateAsync(values).then(() =>
      navigate(
        getRoute("../" + StammdatenRoutes.ProduktDetails, {
          produktId: produktId,
        })
      )
    );
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!produkt || isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <ProduktForm
      initialValues={produkt!}
      onSubmit={onSubmit}
      onCancel={() =>
        navigate(
          "../" + getRoute(StammdatenRoutes.ProduktDetails, { produktId })
        )
      }
    />
  );
};
