import {
  Button,
  ButtonDesign,
  Label,
  StepInput,
  Table,
  TableCell,
  TableColumn,
  TableRow,
} from "@ui5/webcomponents-react";
import { FieldArray, useFormikContext } from "formik";
import { FC, useMemo } from "react";
import { useTranslation } from "react-i18next";

import AppSelect from "../../../components/arbeitsschritt-select/AppSelect";
import { useMaterialien } from "../../../hooks/useMaterial";
import { Produkt } from "../../../models/Produkt";
import { useHandleFieldState } from "../../../util/errorUtil";

/**
 * Table zum Editieren und Anlegen von Materialbedarf
 * für {@link ProduktForm}.
 */
const MaterialbedarfField: FC = () => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();
  const {
    values: { materialbedarf },
    handleChange,
    setFieldValue,
    errors,
  } = useFormikContext<Produkt>();

  const { data: materialien, isLoading, isError } = useMaterialien();

  // beinhaltet alle Materialien, die noch nicht verwendet wurden.
  const unusedMaterialien = useMemo(
    () =>
      materialien?.filter(
        (m) => !materialbedarf.find((b) => b.materialId === m.id)
      ) ?? [],
    [materialien, materialbedarf]
  );

  return (
    <Table
      busy={isLoading}
      busyDelay={0}
      noDataText={isError ? t("stammdaten.tableUnexpectedError") : ""}
      columns={
        <>
          <TableColumn>
            <Label>{t("stammdaten.produkte.materialName")}</Label>
          </TableColumn>
          <TableColumn>
            <Label>{t("stammdaten.produkte.menge")}</Label>
          </TableColumn>
          <TableColumn></TableColumn>
        </>
      }
    >
      <FieldArray name="materialbedarf">
        {({ remove, push }) => (
          <>
            {isLoading ||
              materialbedarf
                .map(({ materialId, menge }, index) => {
                  const material = materialien?.find(
                    (m) => m.id === materialId
                  );

                  return (
                    <TableRow key={index}>
                      <TableCell>
                        <AppSelect
                          name={`materialbedarf.${index}.materialId`}
                          selected={materialId}
                          data={(material ? [material] : []).concat(
                            unusedMaterialien
                          )}
                          onChange={(b) => {
                            setFieldValue(
                              `materialbedarf.${index}.materialId`,
                              b?.id
                            );
                          }}
                          {...handleFieldState(
                            (errors as any)?.materialbedarf?.[index]?.materialId
                          )}
                        />
                      </TableCell>
                      <TableCell>
                        <StepInput
                          min={1}
                          max={Infinity}
                          accessibleName={t(
                            "stammdaten.produkte.materialbedarf"
                          )}
                          name={`materialbedarf.${index}.menge`}
                          value={menge}
                          onChange={handleChange}
                        ></StepInput>
                      </TableCell>
                      <TableCell>
                        <Button
                          design={ButtonDesign.Transparent}
                          icon="delete"
                          onClick={() => remove(index)}
                        />
                      </TableCell>
                    </TableRow>
                  );
                })
                .concat(
                  <TableRow key={materialbedarf.length}>
                    <TableCell>
                      <AppSelect
                        name={`materialbedarf.${materialbedarf.length}.materialId`}
                        selected=""
                        placeholder={t(
                          "stammdaten.produkte.newMaterialPlaceholder"
                        )}
                        data={unusedMaterialien}
                        onChange={(m) => {
                          push({
                            materialId: m?.id,
                            menge: 0,
                          });
                        }}
                      />
                    </TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                )}
          </>
        )}
      </FieldArray>
    </Table>
  );
};

export default MaterialbedarfField;
