import {
  FlexBox,
  FlexBoxDirection,
  SplitterElement,
  SplitterLayout,
  Tab,
  TabContainer,
  TabContainerDomRef,
  Title,
  Ui5CustomEvent,
} from "@ui5/webcomponents-react";
import { Device, ThemingParameters } from "@ui5/webcomponents-react-base";
import { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useOutlet, useSearchParams } from "react-router-dom";

import {
  StammdatenParams,
  StammdatenViewOptions,
} from "../../routes/appRoutes";
import ArbeitsschritteListe from "./arbeitsschritte/ArbeitsschritteListe";
import ChargenListe from "./chargen/ChargenListe";
import MaschinenListe from "./maschinen/MaschinenListe";
import MaterialListe from "./materialien/MaterialListe";
import ProdukteListe from "./produkte/ProdukteListe";

/**
 * Stammdatenansicht eines Szenarios.
 */

const Stammdaten: FC = () => {
  const outlet = useOutlet();
  const { t } = useTranslation();
  const [SearchParams, setSearchParams] = useSearchParams();

  const [mediaSize, setMediaSize] = useState(Device.getCurrentRange().name);

  useEffect(() => {
    const mediaListener = (info: ReturnType<typeof Device.getCurrentRange>) => {
      setMediaSize(info.name);
    };

    Device.attachMediaHandler(mediaListener);

    return () => Device.detachMediaHandler(mediaListener);
  });

  useEffect(() => {
    if (!SearchParams.get(StammdatenParams.View)) {
      setSearchParams({
        [StammdatenParams.View]: StammdatenViewOptions.Maschinen,
      });
    }
  }, [SearchParams, setSearchParams]);

  function handleTabSelect(
    e: Ui5CustomEvent<TabContainerDomRef, { tab: HTMLElement }>
  ) {
    setSearchParams({ [StammdatenParams.View]: e.detail.tab.id });
  }

  const uebersicht = (
    <FlexBox
      direction={FlexBoxDirection.Column}
      style={{ width: "100%", height: "100%" }}
    >
      <Title
        style={{
          padding: "1rem",
          backgroundColor: ThemingParameters.sapList_GroupHeaderBackground,
        }}
      >
        {t("stammdaten.title")}
      </Title>
      <TabContainer
        fixed={true}
        onTabSelect={handleTabSelect}
        style={{
          height: "100%",
        }}
      >
        <Tab
          id={StammdatenViewOptions.Maschinen}
          selected={
            SearchParams.get(StammdatenParams.View) ===
            StammdatenViewOptions.Maschinen
          }
          icon="machine"
          text={t("stammdaten.maschinen.title")}
        >
          <MaschinenListe />
        </Tab>

        <Tab
          id={StammdatenViewOptions.Material}
          selected={
            SearchParams.get(StammdatenParams.View) ===
            StammdatenViewOptions.Material
          }
          icon="wrench"
          text={t("stammdaten.materialien.title")}
        >
          <MaterialListe />
        </Tab>

        <Tab
          id={StammdatenViewOptions.Produkte}
          selected={
            SearchParams.get(StammdatenParams.View) ===
            StammdatenViewOptions.Produkte
          }
          icon="product"
          text={t("stammdaten.produkte.title")}
        >
          <ProdukteListe />
        </Tab>

        <Tab
          id={StammdatenViewOptions.Chargen}
          selected={
            SearchParams.get(StammdatenParams.View) ===
            StammdatenViewOptions.Chargen
          }
          icon="cart"
          text={t("stammdaten.chargen.title")}
        >
          <ChargenListe />
        </Tab>

        <Tab
          id={StammdatenViewOptions.Arbeitsschritte}
          selected={
            SearchParams.get(StammdatenParams.View) ===
            StammdatenViewOptions.Arbeitsschritte
          }
          icon="activities"
          text={t("stammdaten.arbeitsschritte.title")}
        >
          <ArbeitsschritteListe />
        </Tab>
      </TabContainer>
    </FlexBox>
  );
  if (!outlet) {
    return uebersicht;
  }

  if (mediaSize === "Tablet" || mediaSize === "Phone") {
    return outlet;
  }

  return (
    <SplitterLayout
      className="content"
      style={{ width: "100%", height: "100%" }}
    >
      <SplitterElement size="50%">{uebersicht}</SplitterElement>

      <SplitterElement size="50%" minSize={500}>
        {outlet}
      </SplitterElement>
    </SplitterLayout>
  );
};

export default Stammdaten;
