import {
  BreadcrumbsItem,
  Form,
  FormItem,
  Input,
  InputType,
  Label,
  Loader,
  ObjectPageSection,
  Title,
} from "@ui5/webcomponents-react";
import { Formik, FormikHelpers } from "formik";
import { FC, ReactElement } from "react";
import { useTranslation } from "react-i18next";

import FormBar from "../../../components/form-bar/FormBar";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import { Maschine } from "../../../models/Maschine";
import { useHandleFieldState } from "../../../util/errorUtil";
import MaschinenbefaehigungsField from "./MaschinenbefaehigungsField";
import { useMaschineValidationSchema } from "./maschineValidationSchema";

interface MaschineFormProps {
  /**
   * Initiale Werte für das Form.
   */
  initialValues: Maschine;
  /**
   * Funktion wenn das Form Submitted wird.
   */
  onSubmit: (values: Maschine, helpers: FormikHelpers<Maschine>) => void;
  /**
   * Funktion des Cancel-Buttons.
   */
  onCancel: () => void;
}

/**
 * Gemeinsame Basis für die {@link MaschineEdit} und {@link MaschineNew} Views.
 */
const MaschineForm: FC<MaschineFormProps> = ({
  initialValues,
  onSubmit,
  onCancel,
}) => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();

  const { data: maschineValidationSchema } = useMaschineValidationSchema();

  // Values müssen noch geparsed werden
  function sanitizeSubmit(values: Maschine, helpers: FormikHelpers<Maschine>) {
    const parsed = maschineValidationSchema!.cast(values, {
      stripUnknown: true,
    });

    return onSubmit(parsed as Maschine, helpers);
  }

  return (
    <Formik
      initialValues={{
        ...initialValues,
        ausfallWahrscheinlichkeit:
          initialValues.ausfallWahrscheinlichkeit * 100,
      }}
      onSubmit={sanitizeSubmit}
      validationSchema={maschineValidationSchema}
      validateOnChange={false}
      validateOnBlur={false}
    >
      {({ values, handleChange, handleSubmit, errors, isSubmitting }) => {
        // helper function für Textinputs. Kann leider nicht in eigene
        // Component ausgelagert werden, weil das ui5 verwirrt.
        const formItem = (
          name: keyof Maschine,
          type: InputType,
          icon?: ReactElement
        ) => (
          <FormItem
            label={
              <Label id={`${name}Label`}>
                {t(`stammdaten.maschinen.${name}`)}
              </Label>
            }
          >
            <Input
              name={name}
              aria-labelledby={`${name}Label`}
              role="textbox"
              type={type}
              icon={icon}
              onInput={handleChange}
              value={values[name] + ""}
              {...handleFieldState(errors[name] as string)}
            />
          </FormItem>
        );

        return (
          <StammdatenDetailsComponent
            header={
              <Title>
                <Input
                  name="name"
                  aria-label={t("stammdaten.maschinen.name")}
                  role="textbox"
                  placeholder={t("stammdaten.maschinen.name")}
                  type={InputType.Text}
                  onInput={handleChange}
                  value={values.name}
                  {...handleFieldState(errors.name)}
                />
              </Title>
            }
            breadcrumbs={<BreadcrumbsItem>Maschinen</BreadcrumbsItem>}
            footer={
              <>
                {isSubmitting && <Loader />}
                <FormBar
                  submitEnabled={!isSubmitting}
                  errors={errors}
                  errorPrefix="stammdaten.maschinen"
                  onSubmit={handleSubmit}
                  onCancel={onCancel}
                />
              </>
            }
          >
            <ObjectPageSection
              id="general"
              titleText={t("stammdaten.maschinen.sectionGeneral")}
              titleTextUppercase={false}
            >
              <Form
                onSubmit={() => handleSubmit()}
                columnsL={3}
                columnsXL={3}
                columnsM={3}
                columnsS={2}
                labelSpanXL={12}
                labelSpanL={12}
                labelSpanM={12}
              >
                {formItem(
                  "mitarbeiterMin",
                  InputType.Number,
                  <span style={{ marginRight: "1em" }}>Anzahl</span>
                )}
                {formItem("mitarbeiterMax", InputType.Number)}
                {formItem(
                  "ruestzeit",
                  InputType.Number,
                  <span style={{ marginRight: "1em" }}>Sekunden</span>
                )}
                {formItem(
                  "kostenMinute",
                  InputType.Number,
                  <span>&euro;</span>
                )}
                {formItem(
                  "anschaffungskosten",
                  InputType.Number,
                  <span>&euro;</span>
                )}
                {formItem(
                  "ausfallWahrscheinlichkeit",
                  InputType.Number,
                  <span>%</span>
                )}
              </Form>
            </ObjectPageSection>
            <ObjectPageSection
              id="befaehigungen"
              titleText={t("stammdaten.maschinen.sectionBefaehigungen")}
              titleTextUppercase={false}
            >
              <MaschinenbefaehigungsField />
            </ObjectPageSection>
          </StammdatenDetailsComponent>
        );
      }}
    </Formik>
  );
};

export default MaschineForm;
