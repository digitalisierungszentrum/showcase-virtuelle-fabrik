import { FC } from "react";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useEditMaschine, useMaschine } from "../../../hooks/useMaschinen";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Maschine } from "../../../models/Maschine";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";
import MaschineForm from "./MaschineForm";

/**
 * Editieransicht für eine Bestehende Maschine.
 * Die zu editierende Maschine wird den Route-Params entnommen.
 */
export const MaschineEdit: FC = () => {
  const { maschineId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();

  const {
    data: maschine,
    isLoading,
    isError,
    error,
  } = useMaschine(maschineId!);
  const edit = useEditMaschine();

  function onSubmit(values: Maschine) {
    edit.mutateAsync(values).then(() =>
      navigate(
        getRoute("../" + StammdatenRoutes.MaschinenDetails, {
          maschineId: maschineId,
        })
      )
    );
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!maschine || isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <MaschineForm
      initialValues={{
        ...maschine,
        maschinenbefaehigungen: maschine.maschinenbefaehigungen.map((b, i) => ({
          ...b,
          key: i,
        })),
      }}
      onSubmit={onSubmit}
      onCancel={() =>
        navigate(
          "../" + getRoute(StammdatenRoutes.MaschinenDetails, { maschineId })
        )
      }
    />
  );
};
