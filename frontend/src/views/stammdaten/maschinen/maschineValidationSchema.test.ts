import { ValidationError } from "yup";

import {
  testArbeitsschritte,
  testMaschine1,
  testMaschine2,
  testMaschine3,
} from "../../../util/testData";
import { maschineValidationSchema } from "./maschineValidationSchema";

describe("Maschine Validation tests", () => {
  const validationSchema = maschineValidationSchema(
    testArbeitsschritte,
    () => {}
  );

  test("Erkennt korrekte Objekte", async () => {
    expect(await validationSchema.isValid(testMaschine1)).toBeTruthy();
    expect(await validationSchema.isValid(testMaschine2)).toBeTruthy();
    expect(await validationSchema.isValid(testMaschine3)).toBeTruthy();
  });

  test("Erkennt fehlende Attribute", async () => {
    expect(() =>
      validationSchema.validateSync({
        id: "test",
      })
    ).toThrow(ValidationError);
  });

  test("Strings in Number Feldern", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        wechselzeit: "bad",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        maschinenbefaehigungen: [
          {
            schrittId: "a1",
            taktrate: "bad",
          },
        ],
      })
    ).toBeFalsy();
  });

  test("erkennt ob Feld Name leer ist", async () => {
    expect(
      await validationSchema.isValid(
        {
          ...testMaschine1,
          name: "Max",
        },
        {}
      )
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        name: "",
      })
    ).toBeFalsy();
  });

  test("erkennt undefined", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        id: undefined,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        name: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        ruestzeit: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        kostenMinute: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        ausfallWahrscheinlichkeit: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        mitarbeiterMin: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        mitarbeiterMax: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        maschinenbefaehigungen: [
          {
            schrittId: undefined,
            taktrate: 1,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        maschinenbefaehigungen: [
          {
            schrittId: "a1",
            taktrate: undefined,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        maschinenbefaehigungen: [{}],
      })
    ).toBeFalsy();
  });

  test("erkennt doppelte arbeitsschritte", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        maschinenbefaehigungen: [
          {
            schrittId: "a1",
            taktrate: 1,
          },
          {
            schrittId: "a2",
            taktrate: 2,
          },
        ],
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        maschinenbefaehigungen: [
          {
            schrittId: "a1",
            taktrate: 1,
          },
          {
            schrittId: "a1",
            taktrate: 2,
          },
        ],
      })
    ).toBeFalsy();
  });

  test("erkennt ob die Taktrate falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        maschinenbefaehigungen: [
          {
            taktrate: "",
          },
          {
            taktrate: -1,
          },
        ],
      })
    ).toBeFalsy();
  });

  test("erkennt ob die Rüstzeit falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        ruestzeit: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        ruestzeit: "4",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        ruestzeit: "-4",
      })
    ).toBeFalsy();
  });

  test("erkennt ob die AusfallWahrscheinlichkeit falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        ausfallWahrscheinlichkeit: "",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        ausfallWahrscheinlichkeit: "4",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        ausfallWahrscheinlichkeit: "-4",
      })
    ).toBeFalsy();
  });

  test("erkennt ob die kostenMinute falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        kostenMinute: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        kostenMinute: "4",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        kostenMinute: "-4",
      })
    ).toBeFalsy();
  });

  test("erkennt ob die mitarbeiterMin falsche Werte annimmt", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        mitarbeiterMin: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        mitarbeiterMin: "5",
        mitarbeiterMax: "6",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        mitarbeiterMin: "-4",
      })
    ).toBeFalsy();
  });

  test("Mitarbeitermax muss größer als Mitarbeitermin sein", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        mitarbeiterMin: "4",
        mitarbeiterMax: "5",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaschine1,
        mitarbeiterMin: "4",
        mitarbeiterMax: "3",
      })
    ).toBeFalsy();
  });
});

export {};
