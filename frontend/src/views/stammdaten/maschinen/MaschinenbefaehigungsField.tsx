import {
  BusyIndicator,
  Button,
  ButtonDesign,
  Label,
  StepInput,
  Table,
  TableCell,
  TableColumn,
  TableRow,
} from "@ui5/webcomponents-react";
import { FieldArray, useFormikContext } from "formik";
import { FC, useMemo } from "react";
import { useTranslation } from "react-i18next";

import AppSelect from "../../../components/arbeitsschritt-select/AppSelect";
import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import { Maschine } from "../../../models/Maschine";
import { useHandleFieldState } from "../../../util/errorUtil";

/**
 * Table zum Editieren und Anlegen von Maschinenbefähigungen
 * für {@link MaschineForm}.
 */
const MaschinenbefaehigungsField: FC = () => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();
  const {
    values: { maschinenbefaehigungen: befaehigungen },
    handleChange,
    setFieldValue,
    errors,
  } = useFormikContext<Maschine>();

  const { data: arbeitsschritte, isLoading, isError } = useArbeitsschritte();

  // beinhaltet alle Arbeitsschritte, die noch nicht verwendet wurden.
  const unusedArbeitsschritte = useMemo(
    () =>
      arbeitsschritte?.filter(
        (a) => !befaehigungen.find((b) => b.schrittId === a.id)
      ) ?? [],
    [arbeitsschritte, befaehigungen]
  );

  return (
    <Table
      busy={isLoading}
      busyDelay={0}
      noDataText={isError ? t("stammdaten.tableUnexpectedError") : ""}
      columns={
        <>
          <TableColumn>
            <Label>{t("stammdaten.maschinen.befaehigungName")}</Label>
          </TableColumn>
          <TableColumn>
            <Label id="taktrate">
              {t("stammdaten.maschinen.befaehigungTaktrate")}
            </Label>
          </TableColumn>
          <TableColumn></TableColumn>
        </>
      }
    >
      <FieldArray name="maschinenbefaehigungen">
        {({ remove, push }) => (
          <>
            {isLoading ||
              befaehigungen
                .map(({ schrittId, taktrate }, index) => {
                  const arbeitsschritt = arbeitsschritte?.find(
                    (d) => d.id === schrittId
                  );
                  return (
                    <TableRow key={index}>
                      <TableCell>
                        <AppSelect
                          name={`.${index}.schrittId`}
                          selected={schrittId}
                          data={(arbeitsschritt ? [arbeitsschritt] : []).concat(
                            unusedArbeitsschritte
                          )}
                          onChange={(schritt) => {
                            setFieldValue(
                              `maschinenbefaehigungen.${index}.schrittId`,
                              schritt?.id
                            );
                          }}
                          {...handleFieldState(
                            (errors as any)?.befaehigungen?.[index]?.schrittId
                          )}
                        />
                      </TableCell>
                      <TableCell>
                        <StepInput
                          min={1}
                          max={Infinity}
                          accessibleName={t(
                            "stammdaten.maschinen.befaehigungTaktrate"
                          )}
                          name={`maschinenbefaehigungen.${index}.taktrate`}
                          value={taktrate}
                          onChange={handleChange}
                        ></StepInput>
                      </TableCell>
                      <TableCell>
                        <Button
                          design={ButtonDesign.Transparent}
                          icon="delete"
                          onClick={() => remove(index)}
                        />
                      </TableCell>
                    </TableRow>
                  );
                })
                .concat(
                  befaehigungen.length < 1 ?
                  <TableRow key={befaehigungen.length}>
                    <TableCell>
                      <BusyIndicator active={isLoading}>
                        <AppSelect
                          name={`maschinenbefaehigungen.${befaehigungen.length}.schrittId`}
                          selected=""
                          placeholder={t(
                            "stammdaten.maschinen.newBefaehigungPlaceholder"
                          )}
                          data={unusedArbeitsschritte}
                          onChange={(schritt) => {
                            push({
                              schrittId: schritt?.id,
                              taktrate: 0,
                            });
                          }}
                        />
                      </BusyIndicator>
                    </TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                  </TableRow> : []
                )}
          </>
        )}
      </FieldArray>
    </Table>
  );
};

export default MaschinenbefaehigungsField;
