import {
  BreadcrumbsItem,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  Form,
  FormItem,
  Label,
  Loader,
  ObjectPageSection,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { FC } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import { useDeleteMaschine, useMaschine } from "../../../hooks/useMaschinen";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { AppParams } from "../../../routes/appRoutes";
import { secondsToTimeString } from "../../../util/misc";

/**
 * Detailansicht einer Maschine.
 * Die anzuzeigende Maschine wird den Route-Params entnommen.
 */
const MaschineDetails: FC = () => {
  const navigate = useNavigateWithParams();
  const { maschineId } = useParams<AppParams>();
  const { t } = useTranslation();

  const maschineQuery = useMaschine(maschineId!);
  const { data: maschine } = maschineQuery;
  const arbeitsschrittQuery = useArbeitsschritte();
  const deleteMutation = useDeleteMaschine();

  async function onDelete() {
    await deleteMutation.mutateAsync({ id: maschine!.id });

    navigate("..");
  }

  if (maschineQuery.isLoading) {
    return <LoadingPage />;
  }

  if (maschineQuery.isError && maschineQuery.error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!maschine || maschineQuery.isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <StammdatenDetailsComponent
      breadcrumbs={
        <BreadcrumbsItem>{t("stammdaten.maschinen.title")}</BreadcrumbsItem>
      }
      header={<Title wrappingType="Normal">{maschine.name}</Title>}
      headerContent={
        <FlexBox
          direction={FlexBoxDirection.Column}
          alignItems={FlexBoxAlignItems.Start}
          justifyContent={FlexBoxJustifyContent.Center}
          style={{ marginLeft: "1em" }}
        >
          <Label>{t("stammdaten.maschinen.id")}</Label>
          <Text>{maschine.id}</Text>
        </FlexBox>
      }
      withActions
      onEdit={() => navigate("edit")}
      onDelete={onDelete}
      deleteConfirmationTitle={t(
        "stammdaten.maschinen.deleteConfirmationTitle"
      )}
      deleteConfirmationBody={t("stammdaten.maschinen.deleteConfirmationBody", {
        name: maschine.name,
      })}
      footer={
        <div style={{ margin: "0 -0.5em" }}>
          {deleteMutation.isLoading && <Loader />}
        </div>
      }
    >
      <ObjectPageSection
        id="general"
        titleText={t("stammdaten.maschinen.sectionGeneral")}
        titleTextUppercase={false}
      >
        <Form
          columnsL={3}
          columnsXL={3}
          columnsM={3}
          columnsS={2}
          labelSpanXL={12}
          labelSpanL={12}
          labelSpanM={12}
        >
          <FormItem label={t("stammdaten.maschinen.mitarbeiterMin")}>
            <Text>{maschine.mitarbeiterMin}</Text>
          </FormItem>
          <FormItem label={t("stammdaten.maschinen.mitarbeiterMax")}>
            <Text>{maschine.mitarbeiterMax}</Text>
          </FormItem>
          <FormItem label={t("stammdaten.maschinen.ruestzeit")}>
            <Text>{secondsToTimeString(maschine.ruestzeit)}</Text>
          </FormItem>
          <FormItem label={t("stammdaten.maschinen.kostenMinute")}>
            <Text>{maschine.kostenMinute} &euro;</Text>
          </FormItem>
          <FormItem label={t("stammdaten.maschinen.anschaffungskosten")}>
            <Text>{maschine.anschaffungskosten} &euro;</Text>
          </FormItem>
          <FormItem label={t("stammdaten.maschinen.ausfallWahrscheinlichkeit")}>
            <Text>{maschine.ausfallWahrscheinlichkeit * 100} %</Text>
          </FormItem>
        </Form>
      </ObjectPageSection>
      <ObjectPageSection
        id="befaehigungen"
        titleText={t("stammdaten.maschinen.sectionBefaehigungen")}
        titleTextUppercase={false}
      >
        <Table
          busy={arbeitsschrittQuery.isLoading}
          noDataText={t("stammdaten.maschinen.noBefaehigungen")}
          style={{
            maxWidth: "20em",
          }}
          columns={
            <>
              <TableColumn>
                <Label>{t("stammdaten.maschinen.befaehigungName")}</Label>
              </TableColumn>
              <TableColumn style={{ textAlign: "end", paddingRight: "1em" }}>
                <Label
                  style={{
                    width: "calc(100% - .5em)",
                    textAlign: "end",
                    marginRight: "1em",
                  }}
                >
                  {t("stammdaten.maschinen.befaehigungTaktrate")}
                </Label>
              </TableColumn>
            </>
          }
        >
          {maschine.maschinenbefaehigungen.map(({ schrittId, taktrate }) => (
            <TableRow key={schrittId}>
              <TableCell>
                {arbeitsschrittQuery.data?.find(
                  (schritt) => schritt.id === schrittId
                )?.name || ""}
              </TableCell>
              <TableCell style={{ textAlign: "end", paddingRight: "1em" }}>
                {taktrate}
              </TableCell>
            </TableRow>
          ))}
        </Table>
      </ObjectPageSection>
    </StammdatenDetailsComponent>
  );
};

export default MaschineDetails;
