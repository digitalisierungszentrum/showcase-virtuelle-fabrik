import { TFunction, useTranslation } from "react-i18next";
import {
  ObjectSchema,
  ValidationError,
  array,
  number,
  object,
  string,
} from "yup";

import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import { Maschine } from "../../../models/Maschine";
import { Maschinenbefaehigung } from "../../../models/Maschinenbefaehigung";
import { NewMaschine } from "../../../models/NewMaschine";

const maschinenBefaehigungValidationSchema: (
  t: TFunction
) => ObjectSchema<Maschinenbefaehigung> = (t) =>
  object({
    schrittId: string().defined(),
    taktrate: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.required")),
  });

/**
 * ValidationSchema für das {@link MaschineForm}.
 */
export const maschineValidationSchema: (
  arbeitsschritte: Arbeitsschritt[],
  t: TFunction
) => ObjectSchema<NewMaschine | Maschine> = (arbeitsschritte, t) =>
  object({
    id: string().transform((value) => (value === "" ? undefined : value)),
    name: string().required(t("stammdaten.form.validation.required")),
    ruestzeit: number()
      .integer(t("stammdaten.form.validation.integer"))
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),
    ausfallWahrscheinlichkeit: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number"))
      .transform((value) => (value ? value / 100 : 0)),
    kostenMinute: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),
    anschaffungskosten: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),
    mitarbeiterMin: number()
      .integer(t("stammdaten.form.validation.integer"))
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),
    mitarbeiterMax: number()
      .integer(t("stammdaten.form.validation.integer"))
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number"))
      .when(["mitarbeiterMin"], ([mitarbeiterMin], schema) =>
        schema.min(
          mitarbeiterMin,
          t("stammdaten.maschinen.validation.maxMoreThanMin")
        )
      ),
    maschinenbefaehigungen: array()
      .of(maschinenBefaehigungValidationSchema(t))
      // Testet ob der gleiche Arbeitsschritt in mehreren Befähigungen vorkommt
      .test((befaehigungen, context) => {
        if (!befaehigungen) {
          return true;
        }
        const createError = (i: number, name?: string) =>
          context.createError({
            message: t("stammdaten.maschinen.validation.befaehigungUnique", {
              name,
            }),
            path: `befaehigungen.${i}.schrittId`,
          });

        const errors: ValidationError[] = [];
        // map aller ArbeitsschrittIDs
        const schrittMap: { [id: string]: number } = {};

        befaehigungen.forEach(({ schrittId }, index) => {
          // schaue für jede befähigung, ob schon eine andere befähigung mit
          // der gleichen SchrittId existiert
          if (schrittMap[schrittId] !== undefined) {
            // falls ja, erzeuge error für beide befähigungen
            const name = arbeitsschritte.find((s) => s.id === schrittId)?.name;
            errors.push(createError(schrittMap[schrittId], name));
            errors.push(createError(index, name));
          }

          // füge eintrag zur schrittMap hinzu
          schrittMap[schrittId] = index;
        });

        if (errors.length > 0) {
          return new ValidationError(errors);
        }

        return true;
      })
      .default([]),
  });

/**
 * Stellt das ValidationSchema für Maschinen zur verfügung.
 * Da das ValidationSchema von Arbeitsschritten abhängig ist, wird das Ergebnis
 * in ein QueryResult gewrappt.
 *
 * @returns QueryResult mit MaschinenValidationSchema als data.
 */
export const useMaschineValidationSchema = () => {
  const arbeitsschritte = useArbeitsschritte();
  const { t } = useTranslation();

  return {
    ...arbeitsschritte,
    data: arbeitsschritte.data
      ? maschineValidationSchema(arbeitsschritte.data, t)
      : undefined,
  };
};
