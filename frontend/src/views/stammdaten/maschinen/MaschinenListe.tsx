import {
  Button,
  ButtonDesign,
  Icon,
  Input,
  Label,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  TableRowType,
  Title,
  Toolbar,
  ToolbarSpacer,
  WrappingType,
} from "@ui5/webcomponents-react";
import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { useAllMaschinen } from "../../../hooks/useMaschinen";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Maschine } from "../../../models/Maschine";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";

/**
 * Liste aller Maschinen in der Stammdatenübersicht.
 */
const MaschinenListe: FC = () => {
  const { maschineId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();

  const {
    data: maschinen,
    isLoading,
    isError,
    isRefetching,
  } = useAllMaschinen();

  const [filterStr, setFilterStr] = useState("");

  function filter(m: Maschine): boolean {
    return (
      filterStr === "" ||
      Object.values(m).find((v) =>
        String(v).toLowerCase().includes(filterStr.toLowerCase())
      )
    );
  }

  return (
    <>
      <Toolbar>
        <Title>{t("stammdaten.maschinen.title")}</Title>
        <ToolbarSpacer />
        <Input
          icon={<Icon name="search" />}
          onInput={(e) => setFilterStr(e.target.value ?? "")}
        />
        <Button
          role="button"
          aria-label={t("stammdaten.maschinen.newBtn")}
          design={ButtonDesign.Transparent}
          icon="add"
          onClick={() => navigate(StammdatenRoutes.MaschinenNew)}
        ></Button>
      </Toolbar>
      <Table
        busy={isRefetching || isLoading}
        busyDelay={0}
        noDataText={
          isLoading
            ? ""
            : isError
            ? t("stammdaten.tableUnexpectedError")
            : t("stammdaten.tableEmpty")
        }
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.maschinen.id")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.maschinen.name")}</Label>
            </TableColumn>
            <TableColumn>
              <Label
                wrappingType={WrappingType.Normal}
                style={{ display: "block", textAlign: "right" }}
              >
                {t("stammdaten.maschinen.ruestzeitSekunden")}
              </Label>
            </TableColumn>
            <TableColumn>
              <Label
                wrappingType={WrappingType.Normal}
                style={{ display: "block", textAlign: "right" }}
              >
                {t("stammdaten.maschinen.kostenMinuteWaehrung")}
              </Label>
            </TableColumn>
            <TableColumn>
              <Label
                wrappingType={WrappingType.Normal}
                style={{ display: "block", textAlign: "right" }}
              >
                {t("stammdaten.maschinen.anschaffungskostenWaehrung")}
              </Label>
            </TableColumn>
            <TableColumn>
              <Label
                wrappingType={WrappingType.Normal}
                style={{ display: "block", textAlign: "right" }}
              >
                {t("stammdaten.maschinen.mitarbeiterMax")}
              </Label>
            </TableColumn>
            <TableColumn minWidth={700}>
              <Label
                wrappingType={WrappingType.Normal}
                style={{ display: "block", textAlign: "right" }}
              >
                {t("stammdaten.maschinen.mitarbeiterMin")}
              </Label>
            </TableColumn>
            <TableColumn minWidth={700}>
              <Label
                wrappingType={WrappingType.Normal}
                style={{ display: "block", textAlign: "right" }}
              >
                {t("stammdaten.maschinen.ausfallWahrscheinlichkeitProzent")}
              </Label>
            </TableColumn>
            <TableColumn></TableColumn>
          </>
        }
      >
        {maschinen &&
          maschinen.filter(filter).map((maschine) => (
            <TableRow
              key={maschine.id}
              type={TableRowType.Active}
              onClick={() =>
                navigate(
                  getRoute(StammdatenRoutes.MaschinenDetails, {
                    maschineId: maschine.id,
                  })
                )
              }
              selected={maschineId === maschine.id}
            >
              <TableCell style={{ minWidth: "10em" }}>{maschine.id}</TableCell>
              <TableCell>{maschine.name}</TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {maschine.ruestzeit}
              </TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {maschine.kostenMinute}
              </TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {maschine.anschaffungskosten}
              </TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {maschine.mitarbeiterMax}
              </TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {maschine.mitarbeiterMin}
              </TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {maschine.ausfallWahrscheinlichkeit * 100}
              </TableCell>
              <TableCell>
                <Icon name="feeder-arrow" />
              </TableCell>
            </TableRow>
          ))}
      </Table>
    </>
  );
};

export default MaschinenListe;
