import { FC } from "react";

import { useAddMaschine } from "../../../hooks/useMaschinen";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Maschine } from "../../../models/Maschine";
import { StammdatenRoutes, getRoute } from "../../../routes/appRoutes";
import MaschineForm from "./MaschineForm";

/**
 * Ansicht zum Anlegen einer neuen Maschine.
 */
export const MaschineNew: FC = () => {
  const navigate = useNavigateWithParams();

  const add = useAddMaschine();

  const initialValues: Maschine = {
    id: "",
    name: "",
    ausfallWahrscheinlichkeit: 0,
    maschinenbefaehigungen: [],
    kostenMinute: 0,
    anschaffungskosten: 0,
    mitarbeiterMax: 0,
    mitarbeiterMin: 0,
    ruestzeit: 0,
  };

  function onSubmit(values: Maschine) {
    add.mutateAsync(values).then(({ id }) =>
      navigate(
        getRoute("../" + StammdatenRoutes.MaschinenDetails, {
          maschineId: id,
        })
      )
    );
  }

  return (
    <MaschineForm
      initialValues={initialValues}
      onSubmit={onSubmit}
      onCancel={() => navigate("..")}
    />
  );
};
