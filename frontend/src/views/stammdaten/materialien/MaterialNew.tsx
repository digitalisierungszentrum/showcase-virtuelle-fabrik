import { FC } from "react";

import { useAddMaterial } from "../../../hooks/useMaterial";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Material } from "../../../models/Material";
import { StammdatenRoutes, getRoute } from "../../../routes/appRoutes";
import MaterialForm from "./MaterialForm";

/**
 * Ansicht zum Anlegen eines neuen Materials.
 */
export const MaterialNew: FC = () => {
  const navigate = useNavigateWithParams();

  const add = useAddMaterial();

  const initialValues: Material = {
    id: "",
    name: "",
    kostenStueck: 0,
    bestand: 0,
    aufstockenMinute: 0,
  };

  function onSubmit(values: Material) {
    add.mutateAsync(values).then(({ id }) =>
      navigate(
        getRoute("../" + StammdatenRoutes.MaterialienDetails, {
          materialId: id,
        })
      )
    );
  }

  return (
    <MaterialForm
      initialValues={initialValues}
      onSubmit={onSubmit}
      onCancel={() => navigate("..")}
    />
  );
};
