import {
  Button,
  ButtonDesign,
  Icon,
  Input,
  Label,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  TableRowType,
  Title,
  Toolbar,
  ToolbarSpacer,
  WrappingType,
} from "@ui5/webcomponents-react";
import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { useMaterialien } from "../../../hooks/useMaterial";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Material } from "../../../models/Material";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";

/**
 * Liste aller Materialien in der Stammdatenübersicht.
 */
const MaterialListe: FC = () => {
  const { materialId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();

  const {
    data: materialien,
    isLoading,
    isError,
    isRefetching,
  } = useMaterialien();
  const [filterStr, setFilterStr] = useState("");

  function filter(m: Material): boolean {
    return (
      filterStr === "" ||
      Object.values(m).find((v) =>
        String(v).toLowerCase().includes(filterStr.toLowerCase())
      )
    );
  }

  return (
    <>
      <Toolbar>
        <Title>{t("stammdaten.materialien.title")}</Title>
        <ToolbarSpacer />
        <Input
          icon={<Icon name="search" />}
          onInput={(e) => setFilterStr(e.target.value ?? "")}
        />
        <Button
          role="button"
          aria-label={t("stammdaten.materialien.newBtn")}
          design={ButtonDesign.Transparent}
          icon="add"
          onClick={() => navigate(StammdatenRoutes.MaterialienNew)}
        ></Button>
      </Toolbar>
      <Table
        busy={isRefetching || isLoading}
        busyDelay={0}
        noDataText={
          isLoading
            ? ""
            : isError
            ? t("stammdaten.tableUnexpectedError")
            : t("stammdaten.tableEmpty")
        }
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.materialien.id")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.materialien.name")}</Label>
            </TableColumn>
            <TableColumn>
              <Label style={{ display: "block", textAlign: "right" }}>
                {t("stammdaten.materialien.kostenStueckWaehrung")}
              </Label>
            </TableColumn>
            <TableColumn>
              <Label
                wrappingType={WrappingType.Normal}
                style={{ display: "block", textAlign: "right" }}
              >
                {t("stammdaten.materialien.bestandStueck")}
              </Label>
            </TableColumn>
            <TableColumn>
              <Label
                wrappingType={WrappingType.Normal}
                style={{ display: "block", textAlign: "right" }}
              >
                {t("stammdaten.materialien.aufstockenMinuteStueck")}
              </Label>
            </TableColumn>
            <TableColumn></TableColumn>
          </>
        }
      >
        {materialien &&
          materialien.filter(filter).map((material) => (
            <TableRow
              key={material.id}
              type={TableRowType.Active}
              onClick={() =>
                navigate(
                  getRoute(StammdatenRoutes.MaterialienDetails, {
                    materialId: material.id,
                  })
                )
              }
              selected={materialId === material.id}
            >
              <TableCell>{material.id}</TableCell>
              <TableCell>{material.name}</TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {material.kostenStueck}
              </TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {material.bestand}
              </TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {material.aufstockenMinute}
              </TableCell>
              <TableCell>
                <Icon name="feeder-arrow" />
              </TableCell>
            </TableRow>
          ))}
      </Table>
    </>
  );
};

export default MaterialListe;
