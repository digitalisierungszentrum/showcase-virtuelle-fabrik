import { FC } from "react";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useEditMaterial, useMaterial } from "../../../hooks/useMaterial";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Material } from "../../../models/Material";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";
import MaterialForm from "./MaterialForm";

/**
 * Editieransicht für ein bestehendes Material.
 * Das zu editierende Material wird den Route-Params entnommen.
 */
export const MaterialEdit: FC = () => {
  const { materialId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();

  const {
    data: material,
    isLoading,
    isError,
    error,
  } = useMaterial(materialId!);
  const edit = useEditMaterial();

  function onSubmit(values: Material) {
    edit.mutateAsync(values).then(() =>
      navigate(
        getRoute("../" + StammdatenRoutes.MaterialienDetails, {
          materialId: materialId,
        })
      )
    );
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!material || isError) {
    return <UnexpectedErrorPage />;
  }
  return (
    <MaterialForm
      initialValues={material!}
      onSubmit={onSubmit}
      onCancel={() =>
        navigate(
          "../" + getRoute(StammdatenRoutes.MaterialienDetails, { materialId })
        )
      }
    />
  );
};
