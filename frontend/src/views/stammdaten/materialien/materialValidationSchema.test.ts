import { ValidationError } from "yup";

import {
  testMaterial1,
  testMaterial2,
  testMaterial3,
} from "../../../util/testData";
import { materialValidationSchema } from "./materialValidationSchema";

describe("Material Validation tests", () => {
  const validationSchema = materialValidationSchema(() => {});

  test("Erkennt korrekte Objekte", async () => {
    expect(await validationSchema.isValid(testMaterial1)).toBeTruthy();
    expect(await validationSchema.isValid(testMaterial2)).toBeTruthy();
    expect(await validationSchema.isValid(testMaterial3)).toBeTruthy();
  });

  test("Erkennt fehlende Attribute", async () => {
    expect(() =>
      validationSchema.validateSync({
        id: "test",
      })
    ).toThrow(ValidationError);
  });

  test("erkennt undefined", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        id: undefined,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        name: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        kostenStueck: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        bestand: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        aufstockenMinute: undefined,
      })
    ).toBeFalsy();
  });

  test("erkennt ob Feld Name leer", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        name: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        name: "Schraube",
      })
    ).toBeTruthy();
  });

  test("erkennt ob Feld kostenStück leer oder negativ ist", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        kostenStueck: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        kostenStueck: -2,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        kostenStueck: 4,
      })
    ).toBeTruthy();
  });

  test("erkennt ob Bestand leer, ganzzahlig oder negativ ist", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        bestand: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        bestand: 0.5,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        bestand: -2,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        bestand: 4,
      })
    ).toBeTruthy();
  });

  test("erkennt ob Aufstocken pro Minute leer oder negativ ist", async () => {
    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        aufstockenMinute: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        aufstockenMinute: 0.5,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        aufstockenMinute: -2,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMaterial1,
        aufstockenMinute: 4,
      })
    ).toBeTruthy();
  });
});

export {};
