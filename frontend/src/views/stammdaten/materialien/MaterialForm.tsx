import {
  BreadcrumbsItem,
  Form,
  FormItem,
  Input,
  InputType,
  Label,
  Loader,
  ObjectPageSection,
  Title,
} from "@ui5/webcomponents-react";
import { Formik, FormikHelpers } from "formik";
import { FC, ReactElement } from "react";
import { useTranslation } from "react-i18next";

import FormBar from "../../../components/form-bar/FormBar";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import { Material } from "../../../models/Material";
import { useHandleFieldState } from "../../../util/errorUtil";
import { useMaterialValidationSchema } from "./materialValidationSchema";

interface MaterialFormProps {
  /**
   * Initiale Werte für das Form.
   */
  initialValues: Material;
  /**
   * Funktion wenn das Form Submitted wird.
   */
  onSubmit: (values: Material, helpers: FormikHelpers<Material>) => void;
  /**
   * Funktion des Cancel-Buttons.
   */
  onCancel: () => void;
}

/**
 * Gemeinsame Basis für die {@link MaterialEdit} und {@link MaterialNew} Views.
 */
const MaterialForm: FC<MaterialFormProps> = ({
  initialValues,
  onSubmit,
  onCancel,
}) => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();

  const materialValidationSchema = useMaterialValidationSchema();

  // Values müssen noch geparsed werden
  function sanitizeSubmit(values: Material, helpers: FormikHelpers<Material>) {
    const parsed = materialValidationSchema.cast(values, {
      stripUnknown: true,
    });

    return onSubmit(parsed as Material, helpers);
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={sanitizeSubmit}
      validationSchema={materialValidationSchema}
      validateOnChange={false}
      validateOnBlur={false}
    >
      {({ values, handleChange, handleSubmit, errors, isSubmitting }) => {
        // helper function für Textinputs. Kann leider nicht in eigene
        // Component ausgelagert werden, weil das ui5 verwirrt.
        const formItem = (
          name: keyof Material,
          type: InputType,
          icon?: ReactElement
        ) => (
          <FormItem
            label={
              <Label id={`${name}Label`}>
                {t(`stammdaten.materialien.${name}`)}
              </Label>
            }
          >
            <Input
              name={name}
              aria-labelledby={`${name}Label`}
              role="textbox"
              type={type}
              icon={icon}
              onInput={handleChange}
              value={values[name] + ""}
              {...handleFieldState(errors[name] as string)}
            />
          </FormItem>
        );

        return (
          <StammdatenDetailsComponent
            header={
              <Title>
                <Input
                  name="name"
                  aria-label={t("stammdaten.materialien.name")}
                  role="textbox"
                  placeholder={t("stammdaten.materialien.name")}
                  type={InputType.Text}
                  onInput={handleChange}
                  value={values.name}
                  {...handleFieldState(errors.name)}
                />
              </Title>
            }
            breadcrumbs={<BreadcrumbsItem>Materialien</BreadcrumbsItem>}
            footer={
              <>
                {isSubmitting && <Loader />}
                <FormBar
                  submitEnabled={!isSubmitting}
                  errors={errors}
                  errorPrefix="stammdaten.materialien"
                  onSubmit={handleSubmit}
                  onCancel={onCancel}
                />
              </>
            }
          >
            <ObjectPageSection
              id="general"
              titleText={t("stammdaten.materialien.sectionGeneral")}
              titleTextUppercase={false}
            >
              <Form
                onSubmit={() => handleSubmit()}
                columnsL={3}
                columnsXL={3}
                columnsM={3}
                columnsS={2}
                labelSpanXL={12}
                labelSpanL={12}
                labelSpanM={12}
              >
                {formItem(
                  "kostenStueck",
                  InputType.Number,
                  <span>&euro;</span>
                )}
                {formItem(
                  "bestand",
                  InputType.Number,
                  <span style={{ marginRight: "1em" }}>Stück </span>
                )}
                {formItem(
                  "aufstockenMinute",
                  InputType.Number,
                  <span style={{ marginRight: "1em" }}>Stück </span>
                )}
              </Form>
            </ObjectPageSection>
            <ObjectPageSection id={""}>
              <></>
            </ObjectPageSection>
          </StammdatenDetailsComponent>
        );
      }}
    </Formik>
  );
};

export default MaterialForm;
