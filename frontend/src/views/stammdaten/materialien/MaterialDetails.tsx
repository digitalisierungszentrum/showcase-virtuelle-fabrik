import {
  BreadcrumbsItem,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  Form,
  FormItem,
  Label,
  Loader,
  ObjectPageSection,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { FC } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useDeleteMaterial, useMaterial } from "../../../hooks/useMaterial";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { AppParams } from "../../../routes/appRoutes";

/**
 * Detailansicht eines Materials.
 * Das anzuzeigende Material wird den Route-Params entnommen.
 */
const MaterialDetails: FC = () => {
  const navigate = useNavigateWithParams();
  const { materialId } = useParams<AppParams>();
  const { t } = useTranslation();

  const {
    data: material,
    isLoading,
    isError,
    error,
  } = useMaterial(materialId!);

  const deleteMutation = useDeleteMaterial();

  async function onDelete() {
    await deleteMutation.mutateAsync({ id: material!.id });

    navigate("..");
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!material || isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <StammdatenDetailsComponent
      breadcrumbs={
        <BreadcrumbsItem>{t("stammdaten.materialien.title")}</BreadcrumbsItem>
      }
      header={<Title wrappingType="Normal">{material.name}</Title>}
      headerContent={
        <FlexBox
          direction={FlexBoxDirection.Column}
          alignItems={FlexBoxAlignItems.Start}
          justifyContent={FlexBoxJustifyContent.Center}
          style={{ marginLeft: "1em" }}
        >
          <Label>{t("stammdaten.materialien.id")}</Label>
          <Text>{material.id}</Text>
        </FlexBox>
      }
      withActions
      onEdit={() => navigate("edit")}
      onDelete={onDelete}
      deleteConfirmationTitle={t(
        "stammdaten.materialien.deleteConfirmationTitle"
      )}
      deleteConfirmationBody={t(
        "stammdaten.materialien.deleteConfirmationBody",
        {
          name: material.name,
        }
      )}
      footer={
        <div style={{ margin: "0 -0.5em" }}>
          {deleteMutation.isLoading && <Loader />}
        </div>
      }
    >
      <ObjectPageSection
        id="general"
        titleText={t("stammdaten.materialien.sectionGeneral")}
        titleTextUppercase={false}
      >
        <Form
          columnsL={3}
          columnsXL={3}
          columnsM={3}
          columnsS={2}
          labelSpanXL={12}
          labelSpanL={12}
          labelSpanM={12}
        >
          <FormItem label={t("stammdaten.materialien.kostenStueck")}>
            <Text>{material.kostenStueck} &euro;</Text>
          </FormItem>
          <FormItem label={t("stammdaten.materialien.bestand")}>
            <Text>{material.bestand} Stück</Text>
          </FormItem>
          <FormItem label={t("stammdaten.materialien.aufstockenMinute")}>
            <Text>{material.aufstockenMinute} Stück</Text>
          </FormItem>
        </Form>
      </ObjectPageSection>
    </StammdatenDetailsComponent>
  );
};

export default MaterialDetails;
