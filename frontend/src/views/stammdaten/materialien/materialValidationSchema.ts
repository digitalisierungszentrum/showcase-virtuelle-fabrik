import { TFunction } from "i18next";
import { useTranslation } from "react-i18next";
import { ObjectSchema, number, object, string } from "yup";

import { Material } from "../../../models/Material";
import { NewMaterial } from "../../../models/NewMaterial";

/**
 * ValidationSchema für das {@link MaterialienForm}.
 */
export const materialValidationSchema: (
  t: TFunction
) => ObjectSchema<NewMaterial | Material> = (t) =>
  object({
    id: string().transform((value) => (value === "" ? undefined : value)),

    name: string().required(t("stammdaten.form.validation.required")),

    kostenStueck: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),

    bestand: number()
      .integer(t("stammdaten.form.validation.integer"))
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),

    aufstockenMinute: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),
  });

/**
 * Stellt das ValidationSchema für Material zur verfügung.
 *
 * @returns MaterialValidationSchema.
 */
export const useMaterialValidationSchema = () => {
  const { t } = useTranslation();

  return materialValidationSchema(t);
};
