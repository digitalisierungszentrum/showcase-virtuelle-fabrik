import {
  BreadcrumbsItem,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  Form,
  FormItem,
  Label,
  Loader,
  ObjectPageSection,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { FC } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useCharge, useDeleteCharge } from "../../../hooks/useChargen";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { useProdukte } from "../../../hooks/useProdukte";
import { AppParams } from "../../../routes/appRoutes";

/**
 * Detailansicht einer Charge.
 * Die anzuzeigende Charge wird den Route-Params entnommen.
 */
const ChargeDetails: FC = () => {
  const navigate = useNavigateWithParams();
  const { chargeId } = useParams<AppParams>();
  const { t } = useTranslation();

  const { data: charge, isLoading, isError, error } = useCharge(chargeId!);
  const produkteQuery = useProdukte();

  const deleteMutation = useDeleteCharge();

  async function onDelete() {
    await deleteMutation.mutateAsync({ id: charge!.id });

    navigate("..");
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!charge || isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <StammdatenDetailsComponent
      breadcrumbs={
        <BreadcrumbsItem>{t("stammdaten.chargen.title")}</BreadcrumbsItem>
      }
      header={<Title wrappingType="Normal">{charge.name}</Title>}
      headerContent={
        <FlexBox
          direction={FlexBoxDirection.Column}
          alignItems={FlexBoxAlignItems.Start}
          justifyContent={FlexBoxJustifyContent.Center}
          style={{ marginLeft: "1em" }}
        >
          <Label>{t("stammdaten.chargen.id")}</Label>
          <Text>{charge.id}</Text>
        </FlexBox>
      }
      withActions
      onEdit={() => navigate("edit")}
      onDelete={onDelete}
      deleteConfirmationTitle={t("stammdaten.chargen.deleteConfirmationTitle")}
      deleteConfirmationBody={t("stammdaten.chargen.deleteConfirmationBody", {
        name: charge.name,
      })}
      footer={
        <div style={{ margin: "0 -0.5em" }}>
          {deleteMutation.isLoading && <Loader />}
        </div>
      }
    >
      <ObjectPageSection
        id="general"
        titleText={t("stammdaten.chargen.sectionGeneral")}
        titleTextUppercase={false}
      >
        <Form
          columnsL={3}
          columnsXL={3}
          columnsM={3}
          columnsS={2}
          labelSpanXL={12}
          labelSpanL={12}
          labelSpanM={12}
        >
          <FormItem label={t("stammdaten.chargen.prioritaet")}>
            <Text>{charge.prioritaet}</Text>
          </FormItem>
          <FormItem label={t("stammdaten.chargen.produktName")}>
            <Text>
              {produkteQuery.data?.find(
                (m) => m.id === charge.produktbedarf[0]?.produktId
              )?.name ?? ""}
            </Text>
          </FormItem>
          <FormItem label={t("stammdaten.chargen.stueckzahl")}>
            <Text>{charge.produktbedarf[0]?.stueckzahl ?? 0} Stück</Text>
          </FormItem>
        </Form>
      </ObjectPageSection>
    </StammdatenDetailsComponent>
  );
};

export default ChargeDetails;
