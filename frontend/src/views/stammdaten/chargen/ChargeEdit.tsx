import { FC } from "react";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useCharge, useEditCharge } from "../../../hooks/useChargen";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Charge } from "../../../models/Charge";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";
import ChargeForm from "./ChargeForm";

/**
 * Editieransicht für eine Bestehende Charge.
 * Die zu editierende Charge wird den Route-Params entnommen.
 */
export const ChargeEdit: FC = () => {
  const { chargeId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();

  const { data: charge, isLoading, isError, error } = useCharge(chargeId!);
  const edit = useEditCharge();

  function onSubmit(values: Charge) {
    edit.mutateAsync(values).then(() =>
      navigate(
        getRoute("../" + StammdatenRoutes.ChargeDetails, {
          chargeId: chargeId,
        })
      )
    );
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!charge || isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <ChargeForm
      initialValues={charge!}
      onSubmit={onSubmit}
      onCancel={() =>
        navigate("../" + getRoute(StammdatenRoutes.ChargeDetails, { chargeId }))
      }
    />
  );
};
