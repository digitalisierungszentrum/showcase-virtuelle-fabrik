import { ValidationError } from "yup";

import { testCharge1, testCharge2, testCharge3 } from "../../../util/testData";
import { chargeValidationSchema } from "./chargeValidationSchema";

describe("Charge Validation tests", () => {
  const validationSchema = chargeValidationSchema(() => {});

  test("Erkennt korrekte Objekte", async () => {
    expect(await validationSchema.isValid(testCharge1)).toBeTruthy();
    expect(await validationSchema.isValid(testCharge2)).toBeTruthy();
    expect(await validationSchema.isValid(testCharge3)).toBeTruthy();
  });

  test("Erkennt fehlende Attribute", async () => {
    expect(() =>
      validationSchema.validateSync({
        id: "test",
      })
    ).toThrow(ValidationError);
  });

  test("Strings in Number Feldern", async () => {
    expect(
      await validationSchema.isValid({
        ...testCharge1,
        wechselzeit: "bad",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: "bad",
          },
        ],
      })
    ).toBeTruthy();
  });

  test("erkennt undefined", async () => {
    expect(
      await validationSchema.isValid({
        ...testCharge1,
        id: undefined,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        name: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        prioritaet: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        produktbedarf: [
          {
            produktId: "558",
            stueckzahl: undefined,
          },
          {
            produktId: undefined,
            stueckzahl: 55,
          },
        ],
      })
    ).toBeFalsy();
  });

  test("erkennt doppeltes Bedarf an Produkten", async () => {
    expect(
      await validationSchema.isValid({
        ...testCharge1,
        produktbedarf: [
          {
            produktId: 47,
            stueckzahl: 45,
          },
          {
            produktId: 47,
            stueckzahl: 45,
          },
        ],
      })
    ).toBeTruthy();
  });

  test("erkennt leeres Produktbedarf", async () => {
    expect(
      await validationSchema.isValid({
        ...testCharge1,
        produktbedarf: [{}],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        produktbedarf: [
          {
            produktId: 47,
            stueckzahl: "",
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        produktbedarf: [
          {
            produktId: "",
            stueckzahl: 45,
          },
        ],
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        produktbedarf: [
          {
            produktId: 47,
            stueckzahl: 45,
          },
        ],
      })
    ).toBeTruthy();
  });

  test("erkennt ob Feld Priorität leer", async () => {
    expect(
      await validationSchema.isValid({
        ...testCharge1,
        prioritaet: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        prioritaet: "4",
      })
    ).toBeTruthy();
  });

  test("erkennt ob Feld Name leer", async () => {
    expect(
      await validationSchema.isValid({
        ...testCharge1,
        name: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testCharge1,
        name: "Regalwand",
      })
    ).toBeTruthy();
  });
});

export {};
