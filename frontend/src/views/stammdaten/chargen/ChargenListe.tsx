import {
  Button,
  ButtonDesign,
  Icon,
  Input,
  Label,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  TableRowType,
  Title,
  Toolbar,
  ToolbarSpacer,
} from "@ui5/webcomponents-react";
import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { useChargen } from "../../../hooks/useChargen";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { useProdukte } from "../../../hooks/useProdukte";
import { Charge } from "../../../models/Charge";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";
import { combineQueries } from "../../../util/queryUtil";

/**
 * Liste aller Charge in der Stammdatenübersicht.
 */
const ChargenListe: FC = () => {
  const { chargeId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();

  const chargenQuery = useChargen();
  const produkteQuery = useProdukte();

  const { data, isLoading, isError, isRefetching } = combineQueries(
    chargenQuery,
    produkteQuery,
    (chargen, produkte) => {
      return chargen?.map((c) => ({
        ...c,
        produktbedarf: c.produktbedarf.map((bedarf) => ({
          ...bedarf,
          name: produkte?.find((p) => p.id === bedarf.produktId)?.name,
        })),
      }));
    }
  );

  const [filterStr, setFilterStr] = useState("");

  function filter(m: Charge): boolean {
    return (
      filterStr === "" ||
      Object.values(m).find((v) =>
        String(v).toLowerCase().includes(filterStr.toLowerCase())
      )
    );
  }

  return (
    <>
      <Toolbar>
        <Title>{t("stammdaten.chargen.title")}</Title>
        <ToolbarSpacer />
        <Input
          icon={<Icon name="search" />}
          onInput={(e) => setFilterStr(e.target.value ?? "")}
        />
        <Button
          role="button"
          aria-label={t("stammdaten.chargen.newBtn")}
          design={ButtonDesign.Transparent}
          icon="add"
          onClick={() => navigate(StammdatenRoutes.ChargeNew)}
        ></Button>
      </Toolbar>
      <Table
        busy={isRefetching || isLoading}
        busyDelay={0}
        noDataText={
          isLoading
            ? ""
            : isError
            ? t("stammdaten.tableUnexpectedError")
            : t("stammdaten.tableEmpty")
        }
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.chargen.id")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.chargen.name")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.chargen.produktName")}</Label>
            </TableColumn>
            <TableColumn>
              <Label style={{ display: "block", textAlign: "right" }}>
                {t("stammdaten.chargen.stueckzahl")}
              </Label>
            </TableColumn>
            <TableColumn>
              <Label style={{ display: "block", textAlign: "right" }}>
                {t("stammdaten.chargen.prioritaet")}
              </Label>
            </TableColumn>
            <TableColumn></TableColumn>
          </>
        }
      >
        {data &&
          data.filter(filter).map((charge) => (
            <TableRow
              key={charge.id}
              type={TableRowType.Active}
              onClick={() =>
                navigate(
                  getRoute(StammdatenRoutes.ChargeDetails, {
                    chargeId: charge.id,
                  })
                )
              }
              selected={chargeId === charge.id}
            >
              <TableCell style={{ minWidth: "10em" }}>{charge.id}</TableCell>
              <TableCell>{charge.name}</TableCell>
              <TableCell>{charge.produktbedarf?.[0]?.name}</TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {charge.produktbedarf[0]?.stueckzahl ?? 0}
              </TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {charge.prioritaet}
              </TableCell>

              <TableCell>
                <Icon name="feeder-arrow" />
              </TableCell>
            </TableRow>
          ))}
      </Table>
    </>
  );
};

export default ChargenListe;
