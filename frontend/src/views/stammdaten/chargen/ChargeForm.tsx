import {
  BreadcrumbsItem,
  Form,
  FormItem,
  Input,
  InputType,
  Label,
  Loader,
  ObjectPageSection,
  StepInput,
  Title,
} from "@ui5/webcomponents-react";
import { Formik, FormikHelpers } from "formik";
import { FC, ReactElement } from "react";
import { useTranslation } from "react-i18next";

import AppSelect from "../../../components/arbeitsschritt-select/AppSelect";
import FormBar from "../../../components/form-bar/FormBar";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import { useProdukte } from "../../../hooks/useProdukte";
import { Charge } from "../../../models/Charge";
import { useHandleFieldState } from "../../../util/errorUtil";
import { useChargenValidationSchema } from "./chargeValidationSchema";

interface ChargeFormProps {
  /**
   * Initiale Werte für das Form.
   */
  initialValues: Charge;
  /**
   * Funktion wenn das Form Submitted wird.
   */
  onSubmit: (values: Charge, helpers: FormikHelpers<Charge>) => void;
  /**
   * Funktion des Cancel-Buttons.
   */
  onCancel: () => void;
}

/**
 * Gemeinsame Basis für die {@link ChargeEdit} und {@link ChargeNew} Views.
 */
const ChargeForm: FC<ChargeFormProps> = ({
  initialValues,
  onSubmit,
  onCancel,
}) => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();

  const chargeValidationSchema = useChargenValidationSchema();

  const produktQuery = useProdukte();

  function sanitizeSubmit(values: Charge, helpers: FormikHelpers<Charge>) {
    const parsed = chargeValidationSchema.cast(values);

    const charge: Charge = {
      ...parsed,
      id: values.id,
    };

    return onSubmit(charge, helpers);
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={sanitizeSubmit}
      validationSchema={chargeValidationSchema}
      validateOnChange={false}
      validateOnBlur={false}
    >
      {({
        values,
        handleChange,
        handleSubmit,
        errors,
        setFieldValue,
        isSubmitting,
      }) => {
        // helper function für Textinputs. Kann leider nicht in eigene
        // Component ausgelagert werden, weil das ui5 verwirrt.
        const formItem = (
          name: keyof Charge,
          type: InputType,
          icon?: ReactElement
        ) => (
          <FormItem
            label={
              <Label id={`${name}Label`}>
                {t(`stammdaten.chargen.${name}`)}
              </Label>
            }
          >
            <Input
              name={name}
              aria-labelledby={`${name}Label`}
              role="textbox"
              type={type}
              icon={icon}
              onInput={handleChange}
              value={values[name] + ""}
              {...handleFieldState(errors[name] as string)}
            />
          </FormItem>
        );

        return (
          <StammdatenDetailsComponent
            header={
              <Title>
                <Input
                  name="name"
                  aria-label={t("stammdaten.chargen.name")}
                  role="textbox"
                  placeholder={t("stammdaten.chargen.name")}
                  type={InputType.Text}
                  onInput={handleChange}
                  value={values.name}
                  {...handleFieldState(errors.name)}
                />
              </Title>
            }
            breadcrumbs={<BreadcrumbsItem>Charge</BreadcrumbsItem>}
            footer={
              <>
                {isSubmitting && <Loader />}
                <FormBar
                  submitEnabled={!isSubmitting}
                  errors={errors}
                  errorPrefix="stammdaten.chargen"
                  onSubmit={handleSubmit}
                  onCancel={onCancel}
                />
              </>
            }
          >
            <ObjectPageSection
              id="general"
              titleText={t("stammdaten.chargen.sectionGeneral")}
              titleTextUppercase={false}
            >
              <Form
                onSubmit={() => handleSubmit()}
                columnsL={3}
                columnsXL={3}
                columnsM={3}
                columnsS={2}
                labelSpanXL={12}
                labelSpanL={12}
                labelSpanM={12}
              >
                {formItem("prioritaet", InputType.Number)}

                <FormItem
                  label={
                    <Label id={"produktLabel"}>
                      {t("stammdaten.chargen.produktName")}
                    </Label>
                  }
                >
                  <AppSelect
                    name={`produktbedarf.0.produktId`}
                    selected={values.produktbedarf[0]?.produktId ?? ""}
                    placeholder={t("stammdaten.chargen.produktPlaceholder")}
                    data={produktQuery.data! ?? []}
                    onChange={(p) => {
                      if (values.produktbedarf.length < 1) {
                        setFieldValue("produktbedarf.0", {
                          produktId: undefined,
                          stueckzahl: 0,
                        });
                      }

                      setFieldValue(
                        "produktbedarf.0.produktId",
                        p?.id ?? undefined
                      );
                    }}
                    {...handleFieldState(
                      (errors as any)?.produktbedarf?.[0]?.produktId
                    )}
                  />
                </FormItem>
                <FormItem
                  label={
                    <Label id={"produktLabel"}>
                      {t("stammdaten.chargen.stueckzahl")}
                    </Label>
                  }
                >
                  <StepInput
                    min={1}
                    max={Infinity}
                    accessibleName={t("stammdaten.chargen.stueckzahl")}
                    name={"produktbedarf.0.stueckzahl"}
                    value={values.produktbedarf?.[0]?.stueckzahl ?? 0}
                    onChange={(e) => {
                      if (values.produktbedarf.length < 1) {
                        setFieldValue("produktbedarf.0", {
                          produktId: undefined,
                          stueckzahl: 0,
                        });
                      }

                      handleChange(e);
                    }}
                  ></StepInput>
                </FormItem>
              </Form>
            </ObjectPageSection>
            <ObjectPageSection id={""}>
              <></>
            </ObjectPageSection>
          </StammdatenDetailsComponent>
        );
      }}
    </Formik>
  );
};

export default ChargeForm;
