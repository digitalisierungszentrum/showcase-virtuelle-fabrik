import { TFunction, useTranslation } from "react-i18next";
import { ObjectSchema, array, number, object, string } from "yup";

import { Produktbedarf } from "../../../models/Produktbedarf";

export const produktBedarfValidationSchema = (
  t: TFunction
): ObjectSchema<Produktbedarf> =>
  object({
    produktId: string().defined(
      t("stammdaten.chargen.validation.produktDefined")
    ),
    stueckzahl: number()
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.required")),
  });

/**
 * ValidationSchema für das {@link ChargeForm}.
 */
export const chargeValidationSchema = (t: TFunction) =>
  object({
    name: string().required(t("stammdaten.form.validation.required")),
    prioritaet: number().required(t("stammdaten.form.validation.number")),
    produktbedarf: array().of(produktBedarfValidationSchema(t)).default([]),
  });

export const useChargenValidationSchema = () => {
  const { t } = useTranslation();

  return chargeValidationSchema(t);
};
