import { FC } from "react";

import { useAddCharge } from "../../../hooks/useChargen";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Charge } from "../../../models/Charge";
import { StammdatenRoutes, getRoute } from "../../../routes/appRoutes";
import ChargeForm from "./ChargeForm";

/**
 * Ansicht zum Anlegen einer neuen Maschine.
 */
export const ChargeNew: FC = () => {
  const navigate = useNavigateWithParams();

  const add = useAddCharge();

  const initialValues: Charge = {
    id: "",
    name: "",
    prioritaet: 0,
    produktbedarf: [
      {
        produktId: "",
        stueckzahl: 0,
      },
    ],
  };

  function onSubmit(values: Charge) {
    add.mutateAsync(values).then(({ id }) =>
      navigate(
        getRoute("../" + StammdatenRoutes.ChargeDetails, {
          chargeId: id,
        })
      )
    );
  }

  return (
    <ChargeForm
      initialValues={initialValues}
      onSubmit={onSubmit}
      onCancel={() => navigate("..")}
    />
  );
};
