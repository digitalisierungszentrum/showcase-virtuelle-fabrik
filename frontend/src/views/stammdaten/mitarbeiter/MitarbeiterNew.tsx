import { FC } from "react";

import { useAddMitarbeiter } from "../../../hooks/useMitarbeiter";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Mitarbeiter } from "../../../models/Mitarbeiter";
import { StammdatenRoutes, getRoute } from "../../../routes/appRoutes";
import MitarbeiterForm from "./MitarbeiterForm";

/**
 * Ansicht zum Anlegen eines neuen Mitarbeiters.
 */
export const MitarbeiterNew: FC = () => {
  const navigate = useNavigateWithParams();

  const add = useAddMitarbeiter();

  const initialValues: Mitarbeiter = {
    id: "",
    name: "",
    befaehigungen: [],
    wechselzeit: 0,
  };

  function onSubmit(values: Mitarbeiter) {
    add.mutateAsync(values).then(({ id }) =>
      navigate(
        getRoute("../" + StammdatenRoutes.MitarbeiterDetails, {
          mitarbeiterId: id,
        })
      )
    );
  }

  return (
    <MitarbeiterForm
      initialValues={initialValues}
      onSubmit={onSubmit}
      onCancel={() => navigate("..")}
    />
  );
};
