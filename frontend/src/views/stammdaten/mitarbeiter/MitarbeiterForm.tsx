import {
  BreadcrumbsItem,
  Form,
  FormItem,
  Input,
  InputType,
  Label,
  Loader,
  ObjectPageSection,
  Title,
} from "@ui5/webcomponents-react";
import { Formik, FormikHelpers } from "formik";
import { FC, ReactElement } from "react";
import { useTranslation } from "react-i18next";

import FormBar from "../../../components/form-bar/FormBar";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import { Mitarbeiter } from "../../../models/Mitarbeiter";
import { useHandleFieldState } from "../../../util/errorUtil";
import MitarbeiterbefaehigungsField from "./MitarbeiterbefaehigungsField";
import { useMitarbeiterValidationSchema } from "./mitarbeiterValidationSchema";

interface MitarbeiterFormProps {
  /**
   * Initiale Werte für das Form.
   */
  initialValues: Mitarbeiter;
  /**
   * Funktion wenn das Form Submitted wird.
   */
  onSubmit: (values: Mitarbeiter, helpers: FormikHelpers<Mitarbeiter>) => void;
  /**
   * Funktion des Cancel-Buttons.
   */
  onCancel: () => void;
}

/**
 * Gemeinsame Basis für die {@link MitarbeiterEdit} und {@link MitarbeiterNew} Views.
 */
const MitarbeiterForm: FC<MitarbeiterFormProps> = ({
  initialValues,
  onSubmit,
  onCancel,
}) => {
  const { t } = useTranslation();
  const handleFieldState = useHandleFieldState();

  const { data: mitarbeiterValidationSchema } =
    useMitarbeiterValidationSchema();

  function sanitizeSubmit(
    values: Mitarbeiter,
    helpers: FormikHelpers<Mitarbeiter>
  ) {
    const parsed = mitarbeiterValidationSchema!.cast(values, {
      stripUnknown: true,
    });

    return onSubmit(parsed as Mitarbeiter, helpers);
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={sanitizeSubmit}
      validationSchema={mitarbeiterValidationSchema}
      validateOnChange={false}
      validateOnBlur={false}
    >
      {({ values, handleChange, handleSubmit, errors, isSubmitting }) => {
        // helper function für Textinputs. Kann leider nicht in eigene
        // Component ausgelagert werden, weil das ui5 verwirrt.
        const formItem = (
          name: keyof Mitarbeiter,
          type: InputType,
          icon?: ReactElement
        ) => (
          <FormItem
            label={
              <Label id={`${name}Label`}>
                {t(`stammdaten.mitarbeiter.${name}`)}
              </Label>
            }
          >
            <Input
              name={name}
              aria-labelledby={`${name}Label`}
              role="textbox"
              type={type}
              icon={icon}
              onInput={handleChange}
              value={values[name] + ""}
              {...handleFieldState(errors[name] as string)}
            />
          </FormItem>
        );

        return (
          <StammdatenDetailsComponent
            header={
              <Title>
                <Input
                  name="name"
                  aria-label={t("stammdaten.mitarbeiter.name")}
                  role="textbox"
                  placeholder={t("stammdaten.mitarbeiter.name")}
                  type={InputType.Text}
                  onInput={handleChange}
                  value={values.name}
                  {...handleFieldState(errors.name)}
                />
              </Title>
            }
            breadcrumbs={<BreadcrumbsItem>Mitarbeiter</BreadcrumbsItem>}
            footer={
              <>
                {isSubmitting && <Loader />}
                <FormBar
                  submitEnabled={!isSubmitting}
                  errors={errors}
                  errorPrefix="stammdaten.mitarbeiter"
                  onSubmit={handleSubmit}
                  onCancel={onCancel}
                />
              </>
            }
          >
            <ObjectPageSection
              id="general"
              titleText={t("stammdaten.mitarbeiter.sectionGeneral")}
              titleTextUppercase={false}
            >
              <Form
                onSubmit={() => handleSubmit()}
                columnsL={3}
                columnsXL={3}
                columnsM={3}
                columnsS={2}
                labelSpanXL={12}
                labelSpanL={12}
                labelSpanM={12}
              >
                {formItem(
                  "wechselzeit",
                  InputType.Number,
                  <span style={{ marginRight: "1em" }}>Sekunden</span>
                )}
              </Form>
            </ObjectPageSection>
            <ObjectPageSection
              id="befaehigungen"
              titleText={t("stammdaten.mitarbeiter.sectionBefaehigungen")}
              titleTextUppercase={false}
            >
              <MitarbeiterbefaehigungsField />
            </ObjectPageSection>
          </StammdatenDetailsComponent>
        );
      }}
    </Formik>
  );
};

export default MitarbeiterForm;
