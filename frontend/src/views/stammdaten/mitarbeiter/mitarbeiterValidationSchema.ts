import { TFunction, useTranslation } from "react-i18next";
import {
  ObjectSchema,
  ValidationError,
  array,
  number,
  object,
  string,
} from "yup";

import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import { Mitarbeiter } from "../../../models/Mitarbeiter";
import { Mitarbeiterbefaehigung } from "../../../models/Mitarbeiterbefaehigung";
import { NewMitarbeiter } from "../../../models/NewMitarbeiter";

export const mitarbeiterBefaehigungValidationSchema: (
  t: TFunction
) => ObjectSchema<Mitarbeiterbefaehigung> = (t) =>
  object({
    schrittId: string().defined(),
    taktrate: number()
      .integer(t("stammdaten.form.validation.integer"))
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.required")),
  });

/**
 * ValidationSchema für das {@link MitarbeiterForm}.
 */
export const mitarbeiterValidationSchema: (
  arbeitsschritte: Arbeitsschritt[],
  t: TFunction
) => ObjectSchema<NewMitarbeiter | Mitarbeiter> = (arbeitsschritte, t) =>
  object({
    id: string().transform((value) => (value === "" ? undefined : value)),
    name: string().required(t("stammdaten.form.validation.required")),

    wechselzeit: number()
      .integer(t("stammdaten.form.validation.integer"))
      .min(0, t("stammdaten.form.validation.notNegative"))
      .required(t("stammdaten.form.validation.number")),

    befaehigungen: array()
      .of(mitarbeiterBefaehigungValidationSchema(t))
      // Testet ob der gleiche Arbeitsschritt in mehreren Befähigungen vorkommt
      .test((befaehigungen, context) => {
        if (!befaehigungen) {
          return true;
        }
        const createError = (i: number, name?: string) =>
          context.createError({
            message: t("stammdaten.mitarbeiter.validation.befaehigungUnique", {
              name,
            }),
            path: `befaehigungen.${i}.schrittId`,
          });

        const errors: ValidationError[] = [];
        // map aller ArbeitsschrittIDs
        const schrittMap: { [id: string]: number } = {};

        befaehigungen.forEach(({ schrittId }, index) => {
          // schaue für jede befähigung, ob schon eine andere befähigung mit
          // der gleichen SchrittId existiert
          if (schrittMap[schrittId] !== undefined) {
            // falls ja, erzeuge error für beide befähigungen
            const name = arbeitsschritte.find((s) => s.id === schrittId)?.name;
            errors.push(createError(schrittMap[schrittId], name));
            errors.push(createError(index, name));
          }

          // füge eintrag zur schrittMap hinzu
          schrittMap[schrittId] = index;
        });

        if (errors.length > 0) {
          return new ValidationError(errors);
        }

        return true;
      })
      .default([]),
  });

/**
 * Stellt das ValidationSchema für Mitarbeiter zur verfügung.
 * Da das ValidationSchema von Arbeitsschritten abhängig ist, wird das Ergebnis
 * in ein QueryResult gewrappt.
 *
 * @returns QueryResult mit MitarbeiterValidationSchema als data.
 */
export const useMitarbeiterValidationSchema = () => {
  const arbeitsschritte = useArbeitsschritte();
  const { t } = useTranslation();

  return {
    ...arbeitsschritte,
    data: arbeitsschritte.data
      ? mitarbeiterValidationSchema(arbeitsschritte.data, t)
      : undefined,
  };
};
