import { ValidationError } from "yup";

import {
  testArbeitsschritte,
  testMitarbeiter1,
  testMitarbeiter2,
  testMitarbeiter3,
} from "../../../util/testData";
import { mitarbeiterValidationSchema } from "./mitarbeiterValidationSchema";

describe("Mitarbeiter Validation tests", () => {
  const validationSchema = mitarbeiterValidationSchema(
    testArbeitsschritte,
    () => {}
  );

  test("Erkennt korrekte Objekte", async () => {
    expect(await validationSchema.isValid(testMitarbeiter1)).toBeTruthy();
    expect(await validationSchema.isValid(testMitarbeiter2)).toBeTruthy();
    expect(await validationSchema.isValid(testMitarbeiter3)).toBeTruthy();
  });

  test("Erkennt fehlende Attribute", async () => {
    expect(() =>
      validationSchema.validateSync({
        id: "test",
      })
    ).toThrow(ValidationError);
  });

  test("Strings in Number Feldern", async () => {
    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        wechselzeit: "bad",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: "bad",
          },
        ],
      })
    ).toBeFalsy();
  });

  test("Edgecases Number", async () => {
    expect(
      await validationSchema.isValid(
        {
          ...testMitarbeiter1,
          wechselzeit: 0,
        },
      )
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        wechselzeit: 1,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        wechselzeit: -1,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        wechselzeit: Number.MAX_VALUE,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        wechselzeit: Number.MIN_VALUE,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        wechselzeit: 0.5,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: 1,
          },
        ],
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: 0,
          },
        ],
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: -1,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: 0.5,
          },
        ],
      })
    ).toBeFalsy();
  });

  test("erkennt undefined", async () => {
    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        id: undefined,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        name: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        wechselzeit: undefined,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: undefined,
            taktrate: 1,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: undefined,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [{}],
      })
    ).toBeFalsy();
  });

  test("erkennt doppelte arbeitsschritte", async () => {
    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: 1,
          },
          {
            schrittId: "a2",
            taktrate: 2,
          },
        ],
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: 1,
          },
          {
            schrittId: "a1",
            taktrate: 2,
          },
        ],
      })
    ).toBeFalsy();
  });

  test("erkennt ob die Wechselzahl und die Taktrate ganzzahlig oder Kommazahl ist", async () => {
    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,

        wechselzeit: 0,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        wechselzeit: 0.5,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        taktrate: 0,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            wechselzeit: 2.5,
            taktrate: 0.5,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            taktrate: -0.5,
            wechselzeit: -10.4,
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: "Zahl",
          },
        ],
      })
    ).toBeFalsy();
  });

  test("erkennt ob das Feld Name im Mitarbeiterformular leer ist oder nicht", async () => {
    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        name: "Mustermann",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        name: "",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        name: 45,
      })
    ).toBeTruthy();
  });

  test("erkennt ob zwei Mitarbeiter in einem Formular den gleichen Name gespeichert werden können", async () => {
    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        name: "Mustermann",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        ...testMitarbeiter2,

        name: "Maier",
      })
    ).toBeTruthy();
  });

  test("erkennt ob ein Objekt Mitarbeiter dem Formular zugeordnet wurde", async () => {
    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        id: 4487,
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        id: 4407,
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        name: "Mustermann",
      })
    ).toBeTruthy();

    expect(
      await validationSchema.isValid({
        name: "Maier",
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        befaehigungen: [
          {
            schrittId: "a1",
            taktrate: "Zahl",
          },
        ],
      })
    ).toBeFalsy();

    expect(
      await validationSchema.isValid({
        wechselzeit: 4,
      })
    ).toBeFalsy();
  });

  test("erkennt ob Arbeitschritte leer ist", async () => {
    expect(
      await validationSchema.isValid({
        ...testMitarbeiter1,
        testArbeitsschritte: [],
      })
    ).toBeTruthy();
  });
});

export {};
