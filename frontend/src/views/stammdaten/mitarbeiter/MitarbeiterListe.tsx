import {
  Button,
  ButtonDesign,
  Icon,
  Input,
  Label,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  TableRowType,
  Title,
  Toolbar,
  ToolbarSpacer,
} from "@ui5/webcomponents-react";
import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { useAllMitarbeiter } from "../../../hooks/useMitarbeiter";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Mitarbeiter } from "../../../models/Mitarbeiter";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";

/**
 * Liste aller Mitarbeiter in der Stammdatenübersicht.
 */
const MitarbeiterListe: FC = () => {
  const { mitarbeiterId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();
  const { t } = useTranslation();

  const {
    data: mitarbeiter,
    isLoading,
    isError,
    isRefetching,
  } = useAllMitarbeiter();
  const [filterStr, setFilterStr] = useState("");

  function filter(m: Mitarbeiter): boolean {
    return (
      filterStr === "" ||
      Object.values(m).find((v) =>
        String(v).toLowerCase().includes(filterStr.toLowerCase())
      )
    );
  }

  return (
    <>
      <Toolbar>
        <Title>{t("stammdaten.mitarbeiter.title")}</Title>
        <ToolbarSpacer />
        <Input
          icon={<Icon name="search" />}
          onInput={(e) => setFilterStr(e.target.value ?? "")}
        />
        <Button
          design={ButtonDesign.Transparent}
          role="button"
          aria-label={t("stammdaten.mitarbeiter.newBtn")}
          icon="add"
          onClick={() => navigate(StammdatenRoutes.MitarbeiterNew)}
        ></Button>
      </Toolbar>
      <Table
        busy={isRefetching || isLoading}
        busyDelay={0}
        noDataText={
          isLoading
            ? ""
            : isError
            ? t("stammdaten.tableUnexpectedError")
            : t("stammdaten.tableEmpty")
        }
        columns={
          <>
            <TableColumn>
              <Label>{t("stammdaten.mitarbeiter.id")}</Label>
            </TableColumn>
            <TableColumn>
              <Label>{t("stammdaten.mitarbeiter.name")}</Label>
            </TableColumn>
            <TableColumn>
              <Label style={{ display: "block", textAlign: "right" }}>
                {t("stammdaten.mitarbeiter.wechselzeitSekunden")}
              </Label>
            </TableColumn>
            <TableColumn></TableColumn>
          </>
        }
      >
        {mitarbeiter &&
          mitarbeiter.filter(filter).map((mitarbeit) => (
            <TableRow
              key={mitarbeit.id}
              type={TableRowType.Active}
              onClick={() =>
                navigate(
                  getRoute(StammdatenRoutes.MitarbeiterDetails, {
                    mitarbeiterId: mitarbeit.id,
                  })
                )
              }
              selected={mitarbeiterId === mitarbeit.id}
            >
              <TableCell style={{ minWidth: "10em" }}>{mitarbeit.id}</TableCell>
              <TableCell>{mitarbeit.name}</TableCell>
              <TableCell style={{ textAlign: "right" }}>
                {mitarbeit.wechselzeit}
              </TableCell>
              <TableCell>
                <Icon name="feeder-arrow" />
              </TableCell>
            </TableRow>
          ))}
      </Table>
    </>
  );
};

export default MitarbeiterListe;
