import { FC } from "react";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import {
  useEditMitarbeiter,
  useMitarbeiter,
} from "../../../hooks/useMitarbeiter";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { Mitarbeiter } from "../../../models/Mitarbeiter";
import {
  AppParams,
  StammdatenRoutes,
  getRoute,
} from "../../../routes/appRoutes";
import MitarbeiterForm from "./MitarbeiterForm";

/**
 * Editieransicht für einen bestehenden Mitarbeiter.
 * Der zu editierende Mitarbeiter wird den Route-Params entnommen.
 */
export const MitarbeiterEdit: FC = () => {
  const { mitarbeiterId } = useParams<AppParams>();
  const navigate = useNavigateWithParams();

  const {
    data: mitarbeiter,
    isLoading,
    isError,
    error,
  } = useMitarbeiter(mitarbeiterId!);
  const edit = useEditMitarbeiter();

  function onSubmit(values: Mitarbeiter) {
    edit.mutateAsync(values).then(() =>
      navigate(
        getRoute("../" + StammdatenRoutes.MitarbeiterDetails, {
          mitarbeiterId: mitarbeiterId,
        })
      )
    );
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (!mitarbeiter || isError) {
    return <UnexpectedErrorPage />;
  }

  return (
    <MitarbeiterForm
      initialValues={mitarbeiter!}
      onSubmit={onSubmit}
      onCancel={() =>
        navigate(
          "../" +
            getRoute(StammdatenRoutes.MitarbeiterDetails, { mitarbeiterId })
        )
      }
    />
  );
};
