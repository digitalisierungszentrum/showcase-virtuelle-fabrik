import {
  BreadcrumbsItem,
  FlexBox,
  FlexBoxAlignItems,
  FlexBoxDirection,
  FlexBoxJustifyContent,
  Form,
  FormItem,
  Label,
  Loader,
  ObjectPageSection,
  Table,
  TableCell,
  TableColumn,
  TableRow,
  Text,
  Title,
} from "@ui5/webcomponents-react";
import { FC } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import LoadingPage from "../../../components/loading-page/LoadingPage";
import NotFoundPage from "../../../components/not-found-page/NotFoundPage";
import StammdatenDetailsComponent from "../../../components/stammdaten-details-component/StammdatenDetailsComponent";
import UnexpectedErrorPage from "../../../components/unexpected-error-page/UnexpectedErrorPage";
import { useArbeitsschritte } from "../../../hooks/useArbeitsschritte";
import {
  useDeleteMitarbeiter,
  useMitarbeiter,
} from "../../../hooks/useMitarbeiter";
import { useNavigateWithParams } from "../../../hooks/useNavigateWithParams";
import { AppParams } from "../../../routes/appRoutes";
import { secondsToTimeString } from "../../../util/misc";

/**
 *  Es handelt sich hierbei um die Detailansicht eines Mitarbeiters.
 *  Die anzuzeigende Mitarbeiter wird in den Route-Params entnommen.
 */
const MitarbeiterDetails: FC = () => {
  const navigate = useNavigateWithParams();
  const { mitarbeiterId } = useParams<AppParams>();
  const { t } = useTranslation();

  const {
    data: mitarbeiter,
    isLoading,
    isError,
    error,
  } = useMitarbeiter(mitarbeiterId!);
  const arbeitsschrittQuery = useArbeitsschritte();

  const deleteMutation = useDeleteMitarbeiter();

  async function onDelete() {
    await deleteMutation.mutateAsync({ id: mitarbeiter!.id });

    navigate("..");
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  if (isError && error.response?.status === 404) {
    return <NotFoundPage />;
  }

  if (isError || !mitarbeiter) {
    return <UnexpectedErrorPage />;
  }

  return (
    <StammdatenDetailsComponent
      breadcrumbs={
        <BreadcrumbsItem>{t("stammdaten.mitarbeiter.title")}</BreadcrumbsItem>
      }
      header={<Title wrappingType="Normal">{mitarbeiter.name}</Title>}
      headerContent={
        <FlexBox
          direction={FlexBoxDirection.Column}
          alignItems={FlexBoxAlignItems.Start}
          justifyContent={FlexBoxJustifyContent.Center}
          style={{ marginLeft: "1em" }}
        >
          <Label>{t("stammdaten.mitarbeiter.id")}</Label>
          <Text>{mitarbeiter.id}</Text>
        </FlexBox>
      }
      withActions
      onEdit={() => navigate("edit")}
      onDelete={onDelete}
      deleteConfirmationTitle={t(
        "stammdaten.mitarbeiter.deleteConfirmationTitle"
      )}
      deleteConfirmationBody={t(
        "stammdaten.mitarbeiter.deleteConfirmationBody",
        {
          name: mitarbeiter.name,
        }
      )}
      footer={
        <div style={{ margin: "0 -0.5em" }}>
          {deleteMutation.isLoading && <Loader />}
        </div>
      }
    >
      <ObjectPageSection
        id="general"
        titleText={t("stammdaten.mitarbeiter.sectionGeneral")}
        titleTextUppercase={false}
      >
        <Form
          columnsL={3}
          columnsXL={3}
          columnsM={3}
          columnsS={2}
          labelSpanXL={12}
          labelSpanL={12}
          labelSpanM={12}
        >
          <FormItem label="Wechselzeit">
            <Text>{mitarbeiter.wechselzeit}</Text>
          </FormItem>
        </Form>
      </ObjectPageSection>
      <ObjectPageSection
        id="befaehigungen"
        titleText={t("stammdaten.mitarbeiter.sectionBefaehigungen")}
        titleTextUppercase={false}
      >
        <Table
          busy={arbeitsschrittQuery.isLoading}
          noDataText={t("stammdaten.mitarbeiter.noBefaehigungen")}
          style={{
            maxWidth: "20em",
          }}
          columns={
            <>
              <TableColumn>
                <Label>{t("stammdaten.mitarbeiter.befaehigungName")}</Label>
              </TableColumn>
              <TableColumn
                style={{
                  textAlign: "end",
                  paddingRight: "1em",
                }}
              >
                <Label
                  style={{
                    width: "calc(100% - .5em)",
                    textAlign: "end",
                    marginRight: "1em",
                  }}
                >
                  {t("stammdaten.mitarbeiter.befaehigungTaktrate")}
                </Label>
              </TableColumn>
            </>
          }
        >
          {mitarbeiter.befaehigungen.map(({ schrittId, taktrate }) => (
            <TableRow key={schrittId}>
              <TableCell>
                {arbeitsschrittQuery.data?.find(
                  (schritt) => schritt.id === schrittId
                )?.name || ""}
              </TableCell>
              <TableCell
                style={{
                  textAlign: "end",
                  paddingRight: "1em",
                }}
              >
                {secondsToTimeString(taktrate)}
              </TableCell>
            </TableRow>
          ))}
        </Table>
      </ObjectPageSection>
    </StammdatenDetailsComponent>
  );
};

export default MitarbeiterDetails;
