// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import matchers from "@testing-library/jest-dom/matchers";
import "@ui5/webcomponents-react/dist/Assets";
import "@ui5/webcomponents/dist/features/InputElementsFormSupport";
import ResizeObserverPolyfill from "resize-observer-polyfill";
import "whatwg-fetch";

import "./util/icons";

expect.extend(matchers);

// polyfill für UI5-Webcom,ponents, see: https://sap.github.io/ui5-webcomponents-react/?path=/docs/knowledge-base--page
const setupMatchMedia = () => {
  Object.defineProperty(window, "matchMedia", {
    writable: true,
    value: vi.fn().mockImplementation((query) => {
      const maxWidth = parseInt(
        /max-width:(?<maxWidth>\d+)px/.exec(query)?.groups?.maxWidth ?? "0"
      );
      const minWidth = parseInt(
        /min-width:(?<minWidth>\d+)px/.exec(query)?.groups?.minWidth ?? "0"
      );

      let matches =
        (minWidth ? minWidth <= window.innerWidth : true) &&
        (maxWidth ? window.innerWidth <= maxWidth : true);

      if (query === "(orientation: landscape)") {
        matches = window.innerWidth > window.innerHeight;
      }

      return {
        matches,
        media: query,
        onchange: undefined,
        addListener: vi.fn(), // deprecated
        removeListener: vi.fn(), // deprecated
        addEventListener: vi.fn(),
        removeEventListener: vi.fn(),
        dispatchEvent: vi.fn(),
      };
    }),
  });
};

beforeEach(() => {
  if (!Object.hasOwn(globalThis, "crypto")) {
    Object.defineProperty(globalThis, "crypto", {
      value: {
        randomUUID: () => "1",
      },
    });
  }
  setupMatchMedia();
  window.ResizeObserver = ResizeObserverPolyfill;

  const mockIntersectionObserver = vi.fn();
  mockIntersectionObserver.mockReturnValue({
    observe: () => null,
    unobserve: () => null,
    disconnect: () => null,
  });
  globalThis.IntersectionObserver = mockIntersectionObserver;
});

// error und warn log überschreiben, um nervige Fehlermeldungen zu filtern
const consoleError = console.error;
console.error = (message, ...args) => {
  if (
    typeof message === "string" &&
    message.includes("Error: Could not parse CSS stylesheet")
  ) {
    return;
  }
  if (message?.message?.includes("Could not parse CSS stylesheet")) {
    return;
  }

  consoleError(message, ...args);
};

const consoleWarn = console.warn;
console.warn = (message, ...args) => {
  if (
    typeof message === "string" &&
    message.includes("ui5-webcomponents-react Deprecation Notice")
  ) {
    return;
  }

  consoleWarn(message, ...args);
};

// setup i18next mock
vi.mock("react-i18next", () => ({
  useTranslation: () => ({
    t: (str: string) => str,
  }),
}));

// setup Api mock
vi.mock("./api/axios", () => ({
  Axios: {
    get: vi.fn(),
    post: vi.fn(),
    put: vi.fn(),
    delete: vi.fn(),
    patch: vi.fn(),
  },
}));

vi.mock("@ui5/webcomponents-react-base", async () => {
  const actual = (await vi.importActual(
    "@ui5/webcomponents-react-base"
  )) as any;
  return {
    ...actual,
    useI18nBundle: () => ({ getText: () => null }),
  };
});
