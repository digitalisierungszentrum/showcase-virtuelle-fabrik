import { Maschine } from "../../../models/Maschine";
import { NewMaschine } from "../../../models/NewMaschine";
import { MaschinenApiFactory } from "../../../openapi";
import { ApiConfiguration } from "../../ApiConfiguration";

/**
 * Stellt Requests für Maschinen zu verfügung.
 * @param sid ID des Szenarios für welches die Requests
 *            ausgeführt werden sollen.
 */
export const maschinenRequests = (sid: string) => {
  const api = MaschinenApiFactory(ApiConfiguration);

  return {
    /**
     * Erfragt alle Maschinen des Szenarios.
     */
    getAllMaschinen: () => api.getAllMaschinen(sid),
    /**
     * Erstellt die gegebene Maschine.
     * @param maschine Maschine die erstellt werden soll.
     * @return Promise mit der ID für die neu erstellte Maschine.
     */
    postMaschine: (maschine: NewMaschine) => api.postMaschine(sid, maschine),
    /**
     * Erfragt die Maschine mit der gegebenen Id.
     * @param mid Id der gesuchten Maschine.
     */
    getMaschine: (mid: string) => api.getMaschine(sid, mid),
    /**
     * Aktualisiert die Maschine mit der gegebenen ID.
     * @param mid ID der zu aktualisierenden Maschine.
     * @param maschine Aktualisierte Maschine.
     */
    putMaschine: (mid: string, maschine: Maschine) =>
      api.putMaschine(sid, mid, maschine),
    /**
     * Löscht die Maschine mit der gegebenen ID.
     * @param mid ID der zu löschenden Maschine.
     */
    deleteMaschine: (mid: string) => api.deleteMaschine(sid, mid),
  };
};
