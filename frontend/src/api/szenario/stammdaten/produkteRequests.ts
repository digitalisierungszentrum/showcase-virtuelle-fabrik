import { NewProdukt } from "../../../models/NewProdukt";
import { Produkt } from "../../../models/Produkt";
import { ProdukteApiFactory } from "../../../openapi";
import { ApiConfiguration } from "../../ApiConfiguration";

/**
 * Routes der ProduktRequests.
 */
const routes = {
  allProdukte: (sid: string) => `/szenarios/${sid}/produkte/`,
  produkt: (sid: string, mid: string) => `/szenarios/${sid}/produkte/${mid}`,
};

export const produkteRoutes = routes;

/**
 * Stellt Requests für Produkte zu verfügung.
 * @param sid ID des Szenarios für welches die Requests
 *            ausgeführt werden sollen.
 */
export const produkteRequests = (sid: string) => {
  const api = ProdukteApiFactory(ApiConfiguration);

  return {
    /**
     * Erfragt alle Produkte des Szenarios.
     */
    getAllProdukte: () => api.getAllProdukte(sid),
    /**
     * Erstellt das gegebene Produkt.
     * @param produkt Zu erstellendes Produkt.
     * @return Promise mit der ID des neu erstellten Produkts.
     */
    postProdukt: (produkt: NewProdukt) => api.postProdukt(sid, produkt),
    /**
     * Erfragt das Produkt mit der gegebenen ID.
     * @param pid ID des gesuchten Produkts.
     */
    getProdukt: (pid: string) => api.getProdukt(sid, pid),
    /**
     * Aktualisiert das Produkt mit der gegebenen ID.
     * @param pid ID des zu aktualisierende Produkt
     * @param produkt Aktualisiertes Produkt.
     */
    putProdukt: (pid: string, produkt: Produkt) =>
      api.putProdukt(sid, pid, produkt),
    /**
     * Löscht das Produkt mit der gegebenen ID.
     * @param pid ID des zu löschenden Produkts.
     */
    deleteProdukt: (pid: string) => api.deleteProdukt(sid, pid),
  };
};
