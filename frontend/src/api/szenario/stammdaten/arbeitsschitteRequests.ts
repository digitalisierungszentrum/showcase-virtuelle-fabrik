import { Arbeitsschritt } from "../../../models/Arbeitsschritt";
import { NewArbeitsschritt } from "../../../models/NewArbeitsschritt";
import { ArbeitsschritteApiFactory } from "../../../openapi";
import { ApiConfiguration } from "../../ApiConfiguration";

/**
 * Stellt Requests für Arbeitsschritte zu verfügung.
 * @param sid ID des Szenarios für welches die Requests
 *            ausgeführt werden sollen.
 */
export const arbeitsschritteRequests = (sid: string) => {
  const api = ArbeitsschritteApiFactory(ApiConfiguration);

  return {
    /**
     * Erfragt alle Arbeitsschritte des Szenarios.
     */
    getAllArbeitsschritte: () => api.getAllArbeitsschritte(sid),
    /**
     * Erstellt den gegebenen Arbeitsschritt.
     * @param arbeitsschritt Arbeitsschritt der erstellt werden soll.
     * @return Promise mit der ID des neu erstellten Arbeitsschritts.
     */
    postArbeitsschritt: (arbeitsschritt: NewArbeitsschritt) =>
      api.postArbeitsschritt(sid, arbeitsschritt),
    /**
     * Erfragt den Arbeitsschritt mit der gegebenen ID.
     * @param aid ID des gesuchten Arbeitsschritts.
     */
    getArbeitsschritt: (aid: string) => api.getArbeitsschritt(sid, aid),
    /**
     * Aktualisiert den Arbeitsschritt mit der gegebenen ID.
     * @param aid ID des zu ändernden Arbeitsschritts.
     * @param arbeitsschritt Aktualisierter Arbeitsschritt.
     */
    putArbeitsschritt: (aid: string, arbeitsschritt: Arbeitsschritt) =>
      api.putArbeitsschritt(sid, aid, arbeitsschritt),
    /**
     * Löscht den Arbeitsschritt mit der gegebenen Id.
     * @param aid Id des zu löschenden Arbeitsschritts.
     */
    deleteArbeitsschritt: (aid: string) => api.deleteArbeitsschritt(sid, aid),
  };
};
