import { Charge } from "../../../models/Charge";
import { ChargenApiFactory, NewCharge } from "../../../openapi";
import { ApiConfiguration } from "../../ApiConfiguration";

/**
 * Stellt Requests für Chargen zu verfügung.
 * @param sid ID des Szenarios für welches die Requests
 *            ausgeführt werden sollen.
 */
export const chargenRequests = (sid: string) => {
  const api = ChargenApiFactory(ApiConfiguration);
  return {
    /**
     * Erfragt alle Chargen des Szenarios.
     */
    getAllChargen: () => api.getAllChargen(sid),
    /**
     * Erstellt die gegebene Charge.
     * @param charge Charge die erstellt werden soll.
     * @return Promise mit der ID der neu erstellten Charge.
     */
    postCharge: (charge: NewCharge) => api.postCharge(sid, charge),
    /**
     * Erfragt die Charge mit der gegebenen Id.
     * @param cid ID der gesuchten Charge.
     */
    getCharge: (cid: string) => api.getCharge(sid, cid),
    /**
     * Aktualisiert die Charge mit der gegebenen ID.
     * @param cid ID der zu aktualisierenden Charge.
     * @param charge Aktualisierte Charge.
     */
    putCharge: (cid: string, charge: Charge) => api.putCharge(sid, cid, charge),
    /**
     * Löscht die Charge mit der gegebenen ID.
     * @param cid ID der zu löschenden Charge.
     */
    deleteCharge: (cid: string) => api.deleteCharge(sid, cid),
  };
};
