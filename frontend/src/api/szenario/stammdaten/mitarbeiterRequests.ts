import { Mitarbeiter } from "../../../models/Mitarbeiter";
import { MitarbeiterApiFactory } from "../../../openapi";
import { ApiConfiguration } from "../../ApiConfiguration";

/**
 * Stellt Requests für Mitarbeiter zu verfügung.
 * @param sid ID des Szenarios für welches die Requests
 *            ausgeführt werden sollen.
 */
export const mitarbeiterRequests = (sid: string) => {
  const api = MitarbeiterApiFactory(ApiConfiguration);

  return {
    /**
     * Erfragt alle Mitarbeiter des Szenarios.
     */
    getAllMitarbeiter: () => api.getAllMitarbeiter(sid),
    /**
     * Erstellt den gegebenen Mitarbeiter.
     * @param mitarbeiter Mitarbeiter der erstellt werden soll.
     * @return Promise mit der ID des neu erstellten Mitarbeiters.
     */
    postMitarbeiter: (mitarbeiter: Mitarbeiter) =>
      api.postMitarbeiter(sid, mitarbeiter),
    /**
     * Erfragt den Mitarbeiter mit der gegebenen Id.
     * @param mid ID des gesuchten Mitarbeiters.
     */
    getMitarbeiter: (mid: string) => api.getMitarbeiter(sid, mid),
    /**
     * Aktualisiert den Mitarbeiter mit der gegebenen ID.
     * @param mid ID des zu aktualisierenden Mitarbeiters.
     * @param mitarbeiter Aktualisierter Mitarbeiter.
     */
    putMitarbeiter: (mid: string, mitarbeiter: Mitarbeiter) =>
      api.putMitarbeiter(sid, mid, mitarbeiter),
    /**
     * Löscht den Mitarbeiter mit der gegebenen ID
     * @param mid ID des zu löschenden Mitarbeiters.
     */
    deleteMitarbeiter: (mid: string) => api.deleteMitarbeiter(sid, mid),
  };
};
