import { Material } from "../../../models/Material";
import { NewMaterial } from "../../../models/NewMaterial";
import { MaterialienApiFactory } from "../../../openapi";
import { ApiConfiguration } from "../../ApiConfiguration";

/**
 * Stellt Requests für Materialien zu verfügung.
 * @param sid ID des Szenarios für welches die Requests
 *            ausgeführt werden sollen.
 */
export const materialienRequests = (sid: string) => {
  const api = MaterialienApiFactory(ApiConfiguration);

  return {
    /**
     * Erfragt alle Materialien des Szenarios
     */
    getAllMaterialien: () => api.getAllMaterialien(sid),
    /**
     * Erstellt das gegebene Material.
     * @param material Material das erstellt werden soll.
     * @return Promise mit der ID des neu erstellten Materials.
     */
    postMaterial: (material: NewMaterial) => api.postMaterial(sid, material),
    /**
     * Erfragt das Material mit der gegebenen ID.
     * @param mid ID des gesuchten Materials.
     */
    getMaterial: (mid: string) => api.getMaterial(sid, mid),
    /**
     * Aktualisiert das Material mit der gegebenen ID.
     * @param mid Id des zu aktualisierenden Materials.
     * @param material Aktualisiertes Material.
     */
    putMaterial: (mid: string, material: Material) =>
      api.putMaterial(sid, mid, material),
    /**
     * Löscht das Material mit der gegebenen Id.
     * @param mid ID des zu löschenden Materials.
     */
    deleteMaterial: (mid: string) => api.deleteMaterial(sid, mid),
  };
};
