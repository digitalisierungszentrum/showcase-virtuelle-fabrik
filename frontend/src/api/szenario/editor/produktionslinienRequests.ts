import { Socket } from "socket.io-client";

import { EditorNotification } from "../../../models/EditorNotification";
import { ProduktionslinieDto } from "../../../models/ProduktionslinieDto";
import { OptimierungenApiFactory, Optimierung } from "../../../openapi";
import { ApiConfiguration } from "../../ApiConfiguration";
import { websocket } from "../../websocket";

/**
 * Routes der ProduktionslinienRequests.
 */
const routes = {
  produktionslinie: (sid: string) => `/szenarios/${sid}/editor/`,
};

export const produktionslinienRoutes = routes;

/**
 * Socket.IO-Events, die vom Server an den Client gesendet werden können.
 */
interface ServerToClientEvents {
  produktionslinieChanged: (produktionslinie: ProduktionslinieDto) => void;
  simulationRunning: (id: string) => void;
  simulationFinished: (id: string, notificationDetails: Partial<EditorNotification>, optimizationResult: Optimierung) => void;
  simulationCanceled: (id: string) => void;
  simulationInfeasible: (id: string) => void;
  incompleteData: (id: string, details: string) => void;
}

/**
 * Socket.IO-Events, die vom Client an den Server gesendet werden können.
 */
interface ClientToServerEvents {
  changeProduktionslinie: (produktionslinie: ProduktionslinieDto) => void;
  validateProduktionslinie: (
    produktionslinie: ProduktionslinieDto,
    callback: (notifications: EditorNotification[]) => void
  ) => void;
  startSimulation: (produktionslinie: ProduktionslinieDto) => void;
  cancelSimulation: () => void;
}

/**
 * Typ der Produktionslinien-Socket-Verbindung.
 */
export type EditorSocket = Socket<ServerToClientEvents, ClientToServerEvents>;

/**
 * Stellt Requests für Produktionslinien zu verfügung.
 * @param sid ID des Szenarios für welches die Requests
 *            ausgeführt werden sollen.
 */
export const produktionslinienRequests = (sid: string) => {
  const api = OptimierungenApiFactory(ApiConfiguration);

  return {
    /**
     * Stellt einen neuen Socket für die Produktionslinien des Szenarios
     * zu verfügung.
     */
    socket: (): EditorSocket =>
      websocket(routes.produktionslinie(sid), { autoConnect: false }),

    getAllOptimierungen: () => api.getAllOptimierungen(sid),
  };
};
