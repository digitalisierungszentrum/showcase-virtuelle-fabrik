import { useMemo } from "react";
import { useParams } from "react-router-dom";

import { AppParams } from "../../routes/appRoutes";
import { produktionslinienRequests } from "./editor/produktionslinienRequests";
import { arbeitsschritteRequests } from "./stammdaten/arbeitsschitteRequests";
import { chargenRequests } from "./stammdaten/chargenRequests";
import { maschinenRequests } from "./stammdaten/maschinenRequests";
import { materialienRequests } from "./stammdaten/materialienRequests";
import { mitarbeiterRequests } from "./stammdaten/mitarbeiterRequests";
import { produkteRequests } from "./stammdaten/produkteRequests";


/**
 * Stellt eine API mit Szenario spezifischen Requests zur verfügung.
 *
 * Diese Api bezieht das Szenario, für welches die Requests ausgeführt werden
 * sollen aus den Router Params, dementsprechend darf diese funktion nicht
 * außerhalb einer Szenario-Route verwendet werden.
 */
export const useSzenarioApi = () => {
  const { szenarioId } = useParams<AppParams>();

  if (!szenarioId) {
    throw Error(
      "SzenarioId ist nicht gesetzt! " +
        "Wurde SzenarioApi außerhalb eines Szenarios eingebunden?"
    );
  }

  return useMemo(
    () => ({
      ...arbeitsschritteRequests(szenarioId),
      ...chargenRequests(szenarioId),
      ...maschinenRequests(szenarioId),
      ...materialienRequests(szenarioId),
      ...mitarbeiterRequests(szenarioId),
      ...produkteRequests(szenarioId),

      ...produktionslinienRequests(szenarioId),
    }),
    [szenarioId]
  );
};

/**
 * Typ der SzenarioApi.
 */
export type SzenarioApi = ReturnType<typeof useSzenarioApi>;