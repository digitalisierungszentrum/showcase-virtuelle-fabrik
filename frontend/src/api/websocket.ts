import { ManagerOptions, SocketOptions, io } from "socket.io-client";

/**
 * Factory um ein neues Websocket-Objekt zu erstellen.
 * @param path Pfad für die Socket.IO Ressource.
 * @param opts Optionen für das Socket.IO Objekt.
 */
export const websocket = (
  path: string,
  opts?: Partial<ManagerOptions & SocketOptions>
) => {
  const host = `${window.location.protocol}//${window.location.host}`;
  return io(host + path, {
    host,
    path: "/api/ws/socket.io",
    ...opts,
  });
};
