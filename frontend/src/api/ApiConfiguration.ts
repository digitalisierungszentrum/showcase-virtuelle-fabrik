import { Configuration } from "../openapi";

export const ApiConfiguration = new (class extends Configuration {
  basePath = "/api";
})();
