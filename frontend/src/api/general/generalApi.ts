import { szenarioRequests } from "./szenarioRequests";

/**
 * Api für nicht Szenario-spezifische Requests.
 */
export const useGeneralApi = () => {
  return {
    ...szenarioRequests,
  };
};

/**
 * Typ der GeneralApi.
 */
export type GeneralApi = ReturnType<typeof useGeneralApi>;
