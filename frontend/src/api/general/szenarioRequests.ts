import { NewSzenario, Szenario, SzenariosApiFactory } from "../../openapi";
import { ApiConfiguration } from "../ApiConfiguration";

const api = SzenariosApiFactory(ApiConfiguration);

/**
 * Requests um Szenarien zu verwalten.
 */
export const szenarioRequests = {
  getAllSzenarios: () => api.getAllSzenarios(),
  postSzenario: (szenario: NewSzenario) => api.postSzenario(szenario),
  getSzenario: (sid: string) => api.getSzenario(sid),
  putSzenario: (sid: string, szenario: Szenario) =>
    api.putSzenario(sid, szenario),
  deleteSzenario: (sid: string) => api.deleteSzenario(sid),
};
