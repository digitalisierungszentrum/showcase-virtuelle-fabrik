export { useSzenarioApi } from "./szenario/szenarioApi";
export type { SzenarioApi } from "./szenario/szenarioApi";
export { useGeneralApi } from "./general/generalApi";
export type { GeneralApi } from "./general/generalApi";
