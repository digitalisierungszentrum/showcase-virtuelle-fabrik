import { Materialbedarf } from "./Materialbedarf";
import { Produktionsschritt } from "./Produktionsschritt";

/**
 * BO eines Produkts.
 */
export interface Produkt {
  /**
   * Unique Identifier des Produkts.
   */
  id: string;
  /**
   * Names des Produkts.
   */
  name: string;
  /**
   * Preis für den dieses Produkt verkauft wird.
   */
  verkaufspreis: number;
  /**
   * Das Rezept, mit welchem dieses Produkt hergestellt werden kann.
   */
  produktionsschritte: Produktionsschritt[];
  /**
   * Die Materialien die benötigt werden, um dieses Produkt herzustellen.
   */
  materialbedarf: Materialbedarf[];
}
