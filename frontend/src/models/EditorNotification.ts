import { NotificationType } from "./NotificationType";

/**
 * Model von Notifications, die in der Editor View angezeigt werden können.
 */
export interface EditorNotification {
  /**
   * ID der Notification. Wird verwendet, um Notifications zu löschen und zu überschreiben.
   */
  id: string;
  /**
   * Titel der Notification.
   */
  title: string;
  /**
   * Details der Notification.
   */
  details: string;
  /**
   * Typ der Notification.
   */
  type: NotificationType;
  /**
   * Wenn true, hat die Notification einen Knopf um sie zu entfernen.
   */
  showClose?: boolean;
  /**
   * Wenn true, wird ein ProgressIndication im body der Notification angezeigt.
   */
  showProgressIndicator?: boolean;
  /**
   * Wenn true, bleibt die Notification erhalten, wenn alle Notifications gelöscht werden.
   */
  persistent?: boolean;
}
