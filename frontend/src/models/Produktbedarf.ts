/**
 * BO eines Produktbedarfs.
 *
 * Beschreibt welches Produkt und wie viel Stück von einem Produkt
 * benötigt werden.
 */
export interface Produktbedarf {
  /**
   * ID des Produkts.
   */
  produktId: string;
  /**
   * Stückzahl des Produkts.
   */
  stueckzahl: number;
}
