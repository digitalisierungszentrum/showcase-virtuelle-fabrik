/**
 * DTO eines neuen Arbeitsschritts.
 *
 * gleich zu {@link Arbeitsschritt}, ohne eine ID.
 */
export interface NewArbeitsschritt {
  name: string;
}
