import { Produktbedarf } from "./Produktbedarf";

/**
 * BO einer Charge.
 */
export interface Charge {
  /**
   * Unique Identifier der Charge.
   */
  id: string;
  /**
   * Name der Charge.
   */
  name: string;
  /**
   * Anhand der Priorität wird in der Simulation eine Abarbeitungs-Reihenfolge
   * bestimmt.
   */
  prioritaet: number;
  /**
   * Das Produkt und die Stückzahl aus welcher diese Charge besteht.
   *
   * Ursprünglich war angedacht, dass eine Charge mehrere verschiedene Produkte
   * besitzen kann, deswegen ist Produktbedarf ein Array. In der momentanen
   * Implementation wird allerdings damit gerechnet, dass dieses Array
   * genau ein Element besitzt.
   */
  produktbedarf: Produktbedarf[];
}
