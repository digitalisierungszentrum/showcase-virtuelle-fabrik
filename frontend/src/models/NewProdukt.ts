import { Materialbedarf } from "./Materialbedarf";
import { Produktionsschritt } from "./Produktionsschritt";

/**
 * DTO eines Produkts.
 *
 * Gleich zu {@link Produkt}, ohne die ID.
 */
export interface NewProdukt {
  name: string;
  verkaufspreis: number;
  produktionsschritte: Produktionsschritt[];
  materialbedarf: Materialbedarf[];
}
