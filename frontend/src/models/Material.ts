/**
 * BO eines Materials.
 */
export interface Material {
  /**
   * Unique Identifier des Materials.
   */
  id: string;
  /**
   * Name des Materials.
   */
  name: string;
  /**
   * Kosten eines einzelnen Stücks, dieses Materials.
   */
  kostenStueck: number;
  /**
   * Anfänglicher Lagerbestand dieses Materials.
   */
  bestand: number;
  /**
   * Neu eingang dieses Materials pro Minute.
   */
  aufstockenMinute: number;
}
