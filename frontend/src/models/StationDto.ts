/**
 * DTO einer {@link Station}.
 */
export interface StationDto {
  /**
   * Unique Identifier der Station.
   */
  id: string;
  /**
   * Name Der Station.
   */
  name: string;
  /**
   * Bestimmt die Reihenfolge in der die Stationen angezeigt werden.
   */
  order: number;
  /**
   * IDs der Maschinen die in dieser Station existieren.
   */
  maschinen_stueckzahl: {maschine_id: string, stueckzahl: number}[];
  /**
   * ID der Charge die von dieser Station bearbeitet werden soll.
   */
  chargen: string[];
  /**
   * Das Budget welches bei der Optimierung zur Verfügung steht
   */
  budget_optimierung: number;
}
