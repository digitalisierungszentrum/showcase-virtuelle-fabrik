import { Mitarbeiterbefaehigung } from "./Mitarbeiterbefaehigung";

/**
 * DTO eines Mitarbeiters.
 *
 * Gleich zu {@link Mitarbeiter}, ohne die ID.
 */
export interface NewMitarbeiter {
  name: string;
  befaehigungen: Mitarbeiterbefaehigung[];
  wechselzeit: number;
}
