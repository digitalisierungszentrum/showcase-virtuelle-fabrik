/**
 * Bo einer Maschinenbefähigung.
 *
 * Beschreibt, einen Arbeitsschritt, den eine Maschine ausführen kann
 * und wie viel Zeit sie dafür braucht.
 */
export interface Maschinenbefaehigung {
  /**
   * ID des Arbeitsschritts.
   */
  schrittId: string;
  /**
   * Zeit die benötigt wird, um diesen Arbeitsschritt zu bearbeiten,
   * in Sekunden.
   */
  taktrate: number;
}
