import { Charge } from "../Charge";
import { Maschine } from "../Maschine";
import { Produktionslinie } from "../Produktionslinie";
import { ProduktionslinieDto } from "../ProduktionslinieDto";

/**
 * Sucht das Element aus der gegebene Liste, bei dem, das gegebene Feld,
 * den gegebenen Wert hat.
 * @param field Feld das verglichen werden soll.
 * @param value Wert gegen welchen das Feld verglichen werden soll.
 * @param list Liste in der gesucht werden soll.
 * @return Das erste passende Item oder undefined.
 */
function getByField<T>(
  field: keyof T,
  value: T[keyof T],
  list: T[]
): T | undefined {
  return list.find((e) => e[field] === value);
}

/**
 * Mappt die gegebenen Werte auf Elemente aus der gegebenen Liste,
 * anhand dem gegebenen Feld.
 * @param field Identifier des Feldes anhand welchem gematcht werden soll. Das
 *              Feld muss in den Elementen der Liste vorkommen.
 * @param values Werte die gemappt werden sollen.
 * @param list Liste in der nach matches gesucht wird.
 * @return Eine Liste mit Elementen aus der gegebenen Liste.
 */
function mapByField<T>(field: keyof T, values: T[keyof T][], list: T[]): T[] {
  return values.map((v) => getByField(field, v, list)!).filter((v) => v);
}

/**
 * Mappt ein ProduktionslinienDto zu einer Produktionslinie
 * @returns Produktionslinie
 */
export function dtoToProduktionslinie(
  dto: ProduktionslinieDto,
  maschinen: Maschine[],
  chargen: Charge[]
): Produktionslinie {
  return {
    id: dto.id,
    stationen: dto.stationen.map((station) => {
      return {
        ...station,
        maschinen_stueckzahl: station.maschinen_stueckzahl.map((ms) => ({
          stueckzahl: ms.stueckzahl ?? 1,
          objekt: maschinen.filter((m) => ms.maschine_id == m.id)[0],
        })),
        chargen: mapByField("id", station.chargen, chargen),
      };
    }),
  };
}

/**
 * Mappt eine Produktionslinie zu einem ProduktionslinienDto
 * @returns ProduktionslinieDto
 */
export function produktionslinieToDto({
  id,
  stationen,
}: Produktionslinie): ProduktionslinieDto {
  return {
    id,
    stationen: stationen.map((s) => ({
      ...s,
      maschinen_stueckzahl: s.maschinen_stueckzahl.map((ms) => {
        return { maschine_id: ms.objekt.id, stueckzahl: ms.stueckzahl };
      }),
      chargen: s.chargen.map((c) => c.id),
    })),
  };
}
