/**
 * Bo eines Materialbedarfs.
 *
 * Beschreibt wie viel von einem Material benötigt wird.
 */
export interface Materialbedarf {
  /**
   * ID des Materials.
   */
  materialId: string;
  /**
   * Stückzahl die von diesem Material benötigt wird.
   */
  menge: number;
}
