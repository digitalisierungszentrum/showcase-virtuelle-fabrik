import { StationDto } from "./StationDto";

/**
 * DTO einer {@link Produktionslinie}.
 */
export interface ProduktionslinieDto {
  id: string;
  stationen: StationDto[];
}
