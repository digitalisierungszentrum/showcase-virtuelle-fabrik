import { Charge } from "./Charge";
import { Maschine } from "./Maschine";
import { Stueckzahl } from "./Stueckzahl";

/**
 * BO einer Station.
 *
 * Repräsentiert eine Station in einer Produktionslinie.
 */
export interface Station {
  /**
   * Unique Identifier dieser Station.
   */
  id: string;
  /**
   * Name dieser Station.
   */
  name: string;
  /**
   * Bestimmt die Reihenfolge in der die Stationen angezeigt werden.
   */
  order: number;
  /**
   * Maschinen die in dieser Station existieren.
   */
  maschinen_stueckzahl: Stueckzahl<Maschine>[];
  /**
   * Charge die von dieser Station bearbeitet werden sollen.
   */
  chargen: Charge[];
  /**
   * Das Budget welches bei der Optimierung zur Verfügung steht
   */
  budget_optimierung: number;
}
