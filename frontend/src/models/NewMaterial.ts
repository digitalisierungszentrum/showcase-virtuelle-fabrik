/**
 * DTO eines neuen Materials.
 *
 * Gleich zu {@link Material}, ohne die ID.
 */
export interface NewMaterial {
  name: string;
  kostenStueck: number;
  bestand: number;
  aufstockenMinute: number;
}
