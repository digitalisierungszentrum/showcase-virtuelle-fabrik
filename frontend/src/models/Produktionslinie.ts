import { Station } from "./Station";

/**
 * BO einer Produktionslinie.
 */
export interface Produktionslinie {
  id: string;
  /**
   * Stationen die in dieser Produktionslinie existieren.
   */
  stationen: Station[];
}
