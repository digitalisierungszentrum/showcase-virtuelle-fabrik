import { Maschinenbefaehigung } from "./Maschinenbefaehigung";

/**
 * DTO einer neuen Maschine.
 *
 * gleich zu {@link Maschine}, ohne eine ID.
 */
export interface NewMaschine {
  name: string;
  ruestzeit: number;
  kostenMinute: number;
  anschaffungskosten: number;
  ausfallWahrscheinlichkeit: number;
  mitarbeiterMin: number;
  mitarbeiterMax: number;
  maschinenbefaehigungen: Maschinenbefaehigung[];
}
