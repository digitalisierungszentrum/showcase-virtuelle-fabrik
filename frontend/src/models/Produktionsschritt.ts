/**
 * BO eines Produktionsschritts.
 *
 * Beschreibt einen Arbeitsschritt, der in einer Herstellung getätigt werden
 * muss und um den wievielten Schritt in der Herstellung es sich handelt.
 */
export interface Produktionsschritt {
  /**
   * ID des Arbeitsschritts.
   */
  arbeitsschrittId: string;
  /**
   * Der wievielte Schritt in der Herstellung, dieser Arbeitsschritt ist.
   */
  schritt: number;
}
