/**
 * BO eines Arbeitsschritts.
 */
export interface Arbeitsschritt {
  /**
   * Unique Identifier des Arbeitsschritts.
   */
  id: string;
  /**
   * Name des Arbeitsschritts.
   */
  name: string;
}
