/**
 * Typ-Enum der EditorNotifications.
 */
export enum NotificationType {
  Error = "Error",
  Warn = "Warn",
  Success = "Success",
  Info = "Info",
}
