import { Maschinenbefaehigung } from "./Maschinenbefaehigung";

/**
 * BO einer Maschine
 */
export interface Maschine {
  /**
   * Unique Identifier der Maschine
   */
  id: string;
  /**
   * Name der Maschine.
   */
  name: string;
  /**
   * Der Overhead den eine Maschine brauch um auf einen
   * neuen Arbeitsschritt zu wechseln, in Sekunden.
   */
  ruestzeit: number;
  /**
   * Die Kosten die diese Maschine im Betrieb kostet, pro Minute.
   */
  kostenMinute: number;
  /**
   * Die Kosten die diese Maschine in der Anschaffung kostet.
   */
  anschaffungskosten: number;
  /**
   * Prozentuale Wahrscheinlichkeit, dass diese Maschine ausfällt
   * in Dezimaldarstellung.
   */
  ausfallWahrscheinlichkeit: number;
  /**
   * Die Mindestanzahl an benötigten Mitarbeitern, damit diese Maschine
   * arbeiten kann.
   */
  mitarbeiterMin: number;
  /**
   * Die Maximalanzahl an Mitarbeitern, die an dieser Maschine arbeiten können.
   */
  mitarbeiterMax: number;
  /**
   * Die Arbeitsschritte, welche diese Maschine ausführen kann,
   * und wie viel Zeit benötigt wird, um einen Arbeitsschritt durchzuführen.
   */
  maschinenbefaehigungen: Maschinenbefaehigung[];
}
