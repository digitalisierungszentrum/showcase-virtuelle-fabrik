import { Mitarbeiterbefaehigung } from "./Mitarbeiterbefaehigung";

/**
 * BO eines Mitarbeiters.
 */
export interface Mitarbeiter {
  /**
   * Unique Identifier dies Mitarbeiters.
   */
  id: string;
  /**
   * Name des Mitarbeiters.
   */
  name: string;
  /**
   * Arbeitsschritte die dieser Mitarbeiter ausführen kann,
   * und wie lange er dafür braucht.
   */
  befaehigungen: Mitarbeiterbefaehigung[];
  /**
   * Overhead die ein mitarbeiter braucht um an eine Maschine zu wechseln,
   * in Sekunden.
   */
  wechselzeit: number;
}
