/**
 * BO einer Mitarbeiterbefähigung.
 *
 * Beschreibt welchen Arbeitsschritt ein Mitarbeiter ausführen kann,
 * und wie viel Zeit er dafür braucht.
 */
export interface Mitarbeiterbefaehigung {
  /**
   * ID des Arbeitsschritts.
   */
  schrittId: string;
  /**
   * Zeit die benötigt wird, um diesen Arbeitsschritt auszuführen,
   * in Sekunden,
   */
  taktrate: number;
}
