import { Params, generatePath } from "react-router-dom";

/**
 * Haupt-Routen der Anwendung.
 */
export const AppRoutes = {
  Szenarios: "/",
  SzenarioDetails: "/szenario/:szenarioId",
  Fallback: "*",
};

/**
 * Routen eines Szenarios.
 */
export const SzenarioRoutes = {
  Stammdaten: "stammdaten",
  Editor: "editor",
  Dashboard: "dashboard",
};

/**
 * Routen der Stammdatenansicht eines Szenarios.
 */
export const StammdatenRoutes = {
  MaschinenDetails: "maschine/:maschineId",
  MaschinenEdit: "maschine/:maschineId/edit",
  MaschinenNew: "maschine/new",

  MitarbeiterDetails: "mitarbeiter/:mitarbeiterId",
  MitarbeiterEdit: "mitarbeiter/:mitarbeiterId/edit",
  MitarbeiterNew: "mitarbeiter/new",

  MaterialienDetails: "material/:materialId",
  MaterialienEdit: "material/:materialId/edit",
  MaterialienNew: "material/new",

  ProduktDetails: "produkt/:produktId",
  ProduktEdit: "produkt/:produktId/edit",
  ProduktNew: "produkt/new",

  ChargeDetails: "chargen/:chargeId",
  ChargeEdit: "chargen/:chargeId/edit",
  ChargeNew: "chargen/new",

  ArbeitsschritteDetails: "arbeitsschritt/:arbeitsschrittId",
  ArbeitsschritteEdit: "arbeitsschritt/:arbeitsschrittId/edit",
  ArbeitsschritteNew: "arbeitsschritt/new",
};

export const StammdatenParams = {
  View: "view",
};

export enum StammdatenViewOptions {
  Arbeitsschritte = "arbeitsschritte",
  Chargen = "chargen",
  Maschinen = "maschinen",
  Material = "material",
  Mitarbeiter = "mitarbeiter",
  Produkte = "produkte",
}

export const DashboardRoutes = {
  Simulation: ":simulationId",
};

/**
 * Typdefinition der möglichen Route Parameter.
 */
export type AppParams = {
  szenarioId?: string;
  maschineId?: string;
  mitarbeiterId?: string;
  materialId?: string;
  produktId?: string;
  chargeId?: string;
  arbeitsschrittId?: string;
  simulationId?: string;
};

/**
 * A helper method to replace parameter placeholders in a string.
 *
 * @example getRoute('/todo/detail/:id', { id: 1 }) will return '/todo/detail/1'
 *
 * @param {string} route The route with optional parameter placeholders, e.g. '/todo/detail/:id'
 * @param {AppParams} [params] Optional object with key value pairs to replace parameter placeholders from the `route`.
 * @returns {string} resolved route with parameters
 * @throws Will throw a TypeError if provided params and path don’t match
 */
export const getRoute = (route: string, params: AppParams) => {
  return generatePath(route, params as Params);
};

/**
 * Helper um Routes aus verschiedenen parts zusammen zu setzen.
 *
 * @param parts parts aus denen eine Route zusammen gesetzt werden soll
 * @returns Zusammengesetzte Route.
 */
export const buildRoute = (...parts: string[]) =>
  parts.map((part) => part.replace(/(^\/+)|(\/+$)/, "")).join("/");
