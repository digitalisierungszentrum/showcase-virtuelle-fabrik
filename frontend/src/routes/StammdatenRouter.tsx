import { Route } from "react-router-dom";

import Stammdaten from "../views/stammdaten/Stammdaten";
import ArbeitsschrittDetails from "../views/stammdaten/arbeitsschritte/ArbeitsschrittDetails";
import { ArbeitsschrittEdit } from "../views/stammdaten/arbeitsschritte/ArbeitsschrittEdit";
import { ArbeitsschrittNew } from "../views/stammdaten/arbeitsschritte/ArbeitsschrittNew";
import ChargeDetails from "../views/stammdaten/chargen/ChargeDetails";
import { ChargeEdit } from "../views/stammdaten/chargen/ChargeEdit";
import { ChargeNew } from "../views/stammdaten/chargen/ChargeNew";
import MaschineDetails from "../views/stammdaten/maschinen/MaschineDetails";
import { MaschineEdit } from "../views/stammdaten/maschinen/MaschineEdit";
import { MaschineNew } from "../views/stammdaten/maschinen/MaschineNew";
import MaterialDetails from "../views/stammdaten/materialien/MaterialDetails";
import { MaterialEdit } from "../views/stammdaten/materialien/MaterialEdit";
import { MaterialNew } from "../views/stammdaten/materialien/MaterialNew";
import ProduktDetails from "../views/stammdaten/produkte/ProduktDetails";
import { ProduktEdit } from "../views/stammdaten/produkte/ProduktEdit";
import { ProduktNew } from "../views/stammdaten/produkte/ProduktNew";
import { StammdatenRoutes, SzenarioRoutes } from "./appRoutes";

const StammdatenRouter = (
  <Route path={SzenarioRoutes.Stammdaten} element={<Stammdaten />}>
    <Route
      path={StammdatenRoutes.MaschinenDetails}
      element={<MaschineDetails />}
    />
    <Route path={StammdatenRoutes.MaschinenNew} element={<MaschineNew />} />
    <Route path={StammdatenRoutes.MaschinenEdit} element={<MaschineEdit />} />

    <Route
      path={StammdatenRoutes.MaterialienDetails}
      element={<MaterialDetails />}
    />
    <Route path={StammdatenRoutes.MaterialienNew} element={<MaterialNew />} />
    <Route path={StammdatenRoutes.MaterialienEdit} element={<MaterialEdit />} />

    <Route
      path={StammdatenRoutes.ArbeitsschritteDetails}
      element={<ArbeitsschrittDetails />}
    />
    <Route
      path={StammdatenRoutes.ArbeitsschritteNew}
      element={<ArbeitsschrittNew />}
    />
    <Route
      path={StammdatenRoutes.ArbeitsschritteEdit}
      element={<ArbeitsschrittEdit />}
    />

    <Route
      path={StammdatenRoutes.ProduktDetails}
      element={<ProduktDetails />}
    />
    <Route path={StammdatenRoutes.ProduktNew} element={<ProduktNew />} />
    <Route path={StammdatenRoutes.ProduktEdit} element={<ProduktEdit />} />

    <Route path={StammdatenRoutes.ChargeDetails} element={<ChargeDetails />} />
    <Route path={StammdatenRoutes.ChargeNew} element={<ChargeNew />} />
    <Route path={StammdatenRoutes.ChargeEdit} element={<ChargeEdit />} />
  </Route>
);

export default StammdatenRouter;
