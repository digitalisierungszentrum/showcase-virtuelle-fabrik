import { BusyIndicator } from "@ui5/webcomponents-react";
import { Suspense } from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import NotFoundPage from "../components/not-found-page/NotFoundPage";
import UnexpectedErrorPage from "../components/unexpected-error-page/UnexpectedErrorPage";
import Dashboard from "../views/dashboard/Dashboard";
import Editor from "../views/editor/Editor";
import StammdatenRouter from "./StammdatenRouter";
import {
  AppRoutes,
  DashboardRoutes,
  SzenarioRoutes,
  getRoute,
} from "./appRoutes";

/**
 * Primärrouter der Anwendung.
 * Routen werden in {@link ./appRoutes.ts} definiert.
 *
 * @returns AppRouter
 */
const AppRouter = () => {
  return (
    <Suspense fallback={<BusyIndicator active />}>
      <Routes>
        <Route
          path={AppRoutes.Szenarios}
          element={
            <Navigate
              to={getRoute(AppRoutes.SzenarioDetails, { szenarioId: "1" })}
            />
          }
        />
        <Route path={AppRoutes.SzenarioDetails}>
          <Route index element={<Navigate to={SzenarioRoutes.Stammdaten} />} />

          {StammdatenRouter}

          <Route path={SzenarioRoutes.Editor} element={<Editor />} />

          <Route path={SzenarioRoutes.Dashboard}>
            <Route index element={<Dashboard />} />
            <Route path={DashboardRoutes.Simulation} element={<Dashboard />} />
        </Route>
        </Route>
        <Route
          path={AppRoutes.Fallback}
          element={<Navigate to={AppRoutes.Szenarios} replace />}
        ></Route>

        <Route path="error" element={<UnexpectedErrorPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </Suspense>
  );
};

export default AppRouter;
