import {
  NavigateOptions,
  To,
  useNavigate,
  useSearchParams,
} from "react-router-dom";

type NavigateOptionsWithParams = NavigateOptions & { dropParams: boolean };

/**
 * Wrapper für React-Routers useNavigate, der die momentanen QueryParams behält.
 *
 * Durch die option dropParams kann das Behalten der QueryParams, deaktiviert
 * werden.
 */
export const useNavigateWithParams = () => {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  return (to: To, options?: NavigateOptionsWithParams) =>
    navigate(
      to + (options?.dropParams ? "" : "?" + searchParams.toString()),
      options
    );
};
