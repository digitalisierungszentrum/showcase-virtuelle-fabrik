import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";

import { useSzenarioApi } from "../api";
import { NewProdukt } from "../models/NewProdukt";
import { Produkt } from "../models/Produkt";
import { chargenQueryKey } from "./useChargen";

/**
 * Allgemeiner Cache-Key für Produkte.
 */
const key = "produkte";
export const produkteQueryKey = key;

/**
 * @returns React Query mit allen Produkte des Szenarios.
 */
export const useProdukte = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useQuery<Produkt[], AxiosError>(
    [key],
    async () => {
      const res = await api.getAllProdukte();

      return res.data;
    },
    {
      keepPreviousData: true,
      onSuccess: (produkte) => {
        // für alle gefundene Produkte einen cache-eintrag erstellen.
        produkte.forEach((m) => {
          queryClient.setQueryData([key, m.id], m);
        });
      },
    }
  );
};

/**
 * @param {string} id Id der gewünschten Produkte
 * @returns React Query mit gegebener Produkte.
 */
export const useProdukt = (id?: string) => {
  const api = useSzenarioApi();
  return useQuery<Produkt, AxiosError>(
    [key, id],
    async () => {
      const res = await api.getProdukt(id!);

      return res.data;
    },
    {
      keepPreviousData: true,
      enabled: !!id,
    }
  );
};

/**
 * @returns Mutation um gegebene Produkte hinzuzufügen.
 */
export const useAddProdukte = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<{ id: string }, AxiosError, NewProdukt>(
    async (produkt) => {
      const res = await api.postProdukt(produkt);

      return res.data;
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um gegebene Produkte zu editieren.
 */
export const useEditProdukt = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, Produkt>(
    async (produkt) => {
      await api.putProdukt(produkt.id, produkt);
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um Produkte mit gegebener Id zu löschen.
 */
export const useDeleteProdukt = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, { id: string }>(
    async ({ id }) => {
      await api.deleteProdukt(id);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(key);
        queryClient.invalidateQueries(chargenQueryKey);
      },
    }
  );
};
