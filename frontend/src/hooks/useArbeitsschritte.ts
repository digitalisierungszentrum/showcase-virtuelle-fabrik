import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";

import { useSzenarioApi } from "../api";
import { Arbeitsschritt } from "../models/Arbeitsschritt";
import { NewArbeitsschritt } from "../models/NewArbeitsschritt";
import { maschinenQueryKey } from "./useMaschinen";
import { mitarbeiterQueryKey } from "./useMitarbeiter";
import { produkteQueryKey } from "./useProdukte";

/**
 * Allgemeiner Cache-Key für Arbeitsschritte.
 */
const key = "arbeitsschritte";
export const arbeitsschritteQueryKey = key;

/**
 * @returns React Query die alle Arbeitsschritte zurückgibt.
 */
export const useArbeitsschritte = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useQuery<Arbeitsschritt[], AxiosError>(
    [key],
    async () => {
      const res = await api.getAllArbeitsschritte();

      return res.data;
    },
    {
      keepPreviousData: true,
      onSuccess: (arbeitsschritte) => {
        // für alle gefundene Arbeitsschritte einen cache-eintrag erstellen.
        arbeitsschritte.forEach((s) =>
          queryClient.setQueryData([key, s.id], s)
        );
      },
    }
  );
};

/**
 * @param {string} id Id des gewünschten Arbeitsschritts.
 * @returns React Query mit gegebenem Arbeitsschritt.
 */
export const useArbeitsschritt = (id: string) => {
  const api = useSzenarioApi();

  return useQuery<Arbeitsschritt, AxiosError>(
    [key, id],
    async () => {
      const res = await api.getArbeitsschritt(id);

      return res.data;
    },
    {
      keepPreviousData: true,
    }
  );
};

/**
 * @returns Mutation um gegebenes Arbeitsschritt hinzuzufügen.
 */
export const useAddArbeitsschritt = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<{ id: string }, AxiosError, NewArbeitsschritt>(
    async (arbeitsschritt) => {
      const res = await api.postArbeitsschritt(arbeitsschritt);

      return res.data;
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um gegebenen Arbeitsschritt zu editieren.
 */
export const useEditArbeitsschritt = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, Arbeitsschritt>(
    async (arbeitsschritt) => {
      await api.putArbeitsschritt(arbeitsschritt.id, arbeitsschritt);
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um Arbeitsschritt mit gegebener Id zu löschen.
 */
export const useDeleteArbeitsschritt = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, { id: string }>(
    async ({ id }) => {
      await api.deleteArbeitsschritt(id);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries([key]);
        queryClient.invalidateQueries([maschinenQueryKey]);
        queryClient.invalidateQueries([mitarbeiterQueryKey]);
        queryClient.invalidateQueries([produkteQueryKey]);
      },
    }
  );
};
