import { AxiosError } from "axios";
import {
  QueryClient,
  UseQueryOptions,
  useMutation,
  useQuery,
  useQueryClient,
} from "react-query";

import { SzenarioApi, useSzenarioApi } from "../api";
import { Mitarbeiter } from "../models/Mitarbeiter";

/**
 * Allgemeiner Cache-Key für Maschinen.
 */
const key = "mitarbeiter";
export const mitarbeiterQueryKey = key;

/**
 * QueryOptions um alle Mitarbeiter zu querien.
 * @param api SzenarioApi Instanz.
 * @param queryClient QueryClient.
 */
export const allMitarbeiterQueryOptions = (
  api: SzenarioApi,
  queryClient: QueryClient
): UseQueryOptions<Mitarbeiter[], AxiosError> => {
  return {
    queryKey: [key],
    queryFn: async () => {
      const res = await api.getAllMitarbeiter();

      return res.data;
    },
    keepPreviousData: true,
    onSuccess: (mitarbeiter) => {
      // für alle gefundene Maschinen einen cache-eintrag erstellen.
      mitarbeiter.forEach((m) => {
        queryClient.setQueryData([key, m.id], m);
      });
    },
  };
};

/**
 * @returns React Query mit allen Maschinen des Szenarios.
 */
export const useAllMitarbeiter = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useQuery<Mitarbeiter[], AxiosError>(
    allMitarbeiterQueryOptions(api, queryClient)
  );
};

/**
 * @param {string} id Id der gewünschten Maschine.
 * @returns React Query mit gegebener Maschinen.
 */
export const useMitarbeiter = (id: string) => {
  const api = useSzenarioApi();

  return useQuery<Mitarbeiter, AxiosError>(
    [key, id],
    async () => {
      const res = await api.getMitarbeiter(id);

      return res.data;
    },
    {
      keepPreviousData: true,
    }
  );
};

/**
 * @returns Mutation um gegebene Maschinen hinzuzufügen.
 */
export const useAddMitarbeiter = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<{ id: string }, AxiosError, Mitarbeiter>(
    async (mitarbeiter) => {
      const res = await api.postMitarbeiter(mitarbeiter);

      return res.data;
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries(key);
      },
    }
  );
};

/**
 * @returns Mutation um gegebene Maschine zu editieren.
 */
export const useEditMitarbeiter = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, Mitarbeiter>(
    async (mitarbeiter) => {
      await api.putMitarbeiter(mitarbeiter.id, mitarbeiter);
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries(key);
      },
    }
  );
};

/**
 * @returns Mutation um Maschine mit gegebener Id zu löschen.
 */
export const useDeleteMitarbeiter = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, { id: string }>(
    async ({ id }) => {
      await api.deleteMitarbeiter(id);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(key);
      },
    }
  );
};
