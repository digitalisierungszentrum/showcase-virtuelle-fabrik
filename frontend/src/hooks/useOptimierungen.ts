import { AxiosError } from "axios";
import { useQuery, useQueryClient } from "react-query";

import { useSzenarioApi } from "../api";
import { Optimierung } from "../openapi";

/**
 * Allgemeiner Cache-Key für Simulationen.
 */
const key = "simulationen";
export const simulationenQueryKey = key;

/**
 * @returns React Query die alle Simulationen zurückgibt.
 */
export const useOptimierungen = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useQuery<Optimierung[], AxiosError>(
    [key],
    async () => {
      // const res = {
      //   data: testOptimierungen,
      // };
      const res = await api.getAllOptimierungen();

      return res.data;
    },
    {
      keepPreviousData: true,
      cacheTime: 0,
      onSuccess: (simulationen) => {
        // für alle gefundene Optimierungen einen cache-eintrag erstellen.
        simulationen.forEach((s) => queryClient.setQueryData([key, s.id], s));
      },
    }
  );
};
