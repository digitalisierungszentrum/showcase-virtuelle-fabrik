import { AxiosError } from "axios";
import {
  QueryClient,
  UseQueryOptions,
  useMutation,
  useQuery,
  useQueryClient,
} from "react-query";

import { SzenarioApi, useSzenarioApi } from "../api";
import { Maschine } from "../models/Maschine";
import { NewMaschine } from "../models/NewMaschine";

/**
 * Allgemeiner Cache-Key für Maschinen.
 */
const key = "maschinen";
export const maschinenQueryKey = key;

/**
 * QueryOptions um alle Maschinen zu querien.
 * @param api SzenarioApi Instanz.
 * @param queryClient QueryClient.
 */
export const allMaschinenQueryOptions = (
  api: SzenarioApi,
  queryClient: QueryClient
): UseQueryOptions<Maschine[], AxiosError> => {
  return {
    queryKey: [key],
    queryFn: async () => {
      const res = await api.getAllMaschinen();

      return res.data;
    },
    keepPreviousData: true,
    onSuccess: (maschinen) => {
      // für alle gefundene Maschinen einen cache-eintrag erstellen.
      maschinen.forEach((m) => {
        queryClient.setQueryData([key, m.id], m);
      });
    },
  };
};

/**
 * @returns React Query mit allen Maschinen des Szenarios.
 */
export const useAllMaschinen = () => {
  const Api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useQuery<Maschine[], AxiosError>(
    allMaschinenQueryOptions(Api, queryClient)
  );
};

/**
 * @param {string} id Id der gewünschten Maschine.
 * @returns React Query mit gegebener Maschine.
 */
export const useMaschine = (id: string) => {
  const Api = useSzenarioApi();

  return useQuery<Maschine, AxiosError>(
    [key, id],
    async () => {
      const res = await Api.getMaschine(id);

      return res.data;
    },
    {
      keepPreviousData: true,
    }
  );
};

/**
 * @returns Mutation um gegebene Maschinen hinzuzufügen.
 */
export const useAddMaschine = () => {
  const Api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<{ id: string }, AxiosError, NewMaschine>(
    async (maschine) => {
      const res = await Api.postMaschine(maschine);

      return res.data;
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um gegebene Maschine zu editieren.
 */
export const useEditMaschine = () => {
  const Api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, Maschine>(
    async (maschine) => {
      await Api.putMaschine(maschine.id, maschine);
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um Maschine mit gegebener Id zu löschen.
 */
export const useDeleteMaschine = () => {
  const Api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, { id: string }>(
    async ({ id }) => {
      await Api.deleteMaschine(id);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries([key]);
      },
    }
  );
};
