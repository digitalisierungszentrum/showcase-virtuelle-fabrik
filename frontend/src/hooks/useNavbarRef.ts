import { ShellBarDomRef } from "@ui5/webcomponents-react";
import { ReactNode, RefObject, createContext, useContext } from "react";
import { createPortal } from "react-dom";

let ref: RefObject<ShellBarDomRef> | undefined;

/**
 * Context um die Ref der Navbar zu verwalten
 */
export const NavbarContext = createContext({ ref });

/**
 * Stellt die Ref der Navbar zu verfügung.
 */
export const useNavbarRef = () => {
  return useContext(NavbarContext).ref;
};

/**
 * stellt ein Portal bereit über welches, Elemente in der Navbar Actions Leiste angezeigt werden können
 */
export const useNavbarActionsPortal = () => {
  const ref = useNavbarRef();

  return (element: ReactNode) =>
    ref?.current && createPortal(element, ref.current as any);
};
