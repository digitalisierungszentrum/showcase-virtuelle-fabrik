import { AxiosError } from "axios";
import {
  QueryClient,
  UseQueryOptions,
  useMutation,
  useQuery,
  useQueryClient,
} from "react-query";

import { SzenarioApi, useSzenarioApi } from "../api";
import { Charge } from "../models/Charge";
import { NewCharge } from "../openapi";

/**
 * Allgemeiner Cache-Key für Chargen.
 */
const key = "chargen";
export const chargenQueryKey = key;

/**
 * QueryOptions um all Chargen zu querien.
 * @param api SzenarioApi Instanz.
 * @param queryClient QueryClient.
 */
export const allChargenQueryOptions = (
  api: SzenarioApi,
  queryClient: QueryClient
): UseQueryOptions<Charge[], AxiosError> => {
  return {
    queryKey: [key],
    queryFn: async () => {
      const res = await api.getAllChargen();

      return res.data;
    },
    keepPreviousData: true,
    onSuccess: (maschinen) => {
      // für alle gefundene Maschinen einen cache-eintrag erstellen.
      maschinen.forEach((m) => {
        queryClient.setQueryData([key, m.id], m);
      });
    },
  };
};

/**
 * @returns React Query mit allen Chargen des Szenarios.
 */
export const useChargen = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useQuery<Charge[], AxiosError>(
    allChargenQueryOptions(api, queryClient)
  );
};

/**
 * @param {string} id Id der gewünschten Charge.
 * @returns React Query mit gegebener Charge.
 */
export const useCharge = (id?: string) => {
  const api = useSzenarioApi();

  return useQuery<Charge, AxiosError>(
    [key, id],
    async () => {
      const res = await api.getCharge(id!);

      return res.data;
    },
    {
      keepPreviousData: true,
      enabled: !!id,
    }
  );
};

/**
 * @returns Mutation um gegebene Charge hinzuzufügen.
 */
export const useAddCharge = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<{ id: string }, AxiosError, NewCharge>(
    async (charge) => {
      const res = await api.postCharge(charge);

      return res.data;
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um gegebene Charge zu editieren.
 */
export const useEditCharge = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, Charge>(
    async (charge) => {
      await api.putCharge(charge.id, charge);
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um Charge mit gegebener Id zu löschen.
 */
export const useDeleteCharge = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, { id: string }>(
    async ({ id }) => {
      await api.deleteCharge(id);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries([key]);
      },
    }
  );
};
