import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";

import { useSzenarioApi } from "../api";
import { Material } from "../models/Material";
import { produkteQueryKey } from "./useProdukte";

/**
 * Allgemeiner Cache-Key für Materialien.
 */
const key = "materialien";
export const materialienQueryKey = key;

/**
 * @returns React Query mit allen Materialien des Szenarios.
 */
export const useMaterialien = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useQuery<Material[], AxiosError>(
    [key],
    async () => {
      const res = await api.getAllMaterialien();

      return res.data;
    },
    {
      keepPreviousData: true,
      onSuccess: (materialien) => {
        // für alle gefundene Materialien einen cache-eintrag erstellen.
        materialien.forEach((m) => {
          queryClient.setQueryData([key, m.id], m);
        });
      },
    }
  );
};

/**
 * @param {string} id Id des gewünschten Materials.
 * @returns React Query mit gegebenem Material.
 */
export const useMaterial = (id: string) => {
  const api = useSzenarioApi();

  return useQuery<Material, AxiosError>(
    [key, id],
    async () => {
      const res = await api.getMaterial(id);

      return res.data;
    },
    {
      keepPreviousData: true,
    }
  );
};

/**
 * @returns Mutation um gegebenes Material hinzuzufügen.
 */
export const useAddMaterial = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<{ id: string }, AxiosError, Material>(
    async (material) => {
      const res = await api.postMaterial(material);

      return res.data;
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um gegebenes Material zu editieren.
 */
export const useEditMaterial = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, Material>(
    async (material) => {
      await api.putMaterial(material.id, material);

      await queryClient.invalidateQueries(key);
    },
    {
      onSuccess: () => {
        return queryClient.invalidateQueries([key]);
      },
    }
  );
};

/**
 * @returns Mutation um Material mit gegebener Id zu löschen.
 */
export const useDeleteMaterial = () => {
  const api = useSzenarioApi();
  const queryClient = useQueryClient();

  return useMutation<void, AxiosError, { id: string }>(
    async ({ id }) => {
      await api.deleteMaterial(id);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(key);
        queryClient.invalidateQueries(produkteQueryKey);
      },
    }
  );
};
