import { setTheme } from "@ui5/webcomponents-base/dist/config/Theme";
import "@ui5/webcomponents-fiori/dist/Assets";
import { ShellBarDomRef, ThemeProvider } from "@ui5/webcomponents-react";
import "@ui5/webcomponents/dist/Assets";
import { useEffect, useRef } from "react";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { useTranslation } from "react-i18next";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { BrowserRouter } from "react-router-dom";

import Navbar from "./components/navbar/Navbar";
import { NavbarContext } from "./hooks/useNavbarRef";
import AppRouter from "./routes/AppRouter";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 5 * 1000 * 60, // 5 minutes
      cacheTime: Infinity, // do not delete stale data
    },
  },
});

const App = () => {
  const { t } = useTranslation();

  useEffect(() => {
    const theme = localStorage.getItem("theme");

    if (theme) {
      setTheme(theme);
    }
  }, []);

  const navbarRef = useRef<ShellBarDomRef>(null);

  return (
    <QueryClientProvider client={queryClient}>
      <ReactQueryDevtools initialIsOpen={false} />
      <ThemeProvider>
        <NavbarContext.Provider value={{ ref: navbarRef }}>
          <HelmetProvider>
            <BrowserRouter>
              <Helmet title={t("header.title")}></Helmet>
              <Navbar />
              <AppRouter />
            </BrowserRouter>
          </HelmetProvider>
        </NavbarContext.Provider>
      </ThemeProvider>
    </QueryClientProvider>
  );
};

export default App;
