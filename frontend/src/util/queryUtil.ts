import {
  QueryObserverResult,
  UseBaseQueryResult,
  UseQueryResult,
} from "react-query";

/**
 * Helper Funktion die Zwei ReactQueries zu einem combiniert.
 * Der error ist als Tupel aus dem error von Q1 und Q2 dargestellt.
 *
 * @param q1 erste Query.
 * @param q2 zweite Query.
 * @param fn bestimmt wie die data von q1 und q2 combiniert werden sollen.
 * @returns QueryResult bestehen aus beiden gegebenen Queries.
 */
export function combineQueries<T1, T2, R, E1, E2>(
  q1: QueryObserverResult<T1, E1>,
  q2: QueryObserverResult<T2, E2>,
  fn: (a: T1 | undefined, b: T2 | undefined) => R | undefined
): UseBaseQueryResult<R, [E1, E2]>;
export function combineQueries<T1, T2, R, E1, E2>(
  q1: UseQueryResult<T1, E1>,
  q2: UseQueryResult<T2, E2>,
  fn: (a: T1 | undefined, b: T2 | undefined) => R | undefined
): UseQueryResult<R, [E1, E2]>;
export function combineQueries<T1, T2, R, E1, E2>(
  q1: QueryObserverResult<T1, E1> | UseQueryResult<T1, E1>,
  q2: QueryObserverResult<T2, E2> | UseQueryResult<T2, E2>,
  fn: (a: T1 | undefined, b: T2 | undefined) => R | undefined
): UseQueryResult<R, [E1, E2]> | QueryObserverResult<R, [E1, E1]> {
  return {
    isLoading: q1.isLoading || q2.isLoading,
    isError: q1.isError || q2.isError,
    isIdle: q1.isIdle && q2.isIdle,
    isFetched: q1.isFetched && q2.isFetched,
    isFetching: q1.isFetching || q2.isFetching,
    isRefetching: q1.isRefetching || q2.isRefetchError,
    error: [q1.error, q2.error],
    data: fn(q1.data, q2.data),
  } as UseQueryResult<R, [E1, E2]> | QueryObserverResult<R, [E1, E1]>;
}
