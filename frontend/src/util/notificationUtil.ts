import { Priority } from "@ui5/webcomponents-react";

import { EditorNotification } from "../models/EditorNotification";
import { NotificationType } from "../models/NotificationType";

/**
 * Helper funktion die für einen NotificationType die entsprechende ui5 Notification-Priority zurückgibt.
 * @param type
 */
export function notificationTypeToPriority(type: NotificationType): Priority {
  switch (type) {
    case NotificationType.Error:
      return Priority.High;
    case NotificationType.Warn:
      return Priority.Medium;
    case NotificationType.Success:
      return Priority.Low;
    case NotificationType.Info:
      return Priority.None;
  }
}

/**
 * Ersetzt die gegebenen Notification in der Liste an Notifications, anhand der ID.
 *
 * @param notifications Liste aller Notifications.
 * @param notification Notification die ersetzt werden soll.
 */
export function replaceNotification(
  notifications: EditorNotification[],
  notification: EditorNotification
): EditorNotification[] {
  return notifications.map((n) =>
    n.id === notification.id ? notification : n
  );
}

/**
 * Entfernt alle Notifications die nicht auf persistent gestellt sind.
 *
 * @param notifications Liste aller Notifications
 */
export function removeNonPersistentNotifications(
  notifications: EditorNotification[]
): EditorNotification[] {
  return notifications.filter((n) => n.persistent);
}
