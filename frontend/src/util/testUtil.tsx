import {
  RenderOptions,
  RenderResult,
  render,
  screen,
} from "@testing-library/react";
import { ThemeProvider } from "@ui5/webcomponents-react";
import { AxiosError } from "axios";
import { History, createMemoryHistory } from "history";
import { ReactElement } from "react";
import { QueryClient, QueryClientProvider, setLogger } from "react-query";
import { Router } from "react-router-dom";

/**
 * Custom test render-function die App spezifische wrapper bereitstellt.
 *
 * Über den options parameter kann optional ein History-Objekt für den Router festgelegt werden
 */
export function appRender(
  ui: ReactElement,
  options?: RenderOptions & { history?: History }
): RenderResult {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        staleTime: 5 * 1000 * 60, // 5 minutes
        cacheTime: Infinity, // do not delete stale data
        retry: false,
      },
    },
  });
  setLogger({
    log: () => {
      //stub
    },
    warn: () => {
      //stub
    },
    error: () => {
      //stub
    },
  });
  const history = options?.history || createMemoryHistory();
  return render(
    <QueryClientProvider client={queryClient}>
      <Router location={history.location} navigator={history}>
        <ThemeProvider>{ui}</ThemeProvider>
      </Router>
    </QueryClientProvider>,
    options
  );
}

export function findByTextIncluded(text: any) {
  return screen.findByText((m) => m.includes(text));
}

export function getByTextIncluded(text: any) {
  return screen.getByText((m) => m.includes(text));
}

export function MockAxiosError(status: number, message = "") {
  return new AxiosError(message, "", {}, undefined, {
    data: {},
    status: status,
    config: {},
    headers: {},
    request: {},
    statusText: "",
  });
}
