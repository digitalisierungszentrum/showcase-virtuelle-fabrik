import { Arbeitsschritt } from "../models/Arbeitsschritt";
import { Charge } from "../models/Charge";
import { Maschine } from "../models/Maschine";
import { Material } from "../models/Material";
import { Mitarbeiter } from "../models/Mitarbeiter";
import { Produkt } from "../models/Produkt";
import { Produktionslinie } from "../models/Produktionslinie";
import { Optimierung } from "../openapi";

export const testArbeitsschritt1: Arbeitsschritt = {
  id: "a1",
  name: "Bohren",
};

export const testArbeitsschritt2: Arbeitsschritt = {
  id: "a2",
  name: "Sägen",
};

export const testArbeitsschritt3: Arbeitsschritt = {
  id: "a3",
  name: "Stechen",
};

export const testArbeitsschritte = [
  testArbeitsschritt1,
  testArbeitsschritt2,
  testArbeitsschritt3,
];

export const testMaschine1: Maschine = {
  id: "m1",
  name: "Bohrmaschine",
  ausfallWahrscheinlichkeit: 0.5,
  kostenMinute: 10,
  anschaffungskosten: 100,
  mitarbeiterMin: 1,
  mitarbeiterMax: 2,
  ruestzeit: 60,
  maschinenbefaehigungen: [
    {
      schrittId: "1",
      taktrate: 1,
    },
  ],
};

export const testMaschine2: Maschine = {
  id: "m2",
  name: "Sägemaschine",
  ausfallWahrscheinlichkeit: 0.0,
  kostenMinute: 2,
  anschaffungskosten: 200,
  mitarbeiterMin: 3,
  mitarbeiterMax: 5,
  ruestzeit: 30,
  maschinenbefaehigungen: [],
};

export const testMaschine3: Maschine = {
  id: "m3",
  name: "Nagelmaschine",
  ausfallWahrscheinlichkeit: 0.001,
  kostenMinute: 0.1,
  anschaffungskosten: 10,
  mitarbeiterMin: 2,
  mitarbeiterMax: 3,
  ruestzeit: 120,
  maschinenbefaehigungen: [],
};

export const testMaschinen = [testMaschine1, testMaschine2, testMaschine3];

export const testMaterial1: Material = {
  id: "ma1",
  name: "Buchenholz",
  kostenStueck: 15,
  bestand: 8,
  aufstockenMinute: 2,
};

export const testMaterial2: Material = {
  id: "ma2",
  name: "Schrauben",
  kostenStueck: 0.1,
  bestand: 5000,
  aufstockenMinute: 200,
};

export const testMaterial3: Material = {
  id: "ma3",
  name: "Eisen",
  kostenStueck: 30,
  bestand: 50,
  aufstockenMinute: 3,
};

export const testMaterialien = [testMaterial1, testMaterial2, testMaterial3];

export const testMitarbeiter1: Mitarbeiter = {
  id: "2321",
  name: "Lisa",
  befaehigungen: [
    {
      schrittId: "a1",
      taktrate: 1,
    },
  ],
  wechselzeit: 2,
};

export const testMitarbeiter2: Mitarbeiter = {
  id: "2428",
  name: "Max",
  befaehigungen: [],
  wechselzeit: 5,
};

export const testMitarbeiter3: Mitarbeiter = {
  id: "2428",
  name: "Ludwig",
  befaehigungen: [],
  wechselzeit: 8,
};

export const testMitarbeiter = [
  testMitarbeiter1,
  testMitarbeiter2,
  testMitarbeiter3,
];

export const testProdukt1: Produkt = {
  id: "P01",
  name: "Produkt 01",
  verkaufspreis: 8,
  produktionsschritte: [],
  materialbedarf: [],
};

export const testProdukt2: Produkt = {
  id: "P02",
  name: "Produkt 02",
  verkaufspreis: 7,
  produktionsschritte: [],
  materialbedarf: [],
};

export const testProdukt3: Produkt = {
  id: "P03",
  name: "Produkt 03",
  verkaufspreis: 15,
  produktionsschritte: [],
  materialbedarf: [],
};

export const testProdukte = [testProdukt1, testProdukt2, testProdukt3];

export const testCharge1: Charge = {
  id: "6481",
  name: "Tischauftrag",
  prioritaet: 99,
  produktbedarf: [],
};

export const testCharge2: Charge = {
  id: "8323",
  name: "Werkzeugherstellung",
  prioritaet: 55,
  produktbedarf: [],
};

export const testCharge3: Charge = {
  id: "5975",
  name: "Regalanfertigung",
  prioritaet: 66,
  produktbedarf: [],
};

export const testChargen = [testCharge1, testCharge2, testCharge3];

export const testProduktionslinie1: Produktionslinie = {
  id: "P1",
  stationen: [
    {
      id: "S1",
      name: "Station 1",
      order: 0,
      maschinen: [testMaschine1],
      chargen: [testCharge1],
    },
  ],
};

export const testOptimierung1: Optimierung = {
  id: "1",
  ausfuehrung: "2017-07-21T17:32:28Z",
  produktionslinie: "000001",
  stationen: [
    {
      station: "8df47b62-1758-40f4-9d22-f7e9f12fdcf9",
      name: "Fertigung",
      charge: "69c22a3cf7e240d5ad9c980f36bb9375",
      gegeben: {
        kostenProdukt: 3.5,
        maschinenauslastung: [
          {
            maschine: "2a267c98bc444f62ab58b34b2fbb45f7",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "ddbd344df1384a19b37141eeeba0d7c8",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "76ec1a6c711245138f7104dd37a1030e",
            arbeitsschritt: "503c98ed81fc4aedb358478be6540c7a",
            auslastung: 0.1,
          },
        ],
      },
      optimiert: {
        kostenProdukt: 3.11,
        maschinenauslastung: [
          {
            maschine: "2a267c98bc444f62ab58b34b2fbb45f7",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "ddbd344df1384a19b37141eeeba0d7c8",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "76ec1a6c711245138f7104dd37a1030e",
            arbeitsschritt: "503c98ed81fc4aedb358478be6540c7a",
            auslastung: 0.1,
          },
          {
            maschine: "c7cce52dbf6840cba372de510644cfcb",
            arbeitsschritt: "503c98ed81fc4aedb358478be6540c7a",
            auslastung: 0.1,
          },
        ],
      },
    },
    {
      station: "322917b2-d174-49b8-8d7b-14a68876c34d",
      name: "Werkshalle",
      charge: "e79057be0440476d86ab74bc716cdb97",
      gegeben: {
        kostenProdukt: 3.11,
        maschinenauslastung: [
          {
            maschine: "2a267c98bc444f62ab58b34b2fbb45f7",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "ddbd344df1384a19b37141eeeba0d7c8",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "76ec1a6c711245138f7104dd37a1030e",
            arbeitsschritt: "503c98ed81fc4aedb358478be6540c7a",
            auslastung: 0.1,
          },
        ],
      },
      optimiert: {
        kostenProdukt: 3.11,
        maschinenauslastung: [
          {
            maschine: "2a267c98bc444f62ab58b34b2fbb45f7",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "ddbd344df1384a19b37141eeeba0d7c8",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "76ec1a6c711245138f7104dd37a1030e",
            arbeitsschritt: "503c98ed81fc4aedb358478be6540c7a",
            auslastung: 0.1,
          },
        ],
      },
    },
    {
      station: "3c6ce505-2cb4-419e-9eca-07e79d33577c",
      name: "Baustraße",
      charge: "69c22a3cf7e240d5ad9c980f36bb9375",
      gegeben: {
        kostenProdukt: 3.11,
        maschinenauslastung: [
          {
            maschine: "2a267c98bc444f62ab58b34b2fbb45f7",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "ddbd344df1384a19b37141eeeba0d7c8",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "76ec1a6c711245138f7104dd37a1030e",
            arbeitsschritt: "503c98ed81fc4aedb358478be6540c7a",
            auslastung: 0.1,
          },
        ],
      },
      optimiert: {
        kostenProdukt: 3.11,
        maschinenauslastung: [
          {
            maschine: "2a267c98bc444f62ab58b34b2fbb45f7",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "ddbd344df1384a19b37141eeeba0d7c8",
            arbeitsschritt: "1fbc305a576144dc8dde5b681774db40",
            auslastung: 0.3,
          },
          {
            maschine: "76ec1a6c711245138f7104dd37a1030e",
            arbeitsschritt: "503c98ed81fc4aedb358478be6540c7a",
            auslastung: 0.1,
          },
        ],
      },
    },
  ],
};

export const testOptimierung2: Optimierung = {
  ...testOptimierung1,
  id: "2",
  ausfuehrung: "2017-07-21T18:32:28Z",
};

export const testOptimierungen: Optimierung[] = [
  testOptimierung1,
  testOptimierung2,
];
