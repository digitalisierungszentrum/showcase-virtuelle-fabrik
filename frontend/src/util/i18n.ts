import * as i18next from "i18next";
import { initReactI18next } from "react-i18next";

import translationDe from "../locales/de/translation.json";

/**
 * Definition der Localization-Ressourcen.
 */
const resources = {
  de: {
    translation: translationDe,
  },
};

/**
 * Konfiguration von i18next.
 */
i18next.use(initReactI18next).init({
  resources,
  lng: "de",
  fallbackLng: "de",
  interpolation: {
    escapeValue: false,
  },
});

export default i18next;
