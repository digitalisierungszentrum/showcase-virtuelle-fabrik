/**
 * Flattened ein beliebiges Object,
 * e.g:
 * ```
 * { a: { b : [ { c: "d" } ] } }
 * // wird zu
 * { "a.b.1.c": "d" }
 * ```
 *
 * @param object Das zu flattende Objekt.
 * @param prefix prefix für die keys
 * @returns Flattened Object.
 */
export function flatten(object: any, prefix = ""): any {
  return Object.keys(object).reduce((prev, element) => {
    return object[element] &&
      typeof object[element] == "object" &&
      !Array.isArray(element)
      ? { ...prev, ...flatten(object[element], `${prefix}${element}.`) }
      : { ...prev, ...{ [`${prefix}${element}`]: object[element] } };
  }, {});
}

/**
 * parsed eine Sekundenzahl in das format 'hh:mm:ss'.
 */
export function secondsToTimeString(seconds: number): string {
  const sec = seconds % 60;
  const min = Math.floor(seconds / 60) % 60;
  const hours = Math.floor(seconds / 60 / 60);

  let ret = `${leftPad(min + "", 2, "0")}:${leftPad(sec + "", 2, "0")}`;

  if (hours > 0) {
    ret = `${leftPad(hours + "", 2, "0")}:${ret}`;
  }
  return ret;
}

function leftPad(str: string, places: number, pad: string) {
  let ret = "";
  for (let i = 0; i < places - str.length; i++) {
    ret += pad;
  }

  return ret + str;
}
