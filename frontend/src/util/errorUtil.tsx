import { ValueState } from "@ui5/webcomponents-react";
import { FormikErrors } from "formik";
import { useMemo } from "react";
import { TFunction, useTranslation } from "react-i18next";

import { flatten } from "./misc";

/**
 * Format zur darstellung von Fehlermeldungen.
 */
export type Message = {
  count: number;
  groupName: string;
  title: string;
  subtitle: string;
  type: ValueState;
};

function pushOrInc(
  key: string,
  value: any,
  set: { [id: string]: Message },
  t: TFunction,
  i18nPrefix: string
) {
  const id = `${key}/${value}`;

  if (set[id]) {
    set[id].count += 1;
  } else {
    const groupName = t(`${i18nPrefix}.${key}`);
    set[id] = {
      count: 1,
      groupName: groupName,
      title: value,
      subtitle: groupName,
      type: ValueState.Error,
    };
  }
}

/**
 * Parsed FormikErrors in ein weiterverwendbares Format.
 *
 * @param input Zu parsender Error
 * @param t translation function
 * @param i18nPrefix translation key prefix für object keys.
 * @returns Geparsete Errors als {@link Message}.
 */
function parseErrors<T>(
  input: FormikErrors<T>,
  t: TFunction,
  i18nPrefix: string
): Message[] {
  const set: { [id: string]: Message } = {};

  for (const [key, value] of Object.entries(flatten(input))) {
    pushOrInc(key.split(".")[0], value, set, t, i18nPrefix);
  }

  return Object.values(set);
}

/**
 * React hook für die errorParsing funktion.
 *
 * @param errors zu parsender Error
 * @param i18nPrefix translation key prefix für object keys.
 * @returns Geparsete Errors als {@link Message}.
 */
export function useParseErrors<T>(
  errors: FormikErrors<T> | undefined,
  i18nPrefix: string
) {
  const { t } = useTranslation();

  return useMemo(
    () => (errors ? parseErrors(errors, t, i18nPrefix) : undefined),
    [errors, t, i18nPrefix]
  );
}

/**
 * Helper der ValueState und Message von ui5-formfields setzt.
 * @returns
 */
export function useHandleFieldState() {
  const { t } = useTranslation();

  return (
    error?: string
  ): {
    valueState: ValueState;
    valueStateMessage: JSX.Element;
  } => {
    return {
      valueState: error ? ValueState.Error : ValueState.None,
      valueStateMessage: <div>{t(error ?? "")}</div>,
    };
  };
}
