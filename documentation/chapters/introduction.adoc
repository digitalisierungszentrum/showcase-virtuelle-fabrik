The Virtuelle Fabrik represents a minimal setup of a production. With it, it is possible to visualize and simulate production scenarios. It can help the user to imagine and plan non-existing production lines as well as existing production lines and provide optimization suggestions.

== Quick Start

Simply run `docker compose up -d` and you are set to go

The frontend will be available on http://localhost/